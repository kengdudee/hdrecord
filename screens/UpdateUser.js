import React from 'react'
import { StyleSheet, Text, View, TextInput, SafeAreaView, Alert, Image,ScrollView } from 'react-native'
import { Grid, Col } from 'react-native-easy-grid'
import { CheckBox,Button, SearchBar,ListItem } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
console.disableYellowBox = true;
import axios from 'axios';

import AwesomeAlert from 'react-native-awesome-alerts';
import Icon from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { bool } from 'prop-types';

import { JSHash, JSHmac, CONSTANTS } from "react-native-hash";



export default class UpdateUser extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        name:'',
        email:'',
        password:'',
        adminCheck:false,
        showPass:false,
        showAddAlert:false,
        showFormatAlert:false,
        showWrongAlert:false,
        showDupAlert:false,
        help:false
      };
    }

  helpShow =()=>{
    this.setState({help:true})
  }
  helpHide = ()=>{
    this.setState({help:false})
  }
    showAddAlert = () => {
        this.setState({
          showAddAlert: true
        });
      };
      hideAddAlert = () => {
        this.setState({
          showAddAlert: false
        });
      };
      showFormatAlert = () => {
        this.setState({
          showFormatAlert: true
        });
      };
      hideFormatAlert = () => {
        this.setState({
          showFormatAlert: false
        });
      };
      showWrongAlert = () => {
        this.setState({
          showWrongAlert: true
        });
      };
      hideWrongAlert = () => {
        this.setState({
          showWrongAlert: false
        });
      };
      showDupAlert = () => {
        this.setState({
          showDupAlert: true
        });
      };
      hideDupAlert = () => {
        this.setState({
          showDupAlert: false
        });
      };
    async componentDidMount() {
        //console.log(window.update_user_key)
        let res = await axios.post('http://'+window.ip_server+':'+window.port_server+'/getUserDetail', {
       
        id: window.update_user_key
      
    })

    let data = res.data
    //console.log(data[0].email)
    this.setState({name:data[0].name})
    this.setState({email:data[0].email})
    if(data[0].role=='admin'){
        this.setState({adminCheck:true})
    }
      
      
  }
  
    
    goToManageUser = () => {

      this.props.navigation.navigate('ManageUser')
    }

    

    validateEmail(email) //return true when correct email format
      {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //console.log(re.test(email))
        return re.test(email);
      }
      validatePass(pass) //return true when correct pass format
      {
        var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
        //console.log(re.test(pass))
        return re.test(pass);
      }
      handleSubmit=()=>{

        if(this.validateEmail(this.state.email)){
          if(this.state.password!=''){
            if(this.validatePass(this.state.password)){
              this.updateUser()}
              else{
                this.showFormatAlert()
              }

          }else{
            this.updateUser()}
          }
          
        else{
            this.showFormatAlert()
        }

      }

      async updateUser(){
          if(this.state.name!=''&&this.state.email!=''&&this.state.password!=''){
            let hash_input_pass = await JSHash(this.state.password, CONSTANTS.HashAlgorithms.sha256)
            let role

            if(this.state.adminCheck){
                role='admin'
            }else{
                role='user'
            }

        let res = await axios.post('http://'+window.ip_server+':'+window.port_server+'/updateUser', {
            
            id:window.update_user_key,
            name:this.state.name,
            email:this.state.email,
            password:hash_input_pass,
            role:role
          
          })
    
          let data = res.data
          
          if(data){
              this.showAddAlert()
          }else{
              this.showDupAlert()
          }
        }
        else if(this.state.name!=''&&this.state.email!=''){
          let role

            if(this.state.adminCheck){
                role='admin'
            }else{
                role='user'
            }

            let res = await axios.post('http://'+window.ip_server+':'+window.port_server+'/updateUserNotChangePass', {
            
            id:window.update_user_key,
            name:this.state.name,
            email:this.state.email,
            role:role
          
          })
    
          let data = res.data
          
          if(data){
              this.showAddAlert()
          }else{
              this.showDupAlert()
          }

          }else{
              this.showWrongAlert()
          }


      }


    render(){
        const { search,showAddAlert,showDupAlert,showFormatAlert,showWrongAlert,help } = this.state;
      return(
        <SafeAreaView style={styles.container}>
            
          <View style={styles.main}>
            <View style={styles.up}>
              <View style={styles.up_line}>
              <View style={styles.up_left}>
                      <Ionicons.Button
                        name={'ios-arrow-back'}
                        size={hp('3')}
                        color='#000000'
                        backgroundColor='#fff'
                        onPress={this.goToManageUser}
                        title="back"
                      />
                </View>
              <View style={styles.up_right}>
                    <Text style={styles.head}>Update User</Text>
              </View>
              <View style={styles.up_left}>
              </View>

            </View>
            </View>
          </View>
          <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          <View style={{flexDirection:'column'}}>
                        
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(2)}}>
                                <View>
                                <Text style={{fontSize:hp('3.6')}}>name</Text>
                                </View>
                                <View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('68'),height:hp('5.4'),borderColor:'black',borderColor:'grey',marginLeft:wp(2)}}
                                    onChangeText={(name) => this.setState({name})}
                                    fontSize={hp('4.6')}
                                    value={this.state.name}
                                    onSubmitEditing={() => { this.t1.focus(); }}
                                    blurOnSubmit={false}
                                /> 
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(2)}}>
                                <View>
                                <Text style={{fontSize:hp('3.6')}}>email</Text>
                                </View>
                                <View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('68'),height:hp('5.4'),borderColor:'black',borderColor:'grey',marginLeft:wp(2)}}
                                    onChangeText={(email) => this.setState({email})}
                                    fontSize={hp('4.6')}
                                    value={this.state.email}
                                     ref={(input) => { this.t1 = input; }}
                                     onSubmitEditing={() => { this.t2.focus(); }}
                                    blurOnSubmit={false}
                                /> 
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(2)}}>
                                <View>
                                <Text style={{fontSize:hp('3.6')}}>password</Text>
                                </View>
                                <CheckBox
                                  checkedIcon={<Image style={{width:wp(1.8),height:wp(1.8),marginTop:wp(-1),marginRight:wp(-1),marginLeft:wp(-1)}} source={require('../assets/help.png')}/>}
                                  uncheckedIcon={<Image style={{width:wp(1.8),height:wp(1.8),marginTop:wp(-1),marginRight:wp(-1),marginLeft:wp(-1)}} source={require('../assets/help.png')} />}
                                  checked={this.state.help}
                                  onPress={() => this.helpShow()}
                                  //containerStyle={{alignItems:'flex-end',alignSelf:'flex-end',backgroundColor:'black',width:wp(40),justifyContent:'flex-end'}}
                                  /> 
                                <View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('59'),height:hp('5.4'),borderColor:'black',borderColor:'grey',marginLeft:wp(2)}}
                                    onChangeText={(password) => this.setState({password})}
                                    fontSize={hp('4.6')}
                                    value={this.state.password}
                                    ref={(input) => { this.t2 = input; }}
                                    blurOnSubmit={false}
                                    secureTextEntry={!this.state.showPass}
                                /> 
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(2)}}>
                                <View>
                                    <CheckBox 
                                        title='show password'
                                        checked={this.state.showPass}
                                        onPress={() => this.setState({showPass:!this.state.showPass})}
                                        size={wp('2.6')}
                                        textStyle={{
                                            fontSize:hp('3.4')
                                        }}
                                    />
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(2)}}>
                                <View>
                                    <CheckBox 
                                        title='admin role'
                                        checked={this.state.adminCheck}
                                        onPress={() => this.setState({adminCheck:!this.state.adminCheck})}
                                        size={wp('2.6')}
                                        textStyle={{
                                            fontSize:hp('3.6')
                                        }}
                                    />
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(3)}}>
                                       
                                        <View><Button
                                onPress={this.handleSubmit}
                                title='Update User'
                                containerStyle={{
                                width:wp('26'),
                                
                                
                                }}
                                titleStyle={{
                                    color: "white",
                                    fontSize: wp(2.8),
                                }}
                            /></View>
                            

                        </View>
                        </View>
                        </KeyboardAwareScrollView>
                        <AwesomeAlert
          show={showDupAlert}
          showProgress={false}
          title="This email or password already exists."
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="red"
          // cancelButtonColor="#606060"
          onCancelPressed={() => {
            // window.deleteKey=''
            // this.hideAlert();
          }}
          onConfirmPressed={() => {
            // this.toDeleteRecord(window.deleteKey)
             this.hideDupAlert()
          }}
        />
        <AwesomeAlert
          show={help}
          showProgress={false}
          title="Password rules"
          titleStyle={{fontSize:hp('4.6')}}
          messageStyle={{fontSize:hp('3.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message="Passwords must have at least 8 characters and contain the following: uppercase letters, lowercase letters and numbers."
          
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="blue"
          // cancelButtonColor="#606060"
          onCancelPressed={() => {
            // window.deleteKey=''
            // this.hideAlert();
          }}
          onConfirmPressed={() => {
            // this.toDeleteRecord(window.deleteKey)
             this.helpHide()
          }}
        />
        <AwesomeAlert
          show={showAddAlert}
          showProgress={false}
          title="Update successful."
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="blue"
          // cancelButtonColor="#606060"
          onCancelPressed={() => {
            // window.deleteKey=''
            // this.hideAlert();
          }}
          onConfirmPressed={() => {
            // this.toDeleteRecord(window.deleteKey)
             this.hideAddAlert()
          }}
        />
        <AwesomeAlert
          show={showFormatAlert}
          showProgress={false}
          title="The email format is incorrect or the password does not follow the rules."
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="red"
          // cancelButtonColor="#606060"
          onCancelPressed={() => {
            // window.deleteKey=''
            // this.hideAlert();
          }}
          onConfirmPressed={() => {
            // this.toDeleteRecord(window.deleteKey)
             this.hideFormatAlert()
          }}
        />
        <AwesomeAlert
          show={showWrongAlert}
          showProgress={false}
          title="Invalid input"
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="red"
          // cancelButtonColor="#606060"
          onCancelPressed={() => {
            // window.deleteKey=''
            // this.hideAlert();
          }}
          onConfirmPressed={() => {
            // this.toDeleteRecord(window.deleteKey)
             this.hideWrongAlert()
          }}
        />

      </SafeAreaView>
      )
    }
  
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        
      },
      deleteAndRecoverAll:{
          flexDirection:'row',
          backgroundColor:'#f9f9f9',
          justifyContent:'center'
    
      },
      head:{
        fontSize:hp('2.6%'),
        fontWeight:'bold',
    
      },
      main:{
        flexDirection:'row'
    
      },
      up:{
        flexDirection:'row',
        backgroundColor:'#fff',
        
      },
      up_left:{
        justifyContent:'flex-start',
        alignItems:'flex-start',
        width:wp('10%'),
      },
      up_line:{
        flexDirection:'row',
        
      },
      up_right:{
        width:wp('80%'),
        justifyContent:'center',
        alignItems:'center',
      },
      down:{
        flexDirection:'column',
        backgroundColor:'#f9f9f9',
      },
      left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }
  
  })