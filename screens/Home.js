import React, { Fragment,component } from 'react'
import { StyleSheet, Text, View, Button, Dimensions, Image, ScrollView, SafeAreaView, Alert, TouchableNativeFeedbackBase,TouchableOpacity, ActivityIndicator,RefreshControl } from 'react-native'
import {Header} from 'react-native-elements'
import { TextInput } from 'react-native-gesture-handler'
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import { CheckBox, ListItem, SearchBar } from 'react-native-elements';
import { NavigationEvents } from 'react-navigation';
import AwesomeAlert from 'react-native-awesome-alerts';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
console.disableYellowBox = true;
import axios from 'axios';
import { JSHash, JSHmac, CONSTANTS } from "react-native-hash";
 

export default class Home extends React.Component {

  goToSearch = () => this.props.navigation.navigate('Search')
  goToRecentlyDeleted = () => this.props.navigation.navigate('RecentlyDeleted')

  
  constructor(props) {
    super(props);
    
    this.state = {
      
      hd_record_form1:true,
      hd_record_form2:false,
      hd_record_form3:false,
      hd_record_form4:false,
      hd_record_form5:false,
      list:[],
      sort:'Date (new to old)',
      fullList:[],
      limit:10,
      numRecord:0,
      isOld:false,
      showAlert:false,
      showLoad:true,
      refreshing:false,
      toRefresh:false,
      
      
    };
    
  }

  async refresh() {
    //Alert.alert("เข้าLIST")

    var patientList =[]
      var keyList =[]
      var createdList =[]
      var typeList =[]
      var prefixList = []
      var firstNameList =[]
      var lastNameList =[]
      var num=0


      const config = {
        method: 'get',
        url: 'http://'+window.ip_server+':'+window.port_server+'/getForRefreshList'
      }
  
      let res = await axios(config)
  

      for(let i=0;i<res.data.length;i++){
            
            if(res.data[i].toShow){
              patientList.push(res.data[i].patient)
              keyList.push(res.data[i].record_id)
              createdList.push(res.data[i].created)

              prefixList.push(res.data[i].prefix_patient)
              firstNameList.push(res.data[i].first_patient)
              lastNameList.push(res.data[i].last_patient)

              if(res.data[i].acute){
                  typeList.push('Acute')
              }
              else{
                  typeList.push('Chronic')
              }
              num=num+1
           }

      }
      
      
      var localList =[]
      var j=0
      for(var i=num-1;i>=0;i=i-1){
        
          localList.push({})
        localList[j].title = patientList[i]
        localList[j].icon = "description"
        localList[j].subtitle= createdList[i]
        localList[j].rightSubtitle = typeList[i]
        localList[j].rightTitle = keyList[i]

        j=j+1
        
      }
      

      // if(!this.state.sort.localeCompare("Date (old to new)")){
      //   this.setState({fullList:this.state.fullList.reverse()})
      // }
      
      window.patientList=patientList
      window.typeList=typeList
      window.keyList=keyList
      window.createdList=createdList
      window.prefixList=prefixList
      window.firstNameList=firstNameList
      window.lastNameList=lastNameList

      
      var local_num=localList.length

      var localTrueList=[]

      if(local_num<this.state.limit){
          for(var i=0;i<local_num;i++){
            
            localTrueList.push(localList[i])
          }
      }
      else{
          for(var i=0;i<this.state.limit;i++){
            
            localTrueList.push(localList[i])
          }
      }

      this.setState({fullList:localList})
      this.setState({numRecord:localList.length})
      this.setState({list:localTrueList})
  }

  
  async componentDidMount() {
    //Alert.alert("เข้าMOUNT")
    
    //console.log('HIII COMPONENT')

    //For Search
    window.patientList=[]
    window.keyList=[]
    window.createdList=[]
    window.typeList=[]
    window.prefixList=[]
    window.firstNameList=[]
    window.lastNameList=[]
    
    //For ViewContent
    window.fromHome=true
    window.key=''

    window.deleteKey=''
    //global variable
    //form1
    window.acute= false
    window.chronic= false
    window.CGD=false
    window.SSS=false
    window.NHSO=false
    window.state_enterprise=false
    window.self_pay=false
    window.patient=''
    window.prefix_patient=''
    window.first_patient=''
    window.last_patient=''
    window.other_patient=''
    window.id=''
    window.HN=''
    window.HD_no=''
    window.date=''
    window.ward=''
    window.diagnosis=''
    window.notice=''
    window.nurse_assign=''
    //form2
    window.aph_e=false
    window.aph_lc=false
    window.aph_env=false
    window.aph_cd=false
    window.aph_d=false
    window.aph_cp=false
    window.aph_p=false
    window.aph_f=false
    window.aph_nv=false
    window.aph_h=false
    window.aph_bt=false
    window.aph_i=false
    window.aph_ano=false
    window.aph_anx=false
    window.aph_sd=false
    window.aph_c=false
    window.aph_ps=false
    window.aph_other=false
    window.aph_e_n=false
    window.aph_lc_n=false
    window.aph_env_n=false
    window.aph_cd_n=false
    window.aph_d_n=false
    window.aph_cp_n=false
    window.aph_p_n=false
    window.aph_f_n=false
    window.aph_nv_n=false
    window.aph_h_n=false
    window.aph_bt_n=false
    window.aph_i_n=false
    window.aph_ano_n=false
    window.aph_anx_n=false
    window.aph_sd_n=false
    window.aph_c_n=false
    window.aph_ps_n=false
    window.aph_other_desc=''
    //form3
    window.DLC=false
    window.PERM=false
    window.AVF=false
    window.AVG=false
    window.thrill=false
    window.bruit=false
    window.catheter_site=''
    window.insertion_date=''
    window.needle_no=''
    window.characteristics_found=''
    window.BP=''
    window.PR=''
    window.RR=''
    window.sign_symp=''
    //form4
    window.normal_formula=false
    window.v_Na=false
    window.v_K=false
    window.v_HCO3=false
    window.v_other=false
    window.physician=''
    window.machine=''
    window.self_tpb=''
    window.dialyzer=''
    window.AS=''
    window.Kuf=''
    window.TCV=''
    window.last_TCV=''
    window.use_no=''
    window.v_Na_unit=''
    window.v_K_unit=''
    window.v_HCO3_unit=''
    window.v_other_desc=''
    //form5
    window.heparin_init_dose=false
    window.LMW=false
    window.no_heparin=false
    window.dialysate_flow=''
    window.dialysate_temp=''
    window.conductivity=''
    window.heparin_init_unit=''
    window.maintenance_unit=''
    window.LMW_unit=''
    window.nss_flush=''
    window.q=''
    window.pre_time=''
    window.BW=''
    window.DW=''
    window.weight_gain=''
    window.last_BW=''
    window.UFG=''
    window.set_UF=''
    window.dialysis_len=''
    window.hd_timeon=''
    window.hd_timeoff=''
    //form6
    window.nd_u=false
    window.nd_fve=false
    window.nd_ei=false
    window.nd_arr=false
    window.nd_ane=false
    window.nd_cf=false
    window.nd_rh=false
    window.nd_ri=false
    window.nd_dfv=false
    window.nd_i=false
    window.nd_m=false
    window.nd_scd=false
    window.nd_rf=false
    window.nd_rcdd=false
    window.nd_other=false
    window.nd_u_p=''
    window.nd_fve_p=''
    window.nd_ei_p=''
    window.nd_arr_p=''
    window.nd_ane_p=''
    window.nd_cf_p=''
    window.nd_rh_p=''
    window.nd_ri_p=''
    window.nd_dfv_p=''
    window.nd_i_p=''
    window.nd_m_p=''
    window.nd_scd_p=''
    window.nd_rf_p=''
    window.nd_rcdd_p=''
    window.nd_other_p=''
    window.nd_rcdd_desc=''
    window.nd_other_desc=''
    //form7
    window.i_mwe=false
    window.i_ot=false
    window.i_pu=false
    window.i_rf=false
    window.i_t=false
    window.i_ldt=false
    window.i_i=false
    window.i_n=false
    window.i_bt=false
    window.i_oc=false
    window.i_maf=false
    window.i_cbd=false
    window.i_se=false
    window.i_ps=false
    window.i_nd=false
    window.i_other=false
    window.i_other_desc=''
    //form8
    window.eph_nocom=false
    window.eph_osat=false
    window.eph_fb=false
    window.eph_eb=false
    window.eph_ad=false
    window.eph_hn=false
    window.eph_nop=false
    window.eph_s=false
    window.eph_com=false
    window.eph_hypo=false
    window.eph_hyper=false
    window.eph_arr=false
    window.eph_cp=false
    window.eph_mc=false
    window.eph_ap=false
    window.eph_cc=false
    //form9
    window.tableHead= ['time','','','','','','','','','','','','']
    window.tableO2sat= ['','','','','','','','','','','','']
    window.tableQB= ['','','','','','','','','','','','']
    window.tableVP= ['','','','','','','','','','','','']
    window.tableTMP= ['','','','','','','','','','','','']
    window.tableUFR= ['','','','','','','','','','','','']
    window.tableUF= ['','','','','','','','','','','','']

    window.g_time=['time','','','','','','','','','','','']
    window.g_bpup=[0]
    window.g_bpdown=[0]
    window.g_pr=[0]
    window.g_rr=[0]
    
    window.tableSubmitTime=[]

    window.TtextMarginLeft=0
    window.TmarginLeft=0
    window.Twidth=0
    //form10
    window.av_shunt_thrill=false
    window.av_shunt_bruit=false
    window.problem=false
    window.nurse_note=''
    window.nurse_note_object=[]
    window.post_BP=''
    window.pulse=''
    window.post_RR=''
    window.post_BW=''
    window.weight_loss=''
    window.problem_desc=''
    window.TPN=''
    window.venofer=''
    window.ESA=''
    //form11
    window.gnextDate=''
    window.refer_to_home=false
    window.refer_to_ward=false
    window.NSS=''
    window.glucose=''
    window.extra_f=''
    window.total_intake=''
    window.total_UF=''
    window.net_UF=''
    window.doctor_note=''
    window.doctor_note_object=[]
    window.refer_to_ward_desc=''
    window.next_date=''
    window.next_time=''
    window.created=''
    window.toShow=true
    //end db variable


    //this.refreshList()
    var patientList =[]
      var keyList =[]
      var createdList =[]
      var typeList =[]
      var prefixList = []
      var firstNameList =[]
      var lastNameList =[]
      var num=0


      const config = {
        method: 'get',
        url: 'http://'+window.ip_server+':'+window.port_server+'/getForRefreshList'
      }
  
      let res = await axios(config)
  

      for(let i=0;i<res.data.length;i++){
            
            if(res.data[i].toShow){
              patientList.push(res.data[i].patient)
              keyList.push(res.data[i].record_id)
              createdList.push(res.data[i].created)

              prefixList.push(res.data[i].prefix_patient)
              firstNameList.push(res.data[i].first_patient)
              lastNameList.push(res.data[i].last_patient)

              if(res.data[i].acute){
                  typeList.push('Acute')
              }
              else{
                  typeList.push('Chronic')
              }
              num=num+1
           }

      }
      
      
      var localList =[]
      var j=0
      for(var i=num-1;i>=0;i=i-1){
        
          localList.push({})
        localList[j].title = patientList[i]
        localList[j].icon = "description"
        localList[j].subtitle= createdList[i]
        localList[j].rightSubtitle = typeList[i]
        localList[j].rightTitle = keyList[i]

        j=j+1
        
      }
      

      // if(!this.state.sort.localeCompare("Date (old to new)")){
      //   this.setState({fullList:this.state.fullList.reverse()})
      // }
      
      window.patientList=patientList
      window.typeList=typeList
      window.keyList=keyList
      window.createdList=createdList
      window.prefixList=prefixList
      window.firstNameList=firstNameList
      window.lastNameList=lastNameList

      
      var local_num=localList.length

      var localTrueList=[]

      if(local_num<this.state.limit){
          for(var i=0;i<local_num;i++){
            
            localTrueList.push(localList[i])
          }
      }
      else{
          for(var i=0;i<this.state.limit;i++){
            
            localTrueList.push(localList[i])
          }
      }

      this.setState({fullList:localList})
      this.setState({numRecord:localList.length})
      this.setState({list:localTrueList})
      //this.hideAlert()
      //console.log("HII")
      //React.component.forceUpdate()
      this.setState({showLoad:false})
  }
  


goToViewContent = key =>{
  window.fromHome=true
  window.key=key
  this.props.navigation.navigate('ViewContent')
}

deleteRecord = key =>{

  window.deleteKey=key
  this.showAlert()
  

}
showAlert = () => {
  this.setState({
    showAlert: true
  });
};
hideAlert = () => {
  this.setState({
    showAlert: false
  });
};



async toDeleteRecord(key){
  
  var url = 'http://'+window.ip_server+':'+window.port_server+'/deleteRecord';
  axios.post(url,{
      record_id:key
  })
  .then(function (response) {
  console.log(response);
  })
  .catch(function (error) {
  console.log(error);
  });
  //console.log(key)
  //this.refreshList()


  var local_fullList =this.state.fullList
  
  for(let i=0;i<local_fullList.length;i++){
      if(local_fullList[i].rightTitle==key){
          local_fullList.splice(i,1)
      }
  }
  var local_list =this.state.list
  
  for(let i=0;i<local_list.length;i++){
      if(local_list[i].rightTitle==key){
          local_list.splice(i,1)
      }
  }

  //this.setState({fullList:local_fullList})
  
  var local_numRecord=local_fullList.length

  var localTrueList=[]

  if(local_numRecord<this.state.limit){
      for(var i=0;i<local_numRecord;i++){
        
        localTrueList.push(local_fullList[i])
      }
  }
  else{
      for(var i=0;i<this.state.limit;i++){
        
        localTrueList.push(local_fullList[i])
      }
  }

  this.setState({numRecord:local_fullList.length})
  this.setState({list:localTrueList})
  
  //this.createList()
}

async refreshList(){

      var patientList =[]
      var keyList =[]
      var createdList =[]
      var typeList =[]
      var prefixList = []
      var firstNameList =[]
      var lastNameList =[]
      var num=0


      const config = {
        method: 'get',
        url: 'http://'+window.ip_server+':'+window.port_server+'/getForRefreshList'
      }
  
      let res = await axios(config)
  

      for(let i=0;i<res.data.length;i++){
            
            if(res.data[i].toShow){
              patientList.push(res.data[i].patient)
              keyList.push(res.data[i].record_id)
              createdList.push(res.data[i].created)

              prefixList.push(res.data[i].prefix_patient)
              firstNameList.push(res.data[i].first_patient)
              lastNameList.push(res.data[i].last_patient)

              if(res.data[i].acute){
                  typeList.push('Acute')
              }
              else{
                  typeList.push('Chronic')
              }
              num=num+1
           }

      }
      
      
      var localList =[]
      var j=0
      for(var i=num-1;i>=0;i=i-1){
        
          localList.push({})
        localList[j].title = patientList[i]
        localList[j].icon = "description"
        localList[j].subtitle= createdList[i]
        localList[j].rightSubtitle = typeList[i]
        localList[j].rightTitle = keyList[i]

        j=j+1
        
      }
      this.setState({fullList:localList})
      if(!this.state.sort.localeCompare("Date (old to new)")){
        this.setState({fullList:this.state.fullList.reverse()})
      }
      
      window.patientList=patientList
      window.typeList=typeList
      window.keyList=keyList
      window.createdList=createdList
      window.prefixList=prefixList
      window.firstNameList=firstNameList
      window.lastNameList=lastNameList

      this.setState({numRecord:this.state.fullList.length})

      this.createList()
      this.hideAlert()
      
}
createList=()=>{
  var localTrueList=[]

  if(this.state.numRecord<this.state.limit){
      for(var i=0;i<this.state.numRecord;i++){
        
        localTrueList.push(this.state.fullList[i])
      }
  }
  else{
      for(var i=0;i<this.state.limit;i++){
        
        localTrueList.push(this.state.fullList[i])
      }
  }

  this.setState({list:localTrueList})
}
seemore =()=>{
  if(this.state.numRecord>this.state.limit){
    var next=this.state.numRecord-this.state.limit
    if(next>10){
      next=10
    }
    var next = 'Next('+next.toString()+")"
  return <View><Button title ={next} onPress={this.updateLimit}/></View>
  }
  
}

updateLimit =()=>{
  this.setState({limit:this.state.limit+10})
  this.refreshList()
}
sortList = () =>{
  Alert.alert(
    'Sort by',
    'Please select your option to sort',
    [
      {text: 'Date (new to old)', onPress: () => {
        this.setState({limit:10})
        this.setState({isOld:false})
        this.setState({sort:'Date (new to old)'})
        this.refreshList()
      }},
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'Date (old to new)', onPress: () => {
        this.setState({limit:10})
        this.setState({sort:'Date (old to new)'})
        if(!this.state.isOld){
          this.refreshList()
          this.setState({isOld:true})
        }
        
          
      }},
    ],
    {cancelable: false},
  );
}
showLoad=()=>{
  if(this.state.showLoad){
  return  <View style={{alignSelf:'center',}}><ActivityIndicator size="large" color="grey" /></View>
  } 
}

goToForm = () => this.props.navigation.navigate('Form')

// refreshScreen(){
//   this.setState({toRefresh:true})
//   //return  <NavigationEvents onDidFocus={() => this.refresh()}/>
// // }
// toRefresh(){
//   if(this.state.toRefresh){
//     return  <NavigationEvents onDidFocus={() => this.refresh()}/>
//   }

// }

render(){
  const {showAlert} = this.state;
  
  
  return (
    <SafeAreaView style={styles.container}>

      {/* {this.toRefresh()} */}
      {/* <NavigationEvents
                onDidFocus={() => this.refresh()}
                /> */}
      <View style={styles.main}>
        <View style={styles.up}>
          {/* <View style={{flexDirection:'row'}}>
            <View><Text style={styles.head}>Documents</Text></View>
            <View style={{justifyContent:'flex-end',alignItems:'flex-end',alignSelf:'flex-end'}}>
              <CheckBox
                  checkedIcon={<Image style={{width:wp(2),height:wp(2)}} source={require('../assets/refresh.png')}/>}
                  uncheckedIcon={<Image style={{width:wp(2),height:wp(2)}} source={require('../assets/refresh.png')} />}
                  checked={this.state.toRefresh}
                  onPress={() => this.refresh()}
                  containerStyle={{alignItems:'flex-end',alignSelf:'flex-end',backgroundColor:'black',width:wp(40),justifyContent:'flex-end'}}
                  /> 
              </View>
          </View> */}
          <View style={styles.up_line}>
              <View style={styles.up_left}>
                      
                </View>
              <View style={styles.up_right}>
                    <Text style={styles.head}>Documents</Text>
              </View>
              <View style={styles.up_right_page}>
              <CheckBox
                  checkedIcon={<Image style={{width:wp(1.8),height:wp(1.8),marginTop:wp('-3')}} source={require('../assets/refresh.png')}/>}
                  uncheckedIcon={<Image style={{width:wp(1.8),height:wp(1.8),marginTop:wp('-3')}} source={require('../assets/refresh.png')} />}
                  checked={this.state.toRefresh}
                  onPress={() => this.refresh()}
                  //containerStyle={{alignItems:'flex-end',alignSelf:'flex-end',backgroundColor:'black',width:wp(40),justifyContent:'flex-end'}}
                  /> 
              </View>
            </View>
          <View>
              <CheckBox
                  checkedIcon={<Image style={styles.search_bar} source={require('../assets/search.png')}/>}
                  checked={true}
                  onPress={this.goToSearch}
                     
                  /> 
          </View>
        </View>
        <View style={styles.down}>
          <View style={styles.down_left}>
            <View style={styles.down_left_line}>
            <View>
              <CheckBox
                  checkedIcon={<Image style={styles.left_button} source={require('../assets/1.png')}/>}
                  uncheckedIcon={<Image style={styles.left_button} source={require('../assets/1.png')}/>}
                  checked={this.state.hd_record_form1}
                  onPress={() => this.setState({hd_record_form1: !this.state.hd_record_form1})}
                     
                  /> 
            </View>
            <View>
                <Ionicons.Button
                            name={'ios-create'}
                            size={hp('10')}
                            color='#000000'
                            backgroundColor='#f1f1f1'
                            onPress={this.goToForm}
                />
            </View>
            </View>
            <View style={styles.down_left_line}>
            <View>
            <CheckBox
                checkedIcon={<Image style={styles.left_button}  source={require('../assets/2un.png')}/>}
                uncheckedIcon={<Image style={styles.left_button}  source={require('../assets/2un.png')}/>}
                checked={this.state.hd_record_form2}
                onPress={() => this.setState({hd_record_form2: !this.state.hd_record_form2})}
                     
                />
                </View> 
                <View>
                <Ionicons.Button
                            name={'ios-create'}
                            size={hp('10')}
                            color='#000000'
                            backgroundColor='#f1f1f1'
                          />
                </View>
            </View>
            <View style={styles.down_left_line}>
            <View>
            <CheckBox
                checkedIcon={<Image style={styles.left_button}  source={require('../assets/3un.png')}/>}
                uncheckedIcon={<Image style={styles.left_button}  source={require('../assets/3un.png')}/>}
                checked={this.state.hd_record_form3}
                onPress={() => this.setState({hd_record_form3: !this.state.hd_record_form3})}
                     
                />
                </View> 
                <View>
                <Ionicons.Button
                            name={'ios-create'}
                            size={hp('10')}
                            color='#000000'
                            backgroundColor='#f1f1f1'
                          />
                </View>
            </View>
            <View style={styles.down_left_line}>
            <View>
            <CheckBox
                checkedIcon={<Image style={styles.left_button}  source={require('../assets/4un.png')}/>}
                uncheckedIcon={<Image style={styles.left_button}  source={require('../assets/4un.png')}/>}
                checked={this.state.hd_record_form4}
                onPress={() => this.setState({hd_record_form4: !this.state.hd_record_form4})}
                    
                />
                </View> 
                <View>
                <Ionicons.Button
                            name={'ios-create'}
                            size={hp('10')}
                            color='#000000'
                            backgroundColor='#f1f1f1'
                          />
                </View>
            </View>
            <View style={styles.down_left_line}>
            <View>
            <CheckBox
                checkedIcon={<Image style={styles.left_button}  source={require('../assets/5un.png')}/>}
                uncheckedIcon={<Image style={styles.left_button}  source={require('../assets/5un.png')}/>}
                checked={this.state.hd_record_form5}
                onPress={() => this.setState({hd_record_form5: !this.state.hd_record_form5})}
                />
                </View> 
                <View>
                <Ionicons.Button
                            name={'ios-create'}
                            size={hp('10')}
                            color='#000000'
                            backgroundColor='#f1f1f1'
                          />
                </View>
            </View>
            <View style={styles.down_left_line}>
                <View>
                <CheckBox
                      checkedIcon={<Image style={styles.redelete} source={require('../assets/redelete.png')}/>}
                      checked={true}
                      onPress={this.goToRecentlyDeleted}
                      /> 
                </View>
            </View>
          </View>
          <View style={styles.down_right}>
            
            <View style={styles.sort_area}>
                <View>
                    <Button title="Sort by" onPress={this.sortList}/>
                </View>
                <View>
                  <Text style={styles.sortFont}>{this.state.sort}</Text>
                </View>
            </View>
            <View style={styles.num_area}>
                  <View>
                  <Text style={styles.numList}>แสดง {this.state.list.length} จากทั้งหมด {this.state.numRecord} </Text>
                  </View>
            </View>
            <View style={styles.list_area}>
            <ScrollView style={styles.scrollView}
          >
                      {this.showLoad()}
                      
                      <View>
                      {
                        this.state.list.map((item, i) => (
                          <ListItem
                            key={i}
                            onPress={() => this.goToViewContent(item.rightTitle)}
                            onLongPress={() => this.deleteRecord(item.rightTitle)}
                            title={item.title}
                            subtitle={item.subtitle}
                            rightSubtitle={item.rightSubtitle}
                            leftIcon={{ name: item.icon }}
                            containerStyle={{
                            }}
                            titleStyle={{
                                fontSize:hp('2.6%'),
                            }}
                            subtitleStyle={{
                              fontSize:hp('2.6%'),
                                  color:'grey'
                            }}
                            rightSubtitleStyle={{
                              fontSize:hp('3%'),
                              color:'grey'
                            }}
                            bottomDivider={true}
                            chevron
                          />
                        ))
                      }
                      </View>
                      <View>
                        {this.seemore()}
                      </View>
                    </ScrollView>
            </View>
          </View>
        </View>
        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          title="Are you sure to delete this record?"
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="Delete"
          confirmButtonColor="red"
          cancelButtonColor="#606060"
          onCancelPressed={() => {
            window.deleteKey=''
            this.hideAlert();
          }}
          onConfirmPressed={() => {
            this.toDeleteRecord(window.deleteKey)
            this.hideAlert()
          }}
        />
      </View>
      
    </SafeAreaView>
    
  )
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    
  },
  main:{
    flexDirection:'column'
    
  },
  numList:{
    fontSize:hp('2.6%'),
    padding:hp('1'),
    color:'grey'
  },
  sortFont:{
    fontSize:hp('2.6%'),
    padding:hp('1'),
    alignSelf:'flex-end'
  },
  head:{
    fontSize:hp('2.6%'),
    fontWeight:'bold'
  },
  up:{
    flexDirection:'column',
    alignItems:'center',
    backgroundColor:'#f9f9f9',
    justifyContent:'flex-start'
  },
  
  down:{
    flexDirection:'row'
  },
  down_left:{
    backgroundColor:'#f9f9f9',
    flexDirection:'column'
  },
  down_left_line:{
    backgroundColor:'#f1f1f1',
    flexDirection:'row',
    justifyContent:'center'
  },
  down_right:{
    backgroundColor:'#f9f9f9'

  },
  search_bar:{
    marginTop:wp('-3'),
    height: hp('6%'), 
    width: wp('100%'),
    alignSelf:'center',
    resizeMode:'contain'

  },
  redelete:{
    height: hp('4%'), 
    width: wp('20%'),
    alignSelf:'center',
    resizeMode:'contain'

  },
  left_button:{
    height: hp('10%'), 
    width: wp('24%'),
    alignSelf:'flex-start',
    resizeMode:'contain'
  },
  sort_area:{
    flexDirection:'row',
    width: wp('62%'),
    justifyContent:'flex-end'

  },
  num_area:{
    flexDirection:'row',
    width: wp('62%'),
    justifyContent:'flex-start'

  },
  list_area:{
    height: hp('66%'), 
    width: wp('62%'),
    flexDirection:'column',
  },
  up_left:{
    justifyContent:'flex-start',
    alignItems:'flex-start',
    width:wp('10%'),
    
  },
  up_right_page:{
    justifyContent:'flex-end',
    alignItems:'flex-end',
    width:wp('10%'),
    padding:hp('2')
  },
  up_line:{
    flexDirection:'row',
    
  },
  up_right:{
    width:wp('80%'),
    justifyContent:'center',
    alignItems:'center',
    marginBottom:wp('2')
  },
  
})