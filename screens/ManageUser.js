import React from 'react'
import { StyleSheet, Text, View, TextInput, SafeAreaView, Alert, Image,ScrollView } from 'react-native'
import { Grid, Col } from 'react-native-easy-grid'
import { CheckBox,Button, SearchBar,ListItem } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
console.disableYellowBox = true;
import axios from 'axios';
import { NavigationEvents } from 'react-navigation';
import AwesomeAlert from 'react-native-awesome-alerts';
import Icon from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { bool } from 'prop-types';

import { JSHash, JSHmac, CONSTANTS } from "react-native-hash";



export default class ManageUser extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        search:'',
        accountlist:[],//for show
        account_data:[], //from db
        name:'',
        email:'',
        password:'',
        adminCheck:false,
        showPass:false,
        showAlert:false,
        ip_server:'',
        port_server:'',
        showFormatAlert:false,
        showAddAlert:false,
        showDupAlert:false,
        toRefresh:false,
        help:false

      };
    }

showDupAlert = () => {
  this.setState({
    showDupAlert: true
  });
};
hideDupAlert = () => {
  this.setState({
    showDupAlert: false
  });
};

    showFormatAlert = () => {
      this.setState({
        showFormatAlert: true
      });
    };
    hideFormatAlert = () => {
      this.setState({
        showFormatAlert: false
      });
    };

showAddAlert = () => {
  this.setState({
    showAddAlert: true
  });
};
hideAddAlert = () => {
  this.setState({
    showAddAlert: false
  });
};
    async componentDidMount() {
      
      //console.log("HI from manage")

      const config = {
        method: 'get',
        url: 'http://'+window.ip_server+':'+window.port_server+'/getAllUser'
      }
  
      let res = await axios(config)
      let data = res.data

      var local_accountdata=[]

      for(let i=0;i<data.length;i++){
          local_accountdata.push({})
          local_accountdata[i].title=data[i].name
          local_accountdata[i].icon="person"
          local_accountdata[i].subtitle=data[i].email
          local_accountdata[i].rightSubtitle=data[i].id
            if(data[i].role=='admin'){
                local_accountdata[i].rightTitle='Admin'
            }else{
                local_accountdata[i].rightTitle='User'
            }
      }

    //   console.log(local_accountdata)
      this.setState({port_server:window.port_server})
      this.setState({account_data:local_accountdata})


  }
  updateSearch = search => {
    
    
    this.setState({ search });

    var indexlist=[]

    for(var i=0;i<this.state.account_data.length;i++){
        if(this.state.account_data[i].title.includes(search)){
            indexlist.push(i)
        }
    }
    if(indexlist.length==0){
      for(var i=0;i<this.state.account_data.length;i++){
        if(this.state.account_data[i].subtitle.includes(search)){
            indexlist.push(i)
        }
    }

    }
    
    //console.log(indexlist)
    
    indexlist.reverse()
    var local_accountlist=[]

    for(let i=0;i<indexlist.length;i++){
        if(indexlist.length!=0){
            local_accountlist.push({})
            local_accountlist[i].title=this.state.account_data[indexlist[i]].title
            local_accountlist[i].icon=this.state.account_data[indexlist[i]].icon
            local_accountlist[i].subtitle=this.state.account_data[indexlist[i]].subtitle
            local_accountlist[i].rightSubtitle=this.state.account_data[indexlist[i]].rightSubtitle
            local_accountlist[i].rightTitle=this.state.account_data[indexlist[i]].rightTitle
        }

    }
    this.setState({accountlist:local_accountlist})

}
  
cancelSearch = search => {
    this.setState({accountlist:[]})
    
  };
clearSearch = search => {
    this.setState({accountlist:[]})
    //this.clearAndUpdate()
    //this.clearSearch
  
};


async refresh(){
  //Alert.alert("eee")

  const config = {
    method: 'get',
    url: 'http://'+window.ip_server+':'+window.port_server+'/getAllUser'
  }

  let res = await axios(config)
  let data = res.data

  var local_accountdata=[]

  for(let i=0;i<data.length;i++){
      local_accountdata.push({})
      local_accountdata[i].title=data[i].name
      local_accountdata[i].icon="person"
      local_accountdata[i].subtitle=data[i].email
      local_accountdata[i].rightSubtitle=data[i].id
        if(data[i].role=='admin'){
            local_accountdata[i].rightTitle='Admin'
        }else{
            local_accountdata[i].rightTitle='User'
        }
  }

//   console.log(local_accountdata)
  this.setState({port_server:window.port_server})
  this.setState({account_data:local_accountdata})

}

// async clearAndUpdate(){

  
  // let res = await axios.post('http://'+window.ip_server+':'+window.port_server+'/getUserDetail', {
       
  //       id: window.update_user_key
      
  //   })

  //   let data = res.data

  //   var local_accountlist = this.state.account_data
  //   for(let i=0;i<local_accountlist;i++){
  //     if(window.update_user_key==local_accountlist[i].rightSubtitle){
  //         local_accountlist[i].title=data[0].name
  //         local_accountlist[i].subtitle=data[0].email
  //         if(data[0].role=='admin'){
  //           local_accountlist[i].rightTitle='Admin'
  //         }else{
  //           local_accountlist[i].rightTitle='User'
  //         }
          
  //         local_accountlist[i].rightSubtitle=data[0].id
  //     }
  //   }
  //   this.setState({account_data:local_accountlist})

//  }

deleteAccount = key =>{

    window.deleteAccountKey=key
    this.showAlert()
    
  
}


async toDeleteAccount(key){
  
    var url = 'http://'+window.ip_server+':'+window.port_server+'/deleteUser';
    axios.post(url,{
        id:key
    })
    .then(function (response) {
    console.log(response);
    })
    .catch(function (error) {
    console.log(error);
    });
    //console.log(this.state.accountlist)
    //this.clearSearch()

    var local_accountlist =this.state.accountlist
    for(let i=0;i<local_accountlist.length;i++){
        if(local_accountlist[i].rightSubtitle==key){
            local_accountlist.splice(i,1)
        }
    }
    this.setState({accountlist:local_accountlist})

    var local_accountdata = this.state.account_data
    for(let i=0;i<local_accountdata.length;i++){
        if(key==local_accountdata[i].rightSubtitle){
            local_accountdata.splice(i,1)
        }
    }

    this.setState({account_data:local_accountdata})




  }
  helpShow =()=>{
    this.setState({help:true})
  }
  helpHide = ()=>{
    this.setState({help:false})
  }

showAlert = () => {
    this.setState({
      showAlert: true
    });
  };
  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };
    
    goToAccount = () => {
      this.props.navigation.navigate('Account')
    }

  handleServerSettingSubmit =()=>{
      
      window.ip_server=this.state.ip_server
      window.port_server=this.state.port_server
      
  }
  validatePass(pass) //return true when correct pass format
      {
        var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
        //console.log(re.test(pass))
        return re.test(pass);
      }

    handleSubmit =()=>{
        //console.log(this.state.name)
        if(this.validateEmail(this.state.email)){
          if(this.state.password!=''&&this.state.name!=''){
            if(this.validatePass(this.state.password)){
              this.addUser()
            }else{
              this.showFormatAlert()
            }
          }
                
        }else{
            this.showFormatAlert()
        }
    }
    async addUser(){

        //validate password here if you want to!! 
        let hash_input_pass = await JSHash(this.state.password, CONSTANTS.HashAlgorithms.sha256)
        let role

        if(this.state.adminCheck){
            role='admin'
        }else{
            role='user'
        }

        let res = await axios.post('http://'+window.ip_server+':'+window.port_server+'/addUser', {
          
          name:this.state.name,
          email:this.state.email,
          password:hash_input_pass,
          role:role
        
        })
  
        let data = res.data
        
        if(data){
            this.showAddAlert()
            let resGetID = await axios.post('http://'+window.ip_server+':'+window.port_server+'/authenEmail', {
          
          
          email:this.state.email,
          
        
        })

        let dataGetID = resGetID.data



        var local_accountdata = this.state.account_data
        //console.log(data[0].id)
        
        local_accountdata.push({})
        local_accountdata[local_accountdata.length-1].title=this.state.name
        local_accountdata[local_accountdata.length-1].icon="person"
        local_accountdata[local_accountdata.length-1].subtitle=this.state.email
        local_accountdata[local_accountdata.length-1].rightSubtitle=dataGetID[0].id
        if(role=='admin'){
          local_accountdata[local_accountdata.length-1].rightTitle='Admin'
        }else{
          local_accountdata[local_accountdata.length-1].rightTitle='User'
        }
        this.setState({account_data:local_accountdata})
        }else{
            this.showDupAlert()
        }

        
    }
    // async refresh(){
    //   //Alert.alert('refreshed')
    //   let res = await axios.post('http://'+window.ip_server+':'+window.port_server+'/getUserDetail', {
       
    //     id: window.update_user_key
      
    // })

    // let data = res.data

    // var local_accountlist = this.state.account_data
    // for(let i=0;i<local_accountlist;i++){
    //   if(window.update_user_key==local_accountlist[i].rightSubtitle){
    //       local_accountlist[i].title=data[0].name
    //       local_accountlist[i].subtitle=data[0].email
    //       if(data[0].role=='admin'){
    //         local_accountlist[i].rightTitle='Admin'
    //       }else{
    //         local_accountlist[i].rightTitle='User'
    //       }
          
    //       local_accountlist[i].rightSubtitle=data[0].id
    //   }
    // }
    // this.setState({account_data:local_accountlist})

    // }

    validateEmail(email) //return true when correct email format
      {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //console.log(re.test(email))
        return re.test(email);
      }

    updateUser=(key)=>{
        window.update_user_key=key
        this.props.navigation.navigate('UpdateUser')
    }
    render(){
      
        const { search,showAlert,showFormatAlert,showAddAlert,showDupAlert,help } = this.state;
      return(
        <SafeAreaView style={styles.container}>
             {/* <NavigationEvents
                onDidFocus={() => this.refresh()}
                /> */}
          <View style={styles.main}>
            <View style={styles.up}>
              <View style={styles.up_line}>
              <View style={styles.up_left}>
                      <Ionicons.Button
                        name={'ios-arrow-back'}
                        size={hp('3')}
                        color='#000000'
                        backgroundColor='#fff'
                        onPress={this.goToAccount}
                        title="back"
                      />
                </View>
              <View style={styles.up_right}>
                    <Text style={styles.head}>Administrator Page</Text>
              </View>
              <View style={styles.up_left}>
              <CheckBox
                  checkedIcon={<Image style={{width:wp(1.8),height:wp(1.8),marginLeft:wp('3')}} source={require('../assets/refresh.png')}/>}
                  uncheckedIcon={<Image style={{width:wp(1.8),height:wp(1.8),marginLeft:wp('3')}} source={require('../assets/refresh.png')} />}
                  checked={this.state.toRefresh}
                  onPress={() => this.refresh()}
                  //containerStyle={{alignItems:'flex-end',alignSelf:'flex-end',backgroundColor:'black',width:wp(40),justifyContent:'flex-end'}}
                  /> 
              
              </View>
            </View>
            </View>
          </View>
            <Grid>
              <Col size={40} style={styles.left_content}>
              <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
                    <View style={{flexDirection:'column'}}>
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(2)}}>
                            <View>
                                <Text style={{fontSize:hp('2.6')}}>ADD new user</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'flex-start',margin:wp(1)}}>
                                <View>
                                <Text style={{fontSize:hp('2.6')}}>name</Text>
                                </View>
                                <View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('28'),height:hp('4.4'),borderColor:'black',borderColor:'grey',marginLeft:wp(2)}}
                                    onChangeText={(name) => this.setState({name})}
                                    fontSize={hp('2.6')}
                                    value={this.state.name}
                                    onSubmitEditing={() => { this.t1.focus(); }}
                                    blurOnSubmit={false}
                                /> 
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'flex-start',margin:wp(1)}}>
                                <View>
                                <Text style={{fontSize:hp('2.6')}}>email</Text>
                                </View>
                                <View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('28'),height:hp('4.4'),borderColor:'black',borderColor:'grey',marginLeft:wp(2)}}
                                    onChangeText={(email) => this.setState({email})}
                                    fontSize={hp('2.6')}
                                    value={this.state.email}
                                     ref={(input) => { this.t1 = input; }}
                                     onSubmitEditing={() => { this.t2.focus(); }}
                                    blurOnSubmit={false}
                                /> 
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'flex-start',margin:wp(1)}}>
                                <View>
                                <Text style={{fontSize:hp('2.6')}}>password</Text>
                                
                                
                                </View>
                                <CheckBox
                                  checkedIcon={<Image style={{width:wp(1.8),height:wp(1.8),marginTop:wp(-1),marginRight:wp(-1),marginLeft:wp(-1)}} source={require('../assets/help.png')}/>}
                                  uncheckedIcon={<Image style={{width:wp(1.8),height:wp(1.8),marginTop:wp(-1),marginRight:wp(-1),marginLeft:wp(-1)}} source={require('../assets/help.png')} />}
                                  checked={this.state.help}
                                  onPress={() => this.helpShow()}
                                  //containerStyle={{alignItems:'flex-end',alignSelf:'flex-end',backgroundColor:'black',width:wp(40),justifyContent:'flex-end'}}
                                  /> 
                                <View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20.3'),height:hp('4.4'),borderColor:'black',borderColor:'grey',marginLeft:wp(2)}}
                                    onChangeText={(password) => this.setState({password})}
                                    fontSize={hp('2.6')}
                                    value={this.state.password}
                                    ref={(input) => { this.t2 = input; }}
                                    blurOnSubmit={false}
                                    secureTextEntry={!this.state.showPass}
                                    
                                /> 
                                 </View>
                                 
                        </View>
                        {/* <View style={{flexDirection:'row',justifyContent:'flex-start',margin:wp(1)}}><Text style={{fontSize:hp('2')}}>Note: passwords must have at least 8 characters and contain the following: </Text></View>
                        <View style={{flexDirection:'row',justifyContent:'flex-start',margin:wp(1)}}><Text style={{fontSize:hp('2')}}>1. uppercase letters</Text></View>
                        <View style={{flexDirection:'row',justifyContent:'flex-start',margin:wp(1)}}><Text style={{fontSize:hp('2')}}>2. lowercase letters</Text></View>
                        <View style={{flexDirection:'row',justifyContent:'flex-start',margin:wp(1)}}><Text style={{fontSize:hp('2')}}>3. numbers</Text></View> */}
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(1)}}>
                                <View>
                                    <CheckBox 
                                        title='show password'
                                        checked={this.state.showPass}
                                        onPress={() => this.setState({showPass:!this.state.showPass})}
                                        size={wp('2')}
                                        textStyle={{
                                            fontSize:hp('2.6')
                                        }}
                                    />
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(1)}}>
                                <View>
                                    <CheckBox 
                                        title='admin role'
                                        checked={this.state.adminCheck}
                                        onPress={() => this.setState({adminCheck:!this.state.adminCheck})}
                                        size={wp('2')}
                                        textStyle={{
                                            fontSize:hp('2.6')
                                        }}
                                    />
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center',marginBottom:wp(2)}}>

                            <Button
                                onPress={this.handleSubmit}
                                title='ADD User'
                                containerStyle={{
                                width:wp('10')
                                }}
                            />

                        </View>
                        {/* <View style={{justifyContent:'center',borderColor:'black',borderBottomWidth:StyleSheet.hairlineWidth}}>

                        </View>
                        
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(2)}}>
                            <View>
                                <Text style={{fontSize:hp('2.6'),color:'blue'}}>Server Setting</Text>
                            </View>
                        </View>
                          <View style={{flexDirection:'row',justifyContent:'flex-start',margin:wp(1)}}>
                                <View>
                                <Text style={{fontSize:hp('2.6'),color:'blue'}}>IP</Text>
                                </View>
                                <View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('30'),height:hp('4.4'),borderColor:'black',borderColor:'grey',marginLeft:wp(2)}}
                                    onChangeText={(ip_server) => this.setState({ip_server})}
                                    fontSize={hp('2.6')}
                                    value={this.state.ip_server}
                                     ref={(input) => { this.t3 = input; }}
                                     onSubmitEditing={() => { this.t4.focus(); }}
                                    blurOnSubmit={false}
                                /> 
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'flex-start',margin:wp(1)}}>
                                <View>
                                <Text style={{fontSize:hp('2.6'),color:'blue'}}>port</Text>
                                </View>
                                <View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('28'),height:hp('4.4'),borderColor:'black',borderColor:'grey',marginLeft:wp(2)}}
                                    onChangeText={(port_server) => this.setState({port_server})}
                                    fontSize={hp('2.6')}
                                    value={this.state.port_server}
                                     ref={(input) => { this.t4 = input; }}
                                     onSubmitEditing={() => { this.t3.focus(); }}
                                    blurOnSubmit={false}
                                /> 
                                 </View>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'center',margin:wp(1)}}>

                            <Button
                                onPress={this.handleServerSettingSubmit}
                                title='Confirm IP and port'
                                containerStyle={{
                                width:wp('20')
                                }}
                            />

                        </View> */}


                    </View>
                </KeyboardAwareScrollView>
              </Col>
              <Col size={60} style={styles.right_content}> 
                    <SearchBar
                      placeholder="Search by name or email..."
                      onChangeText={this.updateSearch}
                      onCancel={this.cancelSearch}
                      onClear={this.clearSearch}
                      value={search}
                      lightTheme
                      platform="ios"
                    />
                     <ScrollView style={styles.scrollView}>
                      {
                        this.state.accountlist.map((item, i) => (
                          <ListItem
                            key={i}
                            onPress={() => this.updateUser(item.rightSubtitle)}
                            onLongPress={() => this.deleteAccount(item.rightSubtitle)}
                            title={item.title}
                            subtitle={item.subtitle}
                            leftIcon={{ name: item.icon }}
                            rightSubtitle={item.rightTitle}
                            containerStyle={{
                            }}
                            titleStyle={{
                                  fontSize:hp('2.6%')
                            }}
                            subtitleStyle={{
                                  fontSize:hp('2.6%'),
                                  color:'grey'
                            }}
                            bottomDivider={true}
                            chevron
                          />
                        ))
                      }
                  </ScrollView>

              </Col>
            </Grid>
            <AwesomeAlert
          show={help}
          showProgress={false}
          title="Password rules"
          titleStyle={{fontSize:hp('4.6')}}
          messageStyle={{fontSize:hp('3.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message="Passwords must have at least 8 characters and contain the following: uppercase letters, lowercase letters and numbers."
          
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="blue"
          // cancelButtonColor="#606060"
          onCancelPressed={() => {
            // window.deleteKey=''
            // this.hideAlert();
          }}
          onConfirmPressed={() => {
            // this.toDeleteRecord(window.deleteKey)
             this.helpHide()
          }}
        />
            <AwesomeAlert
                show={showAlert}
                showProgress={false}
                title="Are you sure to delete this user?"
                titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
                message=""
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText="Cancel"
                confirmText="Delete"
                confirmButtonColor="red"
                cancelButtonColor="#606060"
                onCancelPressed={() => {
                    window.deleteKey=''
                    this.hideAlert();
                }}
                onConfirmPressed={() => {
                    this.toDeleteAccount(window.deleteAccountKey)
                    this.hideAlert()
                }}
                />

          <AwesomeAlert
          show={showFormatAlert}
          showProgress={false}
          title="The email format is incorrect or the password does not follow the rules."
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="red"
          // cancelButtonColor="#606060"
          onCancelPressed={() => {
            // window.deleteKey=''
            // this.hideAlert();
          }}
          onConfirmPressed={() => {
            // this.toDeleteRecord(window.deleteKey)
             this.hideFormatAlert()
          }}
        />

<AwesomeAlert
          show={showAddAlert}
          showProgress={false}
          title="User was created successfully."
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="blue"
          // cancelButtonColor="#606060"
          onCancelPressed={() => {
            // window.deleteKey=''
            // this.hideAlert();
          }}
          onConfirmPressed={() => {
            // this.toDeleteRecord(window.deleteKey)
             this.hideAddAlert()
          }}
        />

<AwesomeAlert
          show={showDupAlert}
          showProgress={false}
          title="This email or password already exists."
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="red"
          // cancelButtonColor="#606060"
          onCancelPressed={() => {
            // window.deleteKey=''
            // this.hideAlert();
          }}
          onConfirmPressed={() => {
            // this.toDeleteRecord(window.deleteKey)
             this.hideDupAlert()
          }}
        />
      </SafeAreaView>
      )
    }
  
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        
      },
      deleteAndRecoverAll:{
          flexDirection:'row',
          backgroundColor:'#f9f9f9',
          justifyContent:'center'
    
      },
      head:{
        fontSize:hp('2.6%'),
        fontWeight:'bold',
    
      },
      main:{
        flexDirection:'row'
    
      },
      up:{
        flexDirection:'row',
        backgroundColor:'#fff',
        
      },
      up_left:{
        justifyContent:'flex-start',
        alignItems:'flex-start',
        //marginLeft:wp(3),
        width:wp('10%'),
      },
      up_line:{
        flexDirection:'row',
        
      },
      up_right:{
        width:wp('80%'),
        justifyContent:'center',
        alignItems:'center',
      },
      down:{
        flexDirection:'column',
        backgroundColor:'#f9f9f9',
      },
      left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }
  
  })