import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput,Image } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
// import { TextInput } from 'react-native-gesture-handler';
import { Button, CheckBox } from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Html5Entities } from 'html-entities'; 
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
console.disableYellowBox = true;
export default class Form4 extends React.Component {

goToForm3 = () => this.props.navigation.navigate('Form3')
goToForm5 = () => this.props.navigation.navigate('Form5')


constructor(props) {
    super(props);
    this.state = {
      normal_formula:false,
      v_Na:false,
      v_K:false,
      v_HCO3:false,
      v_other:false,

      physician:'',
      machine:'',
      self_tpb:'',
      dialyzer:'',
      AS:'',
      Kuf:'',
      TCV:'',
      last_TCV:'',
      use_no:'',
      v_Na_unit:'',
      v_K_unit:'',
      v_HCO3_unit:'',
      v_other_desc:''
    };
}

async componentDidMount (){
  this.setState({normal_formula:window.normal_formula})
  this.setState({v_Na:window.v_Na})
  this.setState({v_K:window.v_K})
  this.setState({v_HCO3:window.v_HCO3})
  this.setState({v_other:window.v_other})
  this.setState({physician:window.physician})
  this.setState({machine:window.machine})
  this.setState({self_tpb:window.self_tpb})
  this.setState({dialyzer:window.dialyzer})
  this.setState({AS:window.AS})
  this.setState({Kuf:window.Kuf})
  this.setState({TCV:window.TCV})
  this.setState({last_TCV:window.last_TCV})
  this.setState({use_no:window.use_no})
  this.setState({v_Na_unit:window.v_Na_unit})
  this.setState({v_K_unit:window.v_K_unit})
  this.setState({v_HCO3_unit:window.v_HCO3_unit})
  this.setState({v_other_desc:window.v_other_desc})
}
handleBack = ()=>{
    
    window.normal_formula=this.state.normal_formula
    window.v_Na=this.state.v_Na
    window.v_K=this.state.v_K
    window.v_HCO3=this.state.v_HCO3
    window.v_other=this.state.v_other
    window.physician=this.state.physician
    window.machine=this.state.machine
    window.self_tpb=this.state.self_tpb
    window.dialyzer=this.state.dialyzer
    window.AS=this.state.AS
    window.Kuf=this.state.Kuf
    window.TCV=this.state.TCV
    window.last_TCV=this.state.last_TCV
    window.use_no=this.state.use_no
    window.v_Na_unit=this.state.v_Na_unit
    window.v_K_unit=this.state.v_K_unit
    window.v_HCO3_unit=this.state.v_HCO3_unit
    window.v_other_desc=this.state.v_other_desc
    
    this.goToForm3()
}

handleSubmit = values => {
    
    window.normal_formula=this.state.normal_formula
    window.v_Na=this.state.v_Na
    window.v_K=this.state.v_K
    window.v_HCO3=this.state.v_HCO3
    window.v_other=this.state.v_other
    window.physician=this.state.physician
    window.machine=this.state.machine
    window.self_tpb=this.state.self_tpb
    window.dialyzer=this.state.dialyzer
    window.AS=this.state.AS
    window.Kuf=this.state.Kuf
    window.TCV=this.state.TCV
    window.last_TCV=this.state.last_TCV
    window.use_no=this.state.use_no
    window.v_Na_unit=this.state.v_Na_unit
    window.v_K_unit=this.state.v_K_unit
    window.v_HCO3_unit=this.state.v_HCO3_unit
    window.v_other_desc=this.state.v_other_desc
    
    this.goToForm5()
}
arabic = "&#13217;"; 
entities = new Html5Entities();
m2=()=>{
    
return <Text style={{fontSize:hp('3')}}>( {this.entities.decode(this.arabic)} )</Text>
}

  render() {
    return (
        <Formik
                initialValues={{physician:'',
                                machine:'',
                                self_tpb:'',
                                dialyzer:'',
                                AS:'',
                                Kuf:'',
                                TCV:'',
                                last_TCV:'',
                                use_no:'',
                                v_Na_unit:'',
                                v_K_unit:'',
                                v_HCO3_unit:'',
                                v_other_desc:''}}
                onSubmit={values => {
                  this.handleSubmit(values)
                }}
                >
                {({ handleChange, values, handleSubmit}) => (
                    <SafeAreaView style={styles.container}>
                      <KeyboardAwareScrollView>
                    <View style={styles.up}>
                    <View style={styles.up_line}>
                    <View style={styles.up_left}>
                            <Ionicons.Button
                              name={'ios-arrow-back'}
                              size={hp('4')}
                              color='#000000'
                              backgroundColor='#fff'
                              onPress={this.handleBack}
                              title="back"
                            />
                      </View>
                    <View style={styles.up_right}>
                          <Text style={styles.head}>PRESCRIPTION</Text>
                    </View>
                    <View style={styles.up_right_page}>
                          <Text style={styles.head}>4/11</Text>
                    </View>
                  </View>
                  </View>
                  <Grid>
                      <Col size={25} style={styles.left_content}>
                          <View style={{flexDirection:'column'}}>
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Physician</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(physician) => this.setState({physician})}
                                    value={this.state.physician}
                                    fontSize={hp('2.6')}
                                    onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                    blurOnSubmit={false}
                                />
                                 <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Machine</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(machine) => this.setState({machine})}
                                    value={this.state.machine}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.secondTextInput = input; }}
                                    onSubmitEditing={() => { this.thirdTextInput.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Self-Test pass by</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(self_tpb) => this.setState({self_tpb})}
                                    value={this.state.self_tpb}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.thirdTextInput = input; }}
                                    onSubmitEditing={() => { this.fourthTextInput.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Dialyzer</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(dialyzer) => this.setState({dialyzer})}
                                    value={this.state.dialyzer}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.fourthTextInput = input; }}
                                    onSubmitEditing={() => { this.fifthTextInput.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>AS {this.m2()}</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(AS) => this.setState({AS})}
                                    value={this.state.AS}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.fifthTextInput = input; }}
                                    onSubmitEditing={() => { this.sixthTextInput.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Kuf (80%)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(Kuf) => this.setState({Kuf})}
                                    value={this.state.Kuf}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.sixthTextInput = input; }}
                                    onSubmitEditing={() => { this.seventhTextInput.focus(); }}
                                    blurOnSubmit={false}
                                />
                                
                          </View>
                      </Col>
                      <Col size={25} style={styles.mid_content}>
                            <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>TCV (ml)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(TCV) => this.setState({TCV})}
                                    value={this.state.TCV}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.seventhTextInput = input; }}
                                    onSubmitEditing={() => { this.eighthTextInput.focus(); }}
                                    blurOnSubmit={false}
                                    
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Last TCV (ml)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(last_TCV) => this.setState({last_TCV})}
                                    value={this.state.last_TCV}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.eighthTextInput = input; }}
                                    onSubmitEditing={() => { this.ninthTextInput.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Use No.</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(use_no) => this.setState({use_no})}
                                    value={this.state.use_no}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.ninthTextInput = input; }}
                                    
                                />
                        </Col>
                      <Col size={50} style={styles.right_content}>
                        <View style={{flexDirection:'column'}}>
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Dialysate</Text>
                                </View>
                                <CheckBox
                                    checkedIcon={<Image style={{width:wp('50'),height:hp('9'),resizeMode:'contain'}} source={require('../assets/normal.png')}/>}
                                    uncheckedIcon={<Image style={{width:wp('50'),height:hp('9'),resizeMode:'contain'}} source={require('../assets/normal_un.png')}/>}
                                    checked={this.state.normal_formula}
                                    onPress={() => this.setState({normal_formula: !this.state.normal_formula})}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Variable</Text>
                                </View>
                                <CheckBox
                                    checkedIcon={<Image style={{width:wp('38'),height:hp('6'),resizeMode:'contain',marginLeft:wp('4')}} source={require('../assets/na.png')}/>}
                                    uncheckedIcon={<Image style={{width:wp('38'),height:hp('6'),resizeMode:'contain',marginLeft:wp('4')}} source={require('../assets/na_un.png')}/>}
                                    checked={this.state.v_Na}
                                    onPress={() => this.setState({v_Na: !this.state.v_Na})}
                                />
                                <TextInput
                                        style={styles.variableInput}
                                        onChangeText={(v_Na_unit) => this.setState({v_Na_unit})}
                                        placeholder="Please fill in volume of Na here..."
                                        fontSize={hp('2.6')}
                                        value={this.state.v_Na_unit}
                                        
                                    />
                                    <CheckBox
                                    checkedIcon={<Image style={{width:wp('38'),height:hp('6'),resizeMode:'contain',marginLeft:wp('4')}} source={require('../assets/k.png')}/>}
                                    uncheckedIcon={<Image style={{width:wp('38'),height:hp('6'),resizeMode:'contain',marginLeft:wp('4')}} source={require('../assets/k_un.png')}/>}
                                    checked={this.state.v_K}
                                    onPress={() => this.setState({v_K: !this.state.v_K})}
                                />
                                <TextInput
                                        style={styles.variableInput}
                                        onChangeText={(v_K_unit) => this.setState({v_K_unit})}
                                        placeholder="Please fill in volume of K here..."
                                        fontSize={hp('2.6')}
                                        value={this.state.v_K_unit}
                                        
                                    />
                                    <CheckBox
                                    checkedIcon={<Image style={{width:wp('38'),height:hp('6'),resizeMode:'contain',marginLeft:wp('4')}} source={require('../assets/hco3.png')}/>}
                                    uncheckedIcon={<Image style={{width:wp('38'),height:hp('6'),resizeMode:'contain',marginLeft:wp('4')}} source={require('../assets/hco_un.png')}/>}
                                    checked={this.state.v_HCO3}
                                    onPress={() => this.setState({v_HCO3: !this.state.v_HCO3})}
                                />
                                <TextInput
                                        style={styles.variableInput}
                                        onChangeText={(v_HCO3_unit) => this.setState({v_HCO3_unit})}
                                        placeholder="Please fill in volume of HCO3 here..."
                                        fontSize={hp('2.6')}
                                        value={this.state.v_HCO3_unit}
                                        
                                    />
                                    <CheckBox
                                    checkedIcon={<Image style={{width:wp('38'),height:hp('6'),resizeMode:'contain',marginLeft:wp('4')}} source={require('../assets/other.png')}/>}
                                    uncheckedIcon={<Image style={{width:wp('38'),height:hp('6'),resizeMode:'contain',marginLeft:wp('4')}} source={require('../assets/other_un.png')}/>}
                                    checked={this.state.v_other}
                                    onPress={() => this.setState({v_other: !this.state.v_other})}
                                />
                                <TextInput
                                        style={styles.variableInput}
                                        onChangeText={(v_other_desc) => this.setState({v_other_desc})}
                                        placeholder="Please fill in volume of Other here..."
                                        fontSize={hp('2.6')}
                                        value={this.state.v_other_desc}
                                        
                                    />
                                    <View style={{flexDirection:'row',justifyContent:'flex-end',padding:wp('2')}}>
                                        <Button
                                                    onPress={handleSubmit}
                                                    title='Next'
                                                    containerStyle={{
                                                    width:wp('10')
                                                    }}
                                                />
                                        </View>
                        </View>
                      </Col>
                  </Grid>
                  </KeyboardAwareScrollView>
                  </SafeAreaView>
                )}
        </Formik>
    )
}
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      
    },
    up_left:{
        justifyContent:'flex-start',
        alignItems:'flex-start',
        width:wp('10%'),
        
      },
      up_right_page:{
        justifyContent:'flex-end',
        alignItems:'flex-end',
        width:wp('10%'),
        padding:hp('2')
      },
      up_line:{
        flexDirection:'row',
        
      },
      up_right:{
        width:wp('80%'),
        justifyContent:'center',
        alignItems:'center',
      },
      head:{
        fontSize:hp('2.6%'),
        fontWeight:'bold',
    
      },
      variableInput:{
        borderBottomWidth:StyleSheet.hairlineWidth,
        width:wp('30'),
        height:hp('3.4'),
        marginLeft:hp('12'),
        borderColor:'grey'
        
      },
      left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }

})