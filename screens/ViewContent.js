import React from 'react'
import { StyleSheet, Text, View, Button, Dimensions, Image, ScrollView, SafeAreaView, Alert, TouchableNativeFeedbackBase,ActivityIndicator } from 'react-native'
import {Header} from 'react-native-elements'
import { TextInput } from 'react-native-gesture-handler'
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import { CheckBox, ListItem, SearchBar } from 'react-native-elements';
import {
  LineChart,
} from "react-native-chart-kit";
import { Table, TableWrapper,Rows, Cols, Cell } from 'react-native-table-component';
import {Row as Rowt} from 'react-native-table-component';
import {Col as Colt} from 'react-native-table-component';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { G } from 'react-native-svg'
import { Html5Entities } from 'html-entities';
import HtmlText from 'react-native-html-to-text';
import HTML from 'react-native-render-html';
import axios from 'axios';
import { object } from 'prop-types';
import { MenuProvider } from 'react-native-popup-menu';
 
console.disableYellowBox = true;
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
const chartConfig = {
  backgroundGradientFrom: "#ffffff",
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: "#08130D",
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
  barPercentage: 0.5,
  strokeWidth: 2, // optional, default 3
  decimalPlaces: 0,
};

//let text_color='blue';
//let medi_text_color='red';

export default class ViewContent extends React.Component {

  goToSearch = () => this.props.navigation.navigate('Search')
  goToHome = () => this.props.navigation.navigate('App')
  constructor(props) {
    super(props);
    this.state = {
        db_data:[],
        showLoad:true,
        text_color:'blue',
        medi_text_color:'red',
        Twidth:0,
        
        tableHead: ['time','','','','','','','','','','',''],
        tableTitle: ['O2 Sat', 'QB', 'VP', 'TMP', 'UFR', 'UF'],
        tableData: [
            //index 0 to 11 
            ['','','','','','','','','','','',''],//o2sat
            ['','','','','','','','','','','',''],//qb
            ['','','','','','','','','','','',''],//vp
            ['','','','','','','','','','','',''],//tmp
            ['','','','','','','','','','','',''],//ufr
            ['','','','','','','','','','','',''],//uf
      ],
      
       nurse_note:'',
      
       doctor_note:'',
      
      data:{
        labels:['time'],
        datasets: [
          {
            data: [0],//bp_up
            color: (opacity = 1) => `rgba(0, 130, 0, ${opacity})`,
            strokeWidth: 2
          },
          {
            data:[0],//bp_down
            color: (opacity = 1) => `rgba(0, 130, 0, ${opacity})`,
            strokeWidth: 2
            },
            {
            data:[0],//pr
            color: (opacity = 1) => `rgba(255, 0, 0, ${opacity})`,
            strokeWidth: 2
          },
          {
            data:[0],//rr
            color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`,
            strokeWidth: 2
            },
          
        ],
      },
    };
  }
  
  
  async componentDidMount() {
    

    //console.log(window.key)
    let res = await axios.post('http://'+window.ip_server+':'+window.port_server+'/getRecord', {
       
        record_id: window.key
      
    })

    let data = res.data
    //console.log(data)
    this.setState({db_data:data[0]})
    this.setState({nurse_note:data[0].nurse_note})
    this.setState({doctor_note:data[0].doctor_note})
    

    let local_tableHead = JSON.parse(data[0].tableHead)
    if(Array.isArray(local_tableHead) && local_tableHead.length){
        this.setState({tableHead:local_tableHead})
    }
    
  let local_tableSubmitTime = JSON.parse(data[0].tableSubmitTime)

    if(Array.isArray(local_tableSubmitTime) && local_tableSubmitTime.length){
      var minToChangeTableHead = local_tableSubmitTime[0].split(':')[1]
      if(minToChangeTableHead>30){
        this.setState({Twidth:wp('66')})
        //720

      }else{
        this.setState({Twidth:wp('61')})
        //660
      }
   }
   
    var localdata={
      labels: ['time'],
      datasets: [
      {
        data: [0],//bp_up
        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
        strokeWidth: 2
      },
      {
        data:[0],//bp_down
        color: (opacity = 1) => `rgba(0, 130, 0, ${opacity})`,
        strokeWidth: 2
        },
        {
        data:[0],//pr
        color: (opacity = 1) => `rgba(255, 0, 0, ${opacity})`,
        strokeWidth: 2
      },
      {
        data:[0],//rr
        color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`,
        strokeWidth: 2
        },
      ]
    }

    localdata.labels=JSON.parse(data[0].g_time)
    localdata.datasets[0].data=JSON.parse(data[0].g_bpup)
    localdata.datasets[1].data=JSON.parse(data[0].g_bpdown)
    localdata.datasets[2].data=JSON.parse(data[0].g_pr)
    localdata.datasets[3].data=JSON.parse(data[0].g_rr)
    
    this.setState({data:localdata})

    var localTableData=[
      //index 0 to 11 
      ['','','','','','','','','','','',''],//o2sat
      ['','','','','','','','','','','',''],//qb
      ['','','','','','','','','','','',''],//vp
      ['','','','','','','','','','','',''],//tmp
      ['','','','','','','','','','','',''],//ufr
      ['','','','','','','','','','','',''],//uf
      ]

    localTableData[0]=JSON.parse(data[0].tableO2sat)
    localTableData[1]=JSON.parse(data[0].tableQB)
    localTableData[2]=JSON.parse(data[0].tableVP)
    localTableData[3]=JSON.parse(data[0].tableTMP)
    localTableData[4]=JSON.parse(data[0].tableUFR)
    localTableData[5]=JSON.parse(data[0].tableUF)

    this.setState({tableData:localTableData})

    this.setState({showLoad:false})



  }
  
  toHomeOrSearch =() =>{
    if(window.fromHome){
        this.goToHome()
    }
    else{
        this.goToSearch()
    }
  }

arabic = "&#8451;"; 
//arabic = "<div><p><span>สีดำ </span><span style='color: #d23431;'>แดงแดง </span><span>ดำดำ </span><span style='color: #d23431;'>แดงแดง </span></p></div>"; 
entities = new Html5Entities();
co =()=>{
    return <Text style={styles.normalFont}>{this.entities.decode(this.arabic)}</Text>

}
arabic2 = "q&#772;"; 
q=()=>{
    return <Text style={styles.normalFont}>{this.entities.decode(this.arabic2)}</Text>
}
arabic3 = "&#13217;"; 
m2=()=>{
    
return <Text style={styles.normalFont}>{this.entities.decode(this.arabic3)}</Text>
}

showLoad=()=>{
  if(this.state.db_data.showLoad){
  return  <View style={{alignSelf:'center',marginTop:wp('10')}}><ActivityIndicator size="large" color="grey" /><Text>กำลังโหลดข้อมูล โปรดรอสักครู่</Text></View>
  } 
}
renderNurseNote=()=>{
  var htmlContent=this.state.nurse_note
  var htmlFakeContent='<div></div>'

    if(this.state.nurse_note.includes('<')){
      return <HTML html={htmlContent} baseFontStyle={{ fontSize:hp('2.2'),color:this.state.text_color}}/>
    }
    else{
      
      return <HTML html={htmlFakeContent} baseFontStyle={{ fontSize:hp('2.2'),color:this.state.text_color}}/>
    }

}
renderDoctorNote=()=>{
  var htmlContent=this.state.doctor_note
  var htmlFakeContent='<div></div>'
  //console.log(this.state.text_color)
  
      if(this.state.doctor_note.includes('<')){
        return <HTML html={htmlContent} baseFontStyle={{ fontSize:hp('2.2'),color:this.state.text_color}}/>
      }
      else{
        return <HTML html={htmlFakeContent} baseFontStyle={{ fontSize:hp('2.2'),color:this.state.text_color}}/>
      }

}

setColor=(color)=>{
  if(color=='black'){
    //console.log("black")
    this.setState({text_color:'black'})
    //this.renderDoctorNote()
  }
  if(color=='blue'){
    //console.log("blue")
    this.setState({text_color:'blue'})
  }
  if(color=='green'){
    //console.log("greeen")
    this.setState({text_color:'green'})
  }
  if(color=='red'){
    //console.log("red")
    this.setState({text_color:'red'})
  }

}
  
  render(){
    
    
    return (
      <MenuProvider>
      <SafeAreaView style={styles.container}>
        {this.showLoad()}
          <View style={styles.up}>
              <View style={styles.up_line}>
              <View style={styles.up_left}>
                      <Ionicons.Button
                        name={'ios-arrow-back'}
                        size={hp('4')}
                        color='#000000'
                        backgroundColor='#fff'
                        onPress={()=>{this.toHomeOrSearch()}}
                        title="back"
                      />
                </View>
                <View style={styles.up_right}>
                    <Text style={styles.head}></Text>
              </View>
              <View style={styles.up_right_page}>
                <View style={{flexDirection:'row'}}>
                    <Menu>
                    <MenuTrigger text='color setting ' customStyles={{triggerText:{color:this.state.text_color,fontSize:wp('1.8'),width:wp("14"),fontWeight:'bold'}}}/>
                    <MenuOptions customStyles={{optionWrapper: { height:wp(6),width:wp(8)}}}>
                      <MenuOption onSelect={() => this.setColor('black')} >
                          <Text style={{color: 'black',fontSize:wp(3)}}>black</Text>
                      </MenuOption>
                      <MenuOption onSelect={() => this.setColor('red')} >
                        <Text style={{color: 'red',fontSize:wp(3)}}>red</Text>
                      </MenuOption>
                      <MenuOption onSelect={() => this.setColor('green')} >
                        <Text style={{color: 'green',fontSize:wp(3)}}>green</Text>
                      </MenuOption>
                      <MenuOption onSelect={() => this.setColor('blue')} >
                        <Text style={{color: 'blue',fontSize:wp(3)}}>blue</Text>
                      </MenuOption>
                    </MenuOptions>
                  </Menu>
                  <Menu>
                    <MenuTrigger text='medicationl color setting ' customStyles={{triggerText:{color:this.state.medi_text_color,fontSize:wp('1.8'),width:wp("22"),fontWeight:'bold'}}}/>
                    <MenuOptions customStyles={{optionWrapper: { height:wp(6),width:wp(8)}}}>
                    <MenuOption onSelect={() => this.setState({medi_text_color:'black'})} >
                          <Text style={{color: 'black',fontSize:wp(3)}}>black</Text>
                      </MenuOption>
                      <MenuOption onSelect={() => this.setState({medi_text_color:'red'})} >
                        <Text style={{color: 'red',fontSize:wp(3)}}>red</Text>
                      </MenuOption>
                      <MenuOption onSelect={() => this.setState({medi_text_color:'green'})} >
                        <Text style={{color: 'green',fontSize:wp(3)}}>green</Text>
                      </MenuOption>
                      <MenuOption onSelect={() => this.setState({medi_text_color:'blue'})} >
                        <Text style={{color: 'blue',fontSize:wp(3)}}>blue</Text>
                      </MenuOption>
                    </MenuOptions>
                  </Menu>
                </View>
              </View>
            </View>
            </View>
      <Grid>
            <Row size={92} style={styles.main_content}>
            
            <ScrollView style={styles.scrollView}>
                <Row size={7.5} style={styles.row1}>
                  <Col size={50} style={styles.row1c1}>
                      <View style={{flexDirection: 'column'}}>
                          <View style={{flexDirection: 'row'}}>
                            <View>
                            <CheckBox
                                  title='Acute'
                                  checked={!!!!this.state.db_data.acute}
                                  checkedIcon='dot-circle-o'
                                  uncheckedIcon='circle-o'
                                  size={hp('1.6')}
                                  textStyle={{
                                    fontSize:hp('2.2')
                                  }}
                                  containerStyle={{
                                    width:wp('10'),
                                    
                                  }}
                                />
                              </View>
                               <View>
                                <CheckBox
                                    title='Chronic'
                                    checked={!!!!this.state.db_data.chronic}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    size={hp('1.6')}
                                    textStyle={{
                                    fontSize:hp('2.2')
                                    }}
                                    containerStyle={{
                                      width:wp('12'),
                                      
                                    }}
                                  />
                               </View>
                          <View style={{marginTop:hp('2')}}>
                                <Text style={styles.normalFont}>HD No: </Text>
                                
                          </View>
                          <View style={{marginTop:hp('2'),width:wp('16')}}>
                                <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.HD_no} </Text>
                          </View>
                        </View>
                          
                        <View style={{flexDirection: 'row'}}>
                            <View style={{marginLeft:wp('1'),marginTop:wp('1'),}}>
                                  <Text style={styles.normalFont}>Date: </Text>
                            </View>
                            <View style={{marginTop:wp('1'),width:wp('24')}}>
                                  <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.date} </Text>
                            </View>
                            <View style={{marginTop:wp('1'),}}>
                                    <Text style={styles.normalFont}>Ward: </Text>
                                    
                             </View>
                             <View style={{marginTop:wp('1'),width:wp('12')}}>
                             <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.ward} </Text>
                             </View>
                             </View>
                        <View style={{flexDirection: 'row'}}>
                              <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                    <Text style={styles.normalFont}>Diagnosis: </Text>
                              </View>
                              <View style={{marginTop:wp('1'),width:wp('40'),marginBottom:wp('1')}}>
                                  <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.diagnosis}</Text>
                              </View>
                          </View>
                          
                    </View>
                  </Col>
                  <Col size={50} style={styles.row1c2}>
                    <View style={{flexDirection:'column'}}> 
                    <View style={{flexDirection:'row'}}>
                      <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                          <Text style={styles.normalFont}>Disbursement </Text>
                      </View>
                      <View>
                        <CheckBox
                                    title='CGD'
                                    checked={!!!!this.state.db_data.CGD}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('2.2')
                                    }}
                                    containerStyle={{
                                      
                                      width:wp('8'),
                                    
                                      height:wp('4'),
                                    }}
                                  />
                      </View>
                          <View>
                                <CheckBox
                                  title='SSS'
                                  checked={!!!!this.state.db_data.SSS}
                                  checkedIcon='dot-circle-o'
                                  uncheckedIcon='circle-o'
                                  size={wp('1')}
                                  textStyle={{
                                    fontSize:hp('2.2')
                                  }}
                                  containerStyle={{
                                    width:wp('8'),
                                    
                                    height:wp('4'),
                                    
                                  }}
                                />
                          </View>
                          <View>
                                <CheckBox
                                  title='NHSO'
                                  checked={!!!!this.state.db_data.NHSO}
                                  checkedIcon='dot-circle-o'
                                  uncheckedIcon='circle-o'
                                  size={wp('1')}
                                  textStyle={{
                                    fontSize:hp('2.2')
                                  }}
                                  containerStyle={{
                                    width:wp('10'),
                                    
                                    height:wp('4'),
                                    
                                  }}
                                />
                        </View>
                      </View>
                      <View style={{flexDirection:'row'}}>
                      <View>
                              <CheckBox
                                  title='State Enterprises'
                                  checked={!!!!this.state.db_data.state_enterprise}
                                  checkedIcon='dot-circle-o'
                                  uncheckedIcon='circle-o'
                                  size={wp('1')}
                                  textStyle={{
                                    fontSize:hp('2.2')
                                  }}
                                  containerStyle={{
                                    width:wp('16'),
                                    marginLeft:wp('13'),
                                    height:wp('4'),
                                    
                                  }}
                                />
                        </View>
                        <View>
                              <CheckBox
                                  title='Self-pay'
                                  checked={!!!!this.state.db_data.self_pay}
                                  checkedIcon='dot-circle-o'
                                  uncheckedIcon='circle-o'
                                  size={wp('1')}
                                  textStyle={{
                                    fontSize:hp('2.2')
                                  }}
                                  containerStyle={{
                                    width:wp('12'),
                                    height:wp('4'),
                                    
                                  }}
                                  />
                        </View>

                      </View>
                      <View style={{flexDirection: 'row'}}>      
                                <View style={{marginLeft:wp('1'),marginBottom:wp('1'),marginTop:wp('1'),}}>
                                      <Text style={styles.normalFont}>Notice: </Text>
                                      
                                </View>
                                <View style={{marginTop:wp('1'),marginBottom:wp('1'),width:wp('18')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.notice} </Text>
                                </View>
                                <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                                        <Text style={styles.normalFont}>Nuse assignment: </Text>
                                </View>
                                <View style={{marginTop:wp('1'),marginBottom:wp('1'),width:wp('10')}}>
                                      
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.nurse_assign} </Text>
                                </View>
                        
                        
                      </View>
                  </View>  
                  </Col>
                </Row>
                <Row size={5} style={styles.row2}>
                  <Col size={50} style={styles.row2c1}>
                        <View style={{flexDirection: 'row',marginTop:wp('0.5'),marginBottom:wp('0.5'),alignSelf:'center'}}>
                              <Text style={styles.normalFont}>ASSESSMENT per HD</Text>
                        </View>
                  </Col>
                  <Col size={50} style={styles.row2c2}>
                        <View style={{flexDirection: 'row',marginTop:wp('0.5'),marginBottom:wp('0.5'),alignSelf:'center'}}>
                              <Text style={styles.normalFont}>PRESCRIPTION</Text>
                        </View>
                  </Col>
                </Row>
                <Row size={40} style={styles.row3}>
                    <Col size={50} style={styles.row3c1}>
                      <Row size={60} style={styles.row3c1r1}>
                        <Col size={50} style={{borderRightWidth:StyleSheet.hairlineWidth}}>
                          <View style={{flexDirection:'column'}}>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_e}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}     
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_e_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}      
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Edema</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_lc}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5')
                                    }}        
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_lc_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                        width:wp('5'),
                                        marginLeft:wp('-1')
                                    }}        
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={{fontSize:hp('2')}}>Lung crepitation</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_env}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}     
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_env_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}        
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={{fontSize:hp('1.9')}}>Engorge neck vein</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_cd}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}       
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_cd_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}         
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={{fontSize:hp('2')}}>Chest discomfort</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_d}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}       
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_d_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}        
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Dyspnea</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_cp}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}     
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_cp_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}       
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Chest pain</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_p}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}     
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_p_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}        
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Pale</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_f}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}      
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_f_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}       
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Fever/chill</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!!!this.state.db_data.aph_nv}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}      
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_nv_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}        
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={{fontSize:hp('1.9')}}>Nausea /Vomiting</Text>
                                </View>
                              </View>
                          </View>
                        </Col>
                        <Col size={50}>
                          <View style={{flexDirection:'column'}}>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_h}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}        
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_h_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}        
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Headache</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_bt}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}      
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_bt_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}         
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={{fontSize:hp('1.9')}}>Bleeding tendency</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_i}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}       
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_i_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}        
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Itching</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_ano}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}       
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_ano_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}        
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Anorexia</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_anx}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}      
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_anx_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}        
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Anxiety</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_sd}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}       
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_sd_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}         
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={{fontSize:hp('1.9')}}>Sleep disturbance</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_c}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}      
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_c_n}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}         
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Constipation</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Y'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_ps}
                                    size={wp('1')}
                                    textStyle={{
                                      fontSize:hp('1.8')
                                  }}
                                  containerStyle={{
                                    width:wp('5')
                                  }}     
                                />
                                <CheckBox
                                    title='N'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_ps_n}
                                    size={wp('1')}
                                    containerStyle={{
                                      width:wp('5'),
                                      marginLeft:wp('-1')
                                  }}       
                                />
                                <View style={{marginTop:hp('2')}}>
                                  <Text style={styles.normalFont}>Pain score</Text>
                                </View>
                              </View>
                              <View style={{flexDirection:'row'}}>
                                <CheckBox
                                    title='Other'
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    checked={!!this.state.db_data.aph_other}
                                    size={wp('1')}
                                    textStyle={{
                                        fontSize:hp('1.8')
                                    }}
                                    containerStyle={{
                                      width:wp('8')
                                    }}        
                                />
                                <View style={{marginTop:hp('2'),marginBottom:wp('1'),width:wp('12')}}>
                                  <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.aph_other_desc}</Text>
                                </View>
                              </View>
                            
                            </View>
                        </Col>
                      </Row>
                      <Row size={40} style={styles.row3c1r2}>
                        <View style={{flexDirection:'column'}}>
                                  <View style={{flexDirection:'row'}}>
                                      <View style={{marginTop:hp('2'),marginLeft:wp('1')}}>
                                        <Text style={styles.normalFont}>VASCULA ACCESS</Text>
                                      </View>
                                      <View>
                                      <CheckBox
                                        title='DLC'
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        checked={!!this.state.db_data.DLC}
                                        size={wp('1')}
                                        textStyle={{
                                            fontSize:hp('2.2')
                                        }}
                                        containerStyle={{
                                        }}        
                                      />
                                      </View>
                                      <View>
                                      <CheckBox
                                        title='PERM'
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        checked={!!this.state.db_data.PERM}
                                        size={wp('1')}
                                        textStyle={{
                                            fontSize:hp('2.2')
                                        }}
                                        containerStyle={{
                                        }}        
                                      />
                                      </View>
                                      </View>
                                
                                  <View style={{flexDirection:'row'}}>
                                      <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                                        <Text style={styles.normalFont}>Catheter : site: </Text>
                                      </View>
                                      <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                                      <Text style={{color:this.state.text_color ,fontSize:hp('2.2'),width:wp('5')}}> {this.state.db_data.catheter_site} </Text>
                                      </View>
                                      <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                                        <Text style={styles.normalFont}>Insertion Date: </Text>
                                      </View>
                                      <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                                      <Text style={{color:this.state.text_color ,fontSize:hp('2.2'),width:wp('16')}}> {this.state.db_data.insertion_date} </Text>
                                      </View>

                                  </View>

                                  <View style={{flexDirection:'row'}}>
                                  <View>
                                      <CheckBox
                                        title='AVF'
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        checked={!!this.state.db_data.AVF}
                                        size={wp('1')}
                                        textStyle={{
                                            fontSize:hp('2.2')
                                        }}
                                        containerStyle={{
                                        }}        
                                      />
                                      </View>
                                      <View>
                                      <CheckBox
                                        title='AVG'
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        checked={!!this.state.db_data.AVG}
                                        size={wp('1')}
                                        textStyle={{
                                            fontSize:hp('2.2')
                                        }}
                                        containerStyle={{
                                        }}        
                                      />
                                      </View>
                                      <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                                        <Text style={styles.normalFont}>Needle No. </Text>
                                      </View>
                                      <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.needle_no} </Text>
                                      </View>
                                  </View>
                                  <View style={{flexDirection:'row'}}>
                                  <View>
                                      <CheckBox
                                        title='Thrill'
                                        checked={!!this.state.db_data.thrill}
                                        size={wp('1')}
                                        textStyle={{
                                            fontSize:hp('2.2')
                                        }}
                                        containerStyle={{
                                        }}        
                                      />
                                      </View>
                                      <View>
                                      <CheckBox
                                        title='Bruit'
                                        checked={!!this.state.db_data.bruit}
                                        size={wp('1')}
                                        textStyle={{
                                            fontSize:hp('2.2')
                                        }}
                                        containerStyle={{
                                        }}        
                                      />
                                      </View>
                                  </View>
                                  <View style={{flexDirection:'row'}}>
                                    <View style={{marginTop:wp('1'),marginLeft:wp('1'),marginBottom:wp('1')}}>
                                        <Text style={styles.normalFont}>Characteristics found: </Text>
                                      </View>
                                      <View style={{marginTop:wp('1'),marginLeft:wp('1'),marginBottom:wp('1'),width:wp('30')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.characteristics_found}</Text>
                                      </View>
                                  </View>
                            </View>
                      </Row>
                    </Col>
                    <Col size={50} style={styles.row3c2}>
                      <View style={{flexDirection:'column'}}>
                          <View style={{flexDirection:'row'}}>
                                  <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}>Physician: </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),width:wp('7')}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.physician}</Text>
                                    </View>
                                    <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}>Machine: </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),width:wp('5')}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.machine}</Text>
                                    </View>
                                    <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}>Self-Test pass by: </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),width:wp('6.5')}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.self_tpb}</Text>
                                    </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                                  <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}>Dialyzer: </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),width:wp('8')}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.dialyzer}</Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),}}>
                                      <Text style={styles.normalFont}>  AS: </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),width:wp('4')}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.AS_m2} </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),}}>
                                          <Text> {this.m2()} </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),}}>
                                      <Text style={styles.normalFont}>Kuf: </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),width:wp('4')}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.Kuf} </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),}}>
                                      <Text style={styles.normalFont}>  80%  </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),}}>
                                      <Text style={styles.normalFont}>TCV: </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),width:wp('4')}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.TCV} </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),width:wp('3')}}>
                                      <Text style={styles.normalFont}>  ml</Text>
                                    </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                                    <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                                      <Text style={styles.normalFont}>Last TCV: </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.last_TCV} </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),}}>
                                      <Text style={styles.normalFont}>  ml  </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                                      <Text style={styles.normalFont}>Use No. </Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.use_no} </Text>
                                    </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                                    <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                                      <Text style={styles.normalFont}>Dialysate </Text>
                                    </View>
                                    <CheckBox
                                        checkedIcon={<Image style={{resizeMode:'contain',width:wp('30'),height:hp('10')}} source={require('../assets/normal.png')}/>}
                                        uncheckedIcon={<Image style={{resizeMode:'contain',width:wp('30'),height:hp('10')}} source={require('../assets/normal_un.png')}/>}
                                        checked={!!this.state.db_data.normal_formula}
                                        textStyle={{
                                        }}    
                                        containerStyle={{
                                        }}         
                                    />
                          </View>
                          <View style={{flexDirection:'row'}}>
                                    <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                                      <Text style={styles.normalFont}>Variable </Text>
                                    </View>
                                    <CheckBox
                                        checkedIcon={<Image style={{resizeMode:'contain',width:wp('26'),height:hp('6')}} source={require('../assets/na.png')}/>}
                                        uncheckedIcon={<Image style={{resizeMode:'contain',width:wp('26'),height:hp('6')}} source={require('../assets/na_un.png')}/>}
                                        checked={!!this.state.db_data.v_Na}
                                        
                                        textStyle={{
                                        }}    
                                        containerStyle={{
                                          marginTop:wp('-1'),
                                          marginLeft:wp('2')
                                        }}         
                                    />
                                    <View style={{marginTop:wp('1'),}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.v_Na_unit}</Text>
                                    </View>
                                    <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                                      <Text style={styles.normalFont}> mEq/L</Text>
                                    </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                                    
                                    <CheckBox
                                        checkedIcon={<Image style={{resizeMode:'contain',width:wp('26'),height:hp('6')}} source={require('../assets/k.png')}/>}
                                        uncheckedIcon={<Image style={{resizeMode:'contain',width:wp('26'),height:hp('6')}} source={require('../assets/k_un.png')}/>}
                                        checked={!!this.state.db_data.v_K}
                                        
                                        textStyle={{
                                        }}    
                                        containerStyle={{
                                          marginTop:wp('-2'),
                                          marginLeft:wp('9.4')
                                        }}         
                                    />
                                    <View style={{marginTop:0,}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.v_K_unit}</Text>
                                    </View>
                                    <View style={{marginTop:0,marginLeft:wp('1')}}>
                                      <Text style={styles.normalFont}> mEq/L</Text>
                                    </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                                    
                                    <CheckBox
                                        checkedIcon={<Image style={{resizeMode:'contain',width:wp('26'),height:hp('6')}} source={require('../assets/hco3.png')}/>}
                                        uncheckedIcon={<Image style={{resizeMode:'contain',width:wp('26'),height:hp('6')}} source={require('../assets/hco_un.png')}/>}
                                        checked={!!this.state.db_data.v_HCO3}
                                        
                                        textStyle={{
                                        }}    
                                        containerStyle={{
                                          marginTop:wp('-2'),
                                          marginLeft:wp('9.4')
                                        }}         
                                    />
                                    <View style={{marginTop:0,}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.v_HCO3_unit}</Text>
                                    </View>
                                    <View style={{marginTop:0,marginLeft:wp('1')}}>
                                      <Text style={styles.normalFont}> mEq/L</Text>
                                    </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                                    
                                    <CheckBox
                                        checkedIcon={<Image style={{resizeMode:'contain',width:wp('26'),height:hp('6')}} source={require('../assets/other.png')}/>}
                                        uncheckedIcon={<Image style={{resizeMode:'contain',width:wp('26'),height:hp('6')}} source={require('../assets/other_un.png')}/>}
                                        checked={!!this.state.db_data.v_other}
                                        
                                        textStyle={{
                                        }}    
                                        containerStyle={{
                                          marginTop:wp('-2'),
                                          marginLeft:wp('9.4')
                                        }}         
                                    />
                                    <View style={{marginTop:0,}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.v_other_desc}</Text>
                                    </View>
                                    <View style={{marginTop:0,marginLeft:wp('1')}}>
                                      <Text style={styles.normalFont}> mEq/L</Text>
                                    </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                                  <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}>Dialysate Flow: </Text>
                                   </View>
                                    <View style={{marginTop:wp('1'),}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.dialysate_flow} </Text>
                                    </View>
                                    <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> ml/min </Text>
                                   </View>
                                   <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}>Dialysate Temp: </Text>
                                   </View>
                                    <View style={{marginTop:wp('1'),}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.dialysate_temp} </Text>
                                    </View>
                                    <View style={{marginTop:wp('1')}}>
                                      <Text>{this.co()}</Text>
                                   </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                                  <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}>Conductivity: </Text>
                                   </View>
                                    <View style={{marginTop:wp('1'),}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.conductivity} </Text>
                                    </View>
                                    <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> mS/cm </Text>
                                   </View>
                                   
                          </View>
                          <View style={{flexDirection:'row'}}>
                                  <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}>Anticoagulant </Text>
                                   </View>
                                   <CheckBox
                                            title='HAD Heparin initial dose'
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            checked={!!this.state.db_data.heparin_init_dose}
                                            size={hp('1.4')}
                                            textStyle={{
                                            fontSize:hp('2')
                                            }}       
                                            containerStyle={{
                                              width:wp('22')
                                            }}
                                    />
                                   <View style={{marginTop:hp('2')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.heparin_init_unit} </Text>
                                   </View>
                                   <View style={{marginTop:hp('2')}}>
                                      <Text style={styles.normalFont}> Unit </Text>
                                   </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                                      <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                          <Text style={styles.normalFont}>Maintenance dose </Text>
                                      </View>
                                      <View style={{marginTop:wp('1')}}>
                                          <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.maintenance_unit} </Text>
                                      </View>
                                      <View style={{marginTop:wp('1')}}>
                                          <Text style={styles.normalFont}> Unit/hrs. </Text>
                                      </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                                      <CheckBox
                                      title='LMW'
                                      checked={!!this.state.db_data.LMW}
                                      size={hp('1.4')}
                                      textStyle={{
                                      fontSize:hp('2')
                                      }}   
                                      containerStyle={{
                                        width:wp('10')
                                      }}      
                                      />
                                      <View style={{marginTop:hp('2')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.LMW_unit} </Text>
                                      </View>
                                      <View style={{marginTop:hp('2')}}>
                                      <Text style={styles.normalFont}> UNIT (Bolus Dose) </Text>
                                      </View>
                                      <CheckBox
                                            title='No Heparin'
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            checked={!!this.state.db_data.no_heparin}
                                            size={hp('1.4')}
                                            textStyle={{
                                            fontSize:hp('2')
                                            }}     
                                            containerStyle={{
                                              width:wp('13')
                                            }}  
                                    />
                                      
                          </View>
                          <View style={{flexDirection:'row'}}>
                            <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                          <Text style={styles.normalFont}>NSS Flush: </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nss_flush} </Text>
                                      </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> ml </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                          <Text>{this.q()}</Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.q} </Text>
                              </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> hrs. </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                    <Text style={styles.normalFont}> Time </Text>
                            </View>
                            <View style={{marginTop:wp('1'),width:wp('15')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.pre_time} </Text>
                              </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                            
                            <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                          <Text style={styles.normalFont}>BW: </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.BW} </Text>
                                      </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> kg </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                          <Text style={styles.normalFont}>DW: </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.DW} </Text>
                                      </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> kg </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                          <Text style={styles.normalFont}>Weight gain: </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.weight_gain} </Text>
                                      </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> kg </Text>
                            </View>
                             
                          </View>
                          <View style={{flexDirection:'row'}}>
                            
                            <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                          <Text style={styles.normalFont}>LAST BW: </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.last_BW} </Text>
                                      </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> kg </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                          <Text style={styles.normalFont}>UFG: </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.UFG} </Text>
                                      </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> litre </Text>
                            </View>
                                   
                          </View>
                          <View style={{flexDirection:'row'}}>
                            
                            <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                                          <Text style={styles.normalFont}>Set UF: </Text>
                            </View>
                            <View style={{marginTop:wp('1'),width:wp('16')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.set_UF} </Text>
                                      </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> ml </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                          <Text style={styles.normalFont}>Dialysis Length: </Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.dialysis_len} </Text>
                                      </View>
                            <View style={{marginTop:wp('1')}}>
                                      <Text style={styles.normalFont}> hrs. </Text>
                            </View>
                          </View>
                          <View style={{flexDirection:'row'}}>
                            
                            <View style={{marginLeft:wp('1'),marginTop:wp('1'),marginBottom:wp('1')}}>
                                          <Text style={styles.normalFont}>Start HD: Time ON </Text>
                            </View>
                            <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.hd_timeon} </Text>
                                      </View>
                            <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                                      <Text style={styles.normalFont}> Time OFF </Text>
                            </View>
                            <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                                      <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.hd_timeoff} </Text>
                            </View>

                          </View>
                      </View>
                    </Col>
                </Row>
                <Row style={styles.row4}>
                  <View style={{flexDirection:'comlumn'}}>
                  <View style={{flexDirection:'row'}}>
                    <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>แรกรับ V/S Pre HD BP:</Text>
                    </View>
                    <View style={{marginTop:wp('1')}}>
                          <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.BP} </Text>
                    </View>
                    <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>mmHg</Text>
                    </View>
                    <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>PR:</Text>
                    </View>
                    <View style={{marginTop:wp('1')}}>
                          <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.PR} </Text>
                    </View>
                    <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>beats/min</Text>
                    </View>
                    <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>RR:</Text>
                    </View>
                    <View style={{marginTop:wp('1')}}>
                          <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.RR} </Text>
                    </View>
                    <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>times/min</Text>
                    </View>
                    
                  </View>
                  <View style={{flexDirection:'row'}}>
                      <View style={{marginLeft:wp('1'),marginTop:wp('1'),marginBottom:wp('1')}}>
                              <Text style={styles.normalFont}>Sign and Symptom:</Text>
                        </View>
                        <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                              <Text style={{color:this.state.text_color ,fontSize:hp('2.2'),width:wp('80')}}> {this.state.db_data.sign_symp}</Text>
                        </View>
                  </View>
                  </View>
                </Row>
                <Row style={styles.row5}>
                  <Col style={styles.row5c1}>
                    <View style={{flexDirection: 'row',marginTop:hp('1'),marginBottom:hp('1'),alignSelf:'center'}}>
                              <Text style={styles.normalFont}>NURSING DIAGNOSIS</Text>
                      </View>      
                  </Col>
                  <Col style={styles.row5c2}>
                    <View style={{flexDirection: 'row',marginTop:hp('1'),alignSelf:'center',marginBottom:hp('1')}}>
                              <Text style={styles.normalFont}>INTERVENTION</Text>
                      </View> 
                  </Col>
                  <Col style={styles.row5c3}>
                    <View style={{flexDirection: 'row',marginTop:hp('1'),alignSelf:'center',marginBottom:hp('1')}}>
                              <Text style={styles.normalFont}>EVALUATION post HD</Text>
                      </View> 

                  </Col>
                </Row>
                <Row style={styles.row6}>
                  <Col style={styles.row6c1}>
                    <View style={{flexDirection:"column"}}>

                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_u_p}</Text>
                        </View>
                        <CheckBox
                            title='Uremia'
                            checked={!!this.state.db_data.nd_u}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}       
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_fve_p}</Text>
                        </View>
                        <CheckBox
                            title='Fluid volume excess'
                            checked={!!this.state.db_data.nd_fve}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_ei_p}</Text>
                        </View>
                        <CheckBox
                            title='Electrolytes imbalance'
                            checked={!!this.state.db_data.nd_ei}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_arr_p}</Text>
                        </View>
                        <CheckBox
                            title='Arrhythmia'
                            checked={!!this.state.db_data.nd_arr}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_ane_p}</Text>
                        </View>
                        <CheckBox
                            title='Anemia'
                            checked={!!this.state.db_data.nd_ane}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_cf_p}</Text>
                        </View>
                        <CheckBox
                            title='Chill/Fever'
                            checked={!!this.state.db_data.nd_cf}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_rh_p}</Text>
                        </View>
                        <CheckBox
                            title='Risk of hypoxia'
                            checked={!!this.state.db_data.nd_rh}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_ri_p}</Text>
                        </View>
                        <CheckBox
                            title='Risk for infection'
                            checked={!!this.state.db_data.nd_ri}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_dfv_p}</Text>
                        </View>
                        <CheckBox
                            title='Deficient fluid volume'
                            checked={!!this.state.db_data.nd_dfv}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_i_p}</Text>
                        </View>
                        <CheckBox
                            title='Inadequate'
                            checked={!!this.state.db_data.nd_i}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_m_p}</Text>
                        </View>
                        <CheckBox
                            title='Malnutrition'
                            checked={!!this.state.db_data.nd_m}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_scd_p}</Text>
                        </View>
                        <CheckBox
                            title='Self-care deficits'
                            checked={!!this.state.db_data.nd_scd}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_rf_p}</Text>
                        </View>
                        <CheckBox
                            title='Risk for fall'
                            checked={!!this.state.db_data.nd_rf}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{}}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_rcdd_p}</Text>
                        </View>
                        <CheckBox
                            title='Risk Complication during dialysis'
                            checked={!!this.state.db_data.nd_rcdd}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2')
                            }}       
                            containerStyle={{
                              width:wp('30')
                            }}
                        />
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('2'),marginTop:0,width:wp('30')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>: {this.state.db_data.nd_rcdd_desc}</Text>
                        </View>
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:hp('2'),marginBottom:wp('1')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>{this.state.db_data.nd_other_p}</Text>
                        </View>
                        <CheckBox
                            title='Other'
                            checked={!!this.state.db_data.nd_other}
                            size={wp('1.8')}
                            textStyle={{
                            fontSize:hp('2.2')
                            }}        
                            containerStyle={{
                              marginBottom:wp('1')
                            }}
                        />
                        <View style={{marginTop:hp('2'),width:wp('16'),marginBottom:wp('1')}}>
                              <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>: {this.state.db_data.nd_other_desc}</Text>
                        </View>

                      </View>

                    </View>
                  </Col>
                  <Col style={styles.row6c2}>
                    <View style={{flexDirection:"column"}}>

                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Monitor WS, EKG'
                                  checked={!!this.state.db_data.i_mwe}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Oxygen Therapy'
                                  checked={!!this.state.db_data.i_ot}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Pause UF'
                                  checked={!!this.state.db_data.i_pu}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Replace fluid'
                                  checked={!!this.state.db_data.i_rf}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Trendelenburg'
                                  checked={!!this.state.db_data.i_t}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Lower dialysis Temp.'
                                  checked={!!this.state.db_data.i_ldt}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Increased/ De.QB'
                                  checked={!!this.state.db_data.i_i}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Na modeling'
                                  checked={!!this.state.db_data.i_n}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Blood transfusion'
                                  checked={!!this.state.db_data.i_bt}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Observe Complication'
                                  checked={!!this.state.db_data.i_oc}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Monitor Access Flow'
                                  checked={!!this.state.db_data.i_maf}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Change BI.Line / DZ.'
                                  checked={!!this.state.db_data.i_cbd}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Strength Exercise'
                                  checked={!!this.state.db_data.i_se}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Psycho Support'
                                  checked={!!this.state.db_data.i_ps}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{
                                  }}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Notified Doctor'
                                  checked={!!this.state.db_data.i_nd}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{
                                  }}
                              />
                              
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Other'
                                  checked={!!this.state.db_data.i_other}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{
                                    marginBottom:wp('1')
                                  }}
                              />
                              <View style={{marginTop:hp('2'),marginBottom:wp('1'),width:wp('20')}}>
                                    <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>: {this.state.db_data.i_other_desc}</Text>
                              </View>
                          </View>
                    
                    </View>
                  </Col>
                  <Col style={styles.row6c3}>
                    <View style={{flexDirection:'column'}}>
                    <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='No complication'
                                  checked={!!this.state.db_data.eph_nocom}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='O2 sat > 95%'
                                  checked={!!this.state.db_data.eph_osat}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Fluid balance'
                                  checked={!!this.state.db_data.eph_fb}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Electrolyte balance'
                                  checked={!!this.state.db_data.eph_eb}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Adequate Dialysis'
                                  checked={!!this.state.db_data.eph_ad}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Have nutrition'
                                  checked={!!this.state.db_data.eph_hn}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='No pain'
                                  checked={!!this.state.db_data.eph_nop}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Safety/No accident'
                                  checked={!!this.state.db_data.eph_s}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Complication'
                                  checked={!!this.state.db_data.eph_com}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Hypotension'
                                  checked={!!this.state.db_data.eph_hypo}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Hypertension'
                                  checked={!!this.state.db_data.eph_hyper}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Arrhythmia'
                                  checked={!!this.state.db_data.eph_arr}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Chest pain'
                                  checked={!!this.state.db_data.eph_cp}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{}}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Muscle clamp'
                                  checked={!!this.state.db_data.eph_mc}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{
                                  }}
                              />
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Access problem'
                                  checked={!!this.state.db_data.eph_ap}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{
                                  }}
                              />
                              
                            </View>
                            <View style={{flexDirection:'row'}}>
                              <CheckBox
                                  title='Circuit clotted'
                                  checked={!!this.state.db_data.eph_cc}
                                  size={wp('1.8')}
                                  textStyle={{
                                  fontSize:hp('2.2')
                                  }}       
                                  containerStyle={{
                                    marginBottom:wp('1')
                                  }}
                              />
                              
                          </View>
                    </View>
                  </Col>
                </Row>
                <Row style={styles.row7}>
                  <Col size={70} style={styles.row7c1}>
                    <View style={{flexDirection:'column'}}>
                      <View style={{flexDirection:'row'}}> 
                      
                      <View style={{marginLeft:wp('1'),marginTop:wp('1'),marginBottom:wp('1')}}>
                        <Text style={{fontSize:hp('2.8'),color:'black'}}>BP (บน) (mmHg)</Text>
                      </View>
                      <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                        <Text style={{fontSize:hp('2.8'),color:'red'}}> / </Text>
                      </View>
                      <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                        <Text style={{fontSize:hp('2.8'),color:'green'}}>BP (ล่าง) (mmHg)  </Text>
                      </View>
                      <View style={{marginTop:wp('1.2'),marginBottom:wp('1')}}>
                        <Text style={{fontSize:hp('2.8'),color:'red'}}>PR (beats/min)  </Text>
                      </View>
                      <View style={{marginTop:wp('1.2'),marginBottom:wp('1')}}>
                         <Text style={{fontSize:hp('2.8'),color:'blue'}}>RR (times/min)  </Text>
                      </View>

                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{}}>
                              <LineChart
                                  data={this.state.data}
                                  width={wp('68')}
                                  height={hp('58')}
                                  chartConfig={chartConfig}
                                  withShadow={false}
                                  bezier
                                  verticalLabelRotation={60}
                                  />
                          </View>
                      </View>
                    </View>
                     
                  </Col>
                  <Col size={30} style={styles.row7c2}>

                    <View style={{flexDirection:'column'}}>
                      
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>NURSE NOTE</Text>
                        </View>
                      
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1'),width:wp('28')}}>
                            {this.renderNurseNote()}
                        </View>
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>Post HD : BP</Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.post_BP} </Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={styles.normalFont}> mmHg </Text>
                        </View>
                      
                      </View>
                      
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>Pulse</Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.pulse} </Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={styles.normalFont}> beats/min </Text>
                        </View>
                        
                      </View>
                      
                      <View style={{flexDirection:'row'}}>
                        
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>RR</Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.post_RR} </Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={styles.normalFont}> times/min </Text>
                        </View>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>BW</Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.post_BW} </Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={styles.normalFont}> kg </Text>
                        </View>
                      
                      </View>

                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>Weight loss</Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.weight_loss} </Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={styles.normalFont}> kg </Text>
                        </View>
                        
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('2')}}>
                          <Text style={styles.normalFont}>AV-Shunt</Text>
                        </View>
                        <View>
                        <CheckBox
                              title='Thrill'
                              checked={!!this.state.db_data.av_shunt_thrill}
                              size={hp('1.2')}
                              textStyle={{
                                  fontSize:hp('2.2'),
                                  
                              }}
                              containerStyle={{
                                marginTop:wp('1')

                              }}/>

                        </View>
                        <View>
                        <CheckBox
                              title='Bruit'
                              checked={!!this.state.db_data.av_shunt_bruit}
                              size={hp('1.2')}
                              textStyle={{
                                  fontSize:hp('2.2'),
                                  
                              }}
                              containerStyle={{
                                marginTop:wp('1')

                              }}/>

                        </View>
                        <View>

                        </View>
                        
                      </View>

                      <View style={{flexDirection:'row'}}>
                        
                        <View>
                        <CheckBox
                              title='Problem'
                              checked={!!this.state.db_data.problem}
                              size={hp('1.2')}
                              textStyle={{
                                  fontSize:hp('2.2'),
                                  
                              }}
                              containerStyle={{
                                marginTop:wp('1'),
                                marginBottom:wp('1')

                              }}/>

                        </View>
                        <View style={{marginTop:hp('2'),marginBottom:wp('1'),width:wp('14')}}>
                          <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>: {this.state.db_data.problem_desc}</Text>
                        </View>
                      </View>
                    </View>
                  </Col>
                </Row>
                <Row style={styles.row8}>
                  <Col size={70} style={styles.row8c1}>
                  <View style={styles.container}>
                <Table>
                    <Rowt data={this.state.tableHead} flexArr={[]} style={{height: hp('5'),  backgroundColor: '#f1f8ff' ,width:this.state.Twidth}} textStyle={styles.textHead} />
                    
                </Table>
                <Table borderStyle={{borderWidth: 1, borderColor: '#a9a9a9',}}>
                    <TableWrapper style={styles.wrapper}>
                        <Colt data={this.state.tableTitle} style={styles.title} heightArr={[hp('5')]} textStyle={styles.text}/>
                        <Rows data={this.state.tableData} flexArr={[3,3,3,3,3,3,3,3,3,3,3,3,3]} style={styles.row} textStyle={styles.text}/>
                    </TableWrapper>
                </Table>
                </View>
                  </Col>
                  <Col size={30} style={styles.row8c2}>
                    <View style={{flexDirection:'column'}}>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginTop:wp('1'),marginLeft:wp('1')}}>
                          <Text style={styles.normalFont}>DOCTOR NOTE</Text>
                        </View>
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginTop:wp('1'),marginLeft:wp('1'),marginBottom:wp('1'),width:wp('28')}}>
                            {/* <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.doctor_note} </Text> */}
                            {this.renderDoctorNote()}
                            {/* <HTML html={this.state.doctor_note} baseFontStyle={{ fontSize:hp('2.2'),color:this.state.text_color}} /> */}
                        </View>
                      </View>

                    </View>

                  </Col>

                </Row>

                <Row style={styles.row9}>
                  <Col style={styles.row9c1}>
                    <View style={{flexDirection:'column'}}>

                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>MEDICATION/ TIME / SIGN</Text>
                        </View>
                        
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>TPN</Text>
                        </View>
                        <View style={{marginTop:wp('1'),width:wp('28')}}>
                            <Text style={{color:this.state.medi_text_color,fontSize:hp('2.2')}}> {this.state.db_data.TPN} </Text>
                        </View>
                        
                      </View>

                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>Venofer 100 mg</Text>
                        </View>
                       
                        
                      </View>
                      <View style={{flexDirection:'row'}}>
                        
                        <View style={{marginLeft:wp('1'),marginTop:wp('1'),width:wp('32')}}>
                            <Text style={{color:this.state.medi_text_color,fontSize:hp('2.2')}}> {this.state.db_data.venofer} </Text>
                        </View>
                        
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1'),marginBottom:wp('1')}}>
                          <Text style={styles.normalFont}>ESA</Text>
                        </View>
                        <View style={{marginTop:wp('1'),width:wp('28'),marginBottom:wp('1')}}>
                            <Text style={{color:this.state.medi_text_color,fontSize:hp('2.2')}}> {this.state.db_data.ESA} </Text>
                        </View>
                        
                      </View>

                    </View>

                  </Col>
                  <Col style={styles.row9c2}> 

                  <View style={{flexDirection:'column'}}>
                    
                  <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>FLUID MANAGEMENT</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>NSS:</Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                            <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.NSS} </Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={styles.normalFont}> ml     </Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>Glucose:</Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                            <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.glucose} </Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={styles.normalFont}> ml </Text>
                        </View>
                        
                      </View>
                      <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>Extra fluid:</Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                            <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.extra_f} </Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={styles.normalFont}> ml </Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                          <Text style={styles.normalFont}>TOTAL INTAKE:</Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                            <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.total_intake} </Text>
                        </View>
                        <View style={{marginTop:wp('1')}}>
                          <Text style={styles.normalFont}> ml </Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1'),marginBottom:wp('1')}}>
                          <Text style={styles.normalFont}>TOTAL UF:</Text>
                        </View>
                        <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                            <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.total_UF} </Text>
                        </View>
                        <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                          <Text style={styles.normalFont}> ml      </Text>
                        </View>
                        <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                          <Text style={styles.normalFont}>NET UF:</Text>
                        </View>
                        <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                            <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.net_UF} </Text>
                        </View>
                        <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                          <Text style={styles.normalFont}> ml </Text>
                        </View>
                    </View>
                    

                    </View>

                  </Col>
                  <Col style={styles.row9c3}> 
                  
                  <View style={{flexDirection:'column'}}>
                      <View style={{flexDirection:'row'}}>
                            <View style={{marginLeft:wp('1'),marginTop:hp('2')}}>
                              <Text style={styles.normalFont}>REFER TO:</Text>
                            </View>
                            <View>
                            <CheckBox
                                  title='Home'
                                  checked={!!this.state.db_data.refer_to_home}
                                  size={hp('2')}
                                  textStyle={{
                                      fontSize:hp('2.2')
                                  }}
                                  checkedIcon='dot-circle-o'
                                      uncheckedIcon='circle-o'
                                  containerStyle={{
                                      
                                  }}
                              />

                            </View>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View style={{}}>
                            <CheckBox
                                  title='Ward'
                                  checked={!!this.state.db_data.refer_to_ward}
                                  size={hp('2')}
                                  textStyle={{
                                      fontSize:hp('2.2')
                                  }}
                                  checkedIcon='dot-circle-o'
                                      uncheckedIcon='circle-o'
                                  containerStyle={{
                                      
                                  }}
                              />
                            </View>
                            <View style={{marginTop:hp('2'),width:wp('20')}}>
                                  <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}>: {this.state.db_data.refer_to_ward_desc} </Text>
                            </View>
                            
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                              <Text style={styles.normalFont}>NEXT F/U</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                              <Text style={styles.normalFont}>Date:</Text>
                            </View>
                            <View style={{marginTop:wp('1'),width:260}}>
                                  <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.next_date} </Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View style={{marginLeft:wp('1'),marginTop:wp('1'),marginBottom:wp('1')}}>
                              <Text style={styles.normalFont}>Time:</Text>
                            </View>
                            <View style={{marginTop:wp('1'),width:wp('20'),marginBottom:wp('1')}}>
                                  <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.next_time} </Text>
                            </View>
                        </View>
                   </View>

                  </Col>

                </Row>
                <Row style={styles.row10}>
                  <View style={{flexDirection:'column'}}>
                      <View style={{flexDirection:'row'}}>
                      <View style={{marginLeft:wp('1'),marginTop:wp('1')}}>
                              <Text style={styles.normalFont}>ชื่อผู้ป่วย:</Text>
                            </View>
                            <View style={{marginTop:wp('1')}}>
                                  <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.patient} </Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <View style={{marginLeft:wp('1'),marginTop:wp('1'),marginBottom:wp('1')}}>
                              <Text style={styles.normalFont}>HN:</Text>
                            </View>
                            <View style={{marginTop:wp('1'),marginBottom:wp('1')}}>
                                  <Text style={{color:this.state.text_color,fontSize:hp('2.2')}}> {this.state.db_data.HN} </Text>
                            </View>
                        
                      </View>
                  </View>

                </Row>
             </ScrollView>
             
            </Row>
      </Grid>
      </SafeAreaView> 
      </MenuProvider>
    )
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    
  },
  row1: {
    backgroundColor: 'black',
  },
  row1c1: {
    backgroundColor: '#fff',
    marginRight:1,
    marginTop:1,
    marginLeft:1
  },
  row1c1_2: {
    backgroundColor: '#fff',
    
  },
  row1c2: {
    backgroundColor: '#fff',
    marginTop:1,
    marginRight:1,
  },
  row1c2_2: {
    backgroundColor: '#fff',
    
  },
  row2: {
    backgroundColor: 'black',
  },
  row2c1: {
    backgroundColor: '#fff',
    marginRight:1,
    marginTop:1,
    marginLeft:1
  },
  row2c2: {
    backgroundColor: '#fff',
    marginTop:1,
    marginRight:1,
  },
  row3: {
    backgroundColor: 'black',
  },
  row3c1: {
    backgroundColor: 'black',
    marginRight:1,
    
    marginLeft:1
  },
  row3c1r1: {
    backgroundColor: '#fff',
    
    marginTop:1
  },
  row3c1r2: {
    backgroundColor: '#fff',
    
    marginTop:1
  },
  row3c2: {
    backgroundColor: '#fff',
    marginTop:1,
    marginRight:1,
  },
  row4: {
    backgroundColor: '#fff',
    marginTop:1,
    marginRight:1,
    marginLeft:1,
    marginBottom:1
  },
  row5: {
    backgroundColor: 'black',
  },
  row5c1: {
    backgroundColor: '#fff',
    marginLeft:1,
    marginRight:1,
    marginBottom:1
  },
  row5c2: {
    backgroundColor: '#fff',
    
    marginRight:1,
    marginBottom:1
  },
  row5c3: {
    backgroundColor: '#fff',
    
    marginRight:1,
    marginBottom:1
  },
  row6: {
    backgroundColor: 'black',
  },
  row6c1: {
    backgroundColor: '#fff',
    marginLeft:1,
    marginRight:1,
    marginBottom:1
  },
  row6c2: {
    backgroundColor: '#fff',
    marginRight:1,
    marginBottom:1
  },
  row6c3: {
    backgroundColor: '#fff',
    marginRight:1,
    marginBottom:1
  },
  row7: {
    backgroundColor: 'black',
  },
  row7c1: {
    backgroundColor: '#fff',
    marginLeft:1,
    marginRight:1,
    marginBottom:1
  },
  row7c2: {
    backgroundColor: '#fff',
    marginRight:1,
    marginBottom:1
  },
  row8: {
    backgroundColor: 'black',
  },
  row8c1: {
    backgroundColor: '#fff',
    marginLeft:1,
    marginRight:1,
    marginBottom:1
  },
  row8c2: {
    backgroundColor: '#fff',
    marginRight:1,
    marginBottom:1
  },
  row9: {
    backgroundColor: 'black',
  },
  row9c1: {
    backgroundColor: '#fff',
    marginLeft:1,
    marginRight:1,
    marginBottom:1
  },
  row9c2: {
    backgroundColor: '#fff',
    marginRight:1,
    marginBottom:1
  },
  row9c3: {
    backgroundColor: '#fff',
    marginRight:1,
    marginBottom:1
  },
  row10: {
    backgroundColor: '#fff',
    
    marginRight:1,
    marginLeft:1,
    marginBottom:1
  },
  
  header: {
    backgroundColor: '#f1f1f1',
  },
  main_content: {
    backgroundColor: 'black'
  },
  headline: {
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: '1%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    // width: '100%'
    marginLeft:"1%"
  },
  left: {
    backgroundColor: '#f1f1f1'
  },
  right: {
    backgroundColor: '#f9f9f9',
    
  },
  
  //from table-component
  container: { flex: 1, padding: wp('2'), paddingTop: wp('3'), backgroundColor: '#fff' },
  wrapper: { flexDirection: 'row' },
  title: { flex: 4, backgroundColor: '#f6f8fa' },
  row: {  height: hp('5')  },
  text: { textAlign: 'center' },
  textHead: { textAlign: 'left',marginLeft:0},
  textData: { textAlign: 'right'}
  //end
  ,
  main:{
    flexDirection:'row'

  },
  right:{
    flexDirection:'column',
    justifyContent:'space-around'
  },
  buttonContainer: {
    height: hp('20%'), 
    width: wp('30%'),
    alignSelf:'center',
  },
  bgLogin:{
    height: hp('100%'), 
    width: wp('64%'),
    alignSelf:'flex-start',
    resizeMode:'contain'
  },
  logoLogin:{
    height: hp('36%'), 
    width: wp('36%'),
    alignSelf:'flex-end',
    resizeMode:'contain'
   },
  // inputShow:{
  //   //color:'dodgerblue' ,
  //   {color:this.state.text_color,fontSize:hp('2.2')}

 // },
  // input_text:{
  //   //color:'dodgerblue',
  //   {color:this.state.text_color,fontSize:hp('2.2')}
  // },
  // catheter_uq:{
  //   //color:'dodgerblue',
  //     {color:this.state.text_color ,fontSize:hp('2.2'),width:wp('5')}

  // // },
  // insertDate_uq:{
  //   //color:'dodgerblue',
  //   {color:this.state.text_color ,fontSize:hp('2.2'),width:wp('16')}
  // // },
  // synSymp_uq:{
  //   //color:'dodgerblue',
  //   {color:this.state.text_color ,fontSize:hp('2.2'),width:wp('80')}
  // // },
  // medi_uq:{
  //   //color:'dodgerblue',
  //   {color:this.state.medi_text_color,fontSize:hp('2.2')}
  // },
  normalFont:{
    fontSize:hp('2.2')

  },
  up_left:{
    justifyContent:'flex-start',
    alignItems:'flex-start',
    width:wp('10%'),
    
  },
  up_right_page:{
    justifyContent:'flex-end',
    alignItems:'flex-end',
    width:wp('10%'),
    padding:hp('2')
  },
  up_line:{
    flexDirection:'row',
    
  },
  up_right:{
    width:wp('80%'),
    justifyContent:'center',
    alignItems:'center',
  },
  

  
  
  
})