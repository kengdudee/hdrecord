import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput,TouchableWithoutFeedback,Keyboard, Dimensions } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
// import { TextInput } from 'react-native-gesture-handler';
import { Button, CheckBox } from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Permissions, ImagePicker } from 'expo';
import {
    Menu,
    MenuOptions, 
    MenuOption,
    MenuTrigger,
    MenuContext,
    MenuProvider,
    renderers
  } from 'react-native-popup-menu';
//import RichTextEditor from "react-native-zss-rich-text-editor"
//rich
import  CNRichTextEditor , { CNToolbar , getDefaultStyles, convertToObject ,getInitialObject ,convertToHtmlString} from "react-native-cn-richtext-editor";
const defaultStyles = getDefaultStyles();
const IS_IOS = Platform.OS === 'ios';
const { width, height } = Dimensions.get('window');
const { SlideInMenu } = renderers;
//endrich
console.disableYellowBox = true;
export default class Form10 extends React.Component {

goToForm9 = () => this.props.navigation.navigate('Form9')
goToForm11 = () => this.props.navigation.navigate('Form11')

constructor(props) {
    super(props);
    //rich
    this.customStyles = {...defaultStyles, body: {fontSize:hp('2.6')}, heading : {fontSize: 16}
        , title : {fontSize: 20}, ol : {fontSize: 12 }, ul: {fontSize: 12}, bold: {fontSize: 12, fontWeight: 'bold', color: ''}
        }; 
    //endrich
    this.state = {
      av_shunt_thrill:false,
      av_shunt_bruit:false,
      problem:false,
      nurse_note:'',
        post_BPup:'',
        post_BPdown:'',
        pulse:'',
        post_RR:'',
        post_BW:'',
        weight_loss:'',
        problem_desc:'',
        TPN:'',
        venofer:'',
        ESA:'',
      //rich
      selectedTag : 'body',
            selectedColor : 'default',
            selectedHighlight: 'default',
            colors : ['red'],
            highlights:['yellow_hl','pink_hl', 'orange_hl', 'green_hl','purple_hl','blue_hl'],
            selectedStyles : [],
            value: [getInitialObject()] //get empty editor
            // value: convertToObject('<div><p><span>สีดำ black </span><span style="color: #d23431;">red สีแดง </span><span>ดำ </span><span style="color: #d23431;">แดง </span><span>black </span><span style="color: #d23431;">red</span></p></div>'
            // , this.customStyles)
            // value: convertToObject('<div><p><span>:</span></p><p><span style="color: #d23431;"> \n *</span></p></div>'
            // , this.customStyles)
            //<p><span style="color: #d23431;"> \n </span></p>
        //endrich
    };
    //rich
    this.editor = null;
    //endrich
    
}
//rich
onStyleKeyPress = (toolType) => {
        
    if (toolType == 'image') {
        return;
    }
    else {
        this.editor.applyToolbar(toolType);
    }

}

onSelectedTagChanged = (tag) => {

    this.setState({
        selectedTag: tag
    })
}

onSelectedStyleChanged = (styles) => { 
    const colors = this.state.colors;  
    const highlights = this.state.highlights;  
    let sel = styles.filter(x=> colors.indexOf(x) >= 0);

    let hl = styles.filter(x=> highlights.indexOf(x) >= 0);
    this.setState({
        selectedStyles: styles,
        selectedColor : (sel.length > 0) ? sel[sel.length - 1] : 'default',
        selectedHighlight : (hl.length > 0) ? hl[hl.length - 1] : 'default',
    })
   
}

onValueChanged = (value) => {
    this.setState({
        value: value
    });
}

insertImage(url) {
    
    this.editor.insertImage(url);
}

askPermissionsAsync = async () => {
    const camera = await Permissions.askAsync(Permissions.CAMERA);
    const cameraRoll = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    this.setState({
    hasCameraPermission: camera.status === 'granted',
    hasCameraRollPermission: cameraRoll.status === 'granted'
    });
};

useLibraryHandler = async () => {
    await this.askPermissionsAsync();
    let result = await ImagePicker.launchImageLibraryAsync({
    allowsEditing: true,
    aspect: [4, 4],
    base64: false,
    });
    
    this.insertImage(result.uri);
};

useCameraHandler = async () => {
    await this.askPermissionsAsync();
    let result = await ImagePicker.launchCameraAsync({
    allowsEditing: true,
    aspect: [4, 4],
    base64: false,
    });
    console.log(result);
    
    this.insertImage(result.uri);
};

onImageSelectorClicked = (value) => {
    if(value == 1) {
        this.useCameraHandler();    
    }
    else if(value == 2) {
        this.useLibraryHandler();         
    }
    
}

onColorSelectorClicked = (value) => {
    
    if(value === 'default') {
        this.editor.applyToolbar(this.state.selectedColor);
    }
    else {
        this.editor.applyToolbar(value);
       
    }

    this.setState({
        selectedColor: value
    });
}

onHighlightSelectorClicked = (value) => {
    if(value === 'default') {
        this.editor.applyToolbar(this.state.selectedHighlight);
    }
    else {
        this.editor.applyToolbar(value);
       
    }

    this.setState({
        selectedHighlight: value
    });
}

onRemoveImage = ({url, id}) => {        
    // do what you have to do after removing an image
    console.log(`image removed (url : ${url})`);
    
}

renderImageSelector() {
    return (
        <Menu renderer={SlideInMenu} onSelect={this.onImageSelectorClicked}>
        <MenuTrigger>
            <MaterialCommunityIcons name="image" size={28} color="#737373" />
        </MenuTrigger>
        <MenuOptions>
            <MenuOption value={1}>
                <Text style={styles.menuOptionText}>
                    Take Photo
                </Text>
            </MenuOption>
            <View style={styles.divider}/>
            <MenuOption value={2} >
                <Text style={styles.menuOptionText}>
                    Photo Library
                </Text>
            </MenuOption> 
            <View style={styles.divider}/>
            <MenuOption value={3}>
                <Text style={styles.menuOptionText}>
                    Cancel
                </Text>
            </MenuOption>
        </MenuOptions>
        </Menu>
    );

}

renderColorMenuOptions = () => {

    let lst = [];
    //console.log("hi")

    if(defaultStyles[this.state.selectedColor]) {
         lst = this.state.colors.filter(x => x !== this.state.selectedColor);
         lst.push('default');
        lst.push(this.state.selectedColor);
    }
    else {
        lst = this.state.colors.filter(x=> true);
        lst.push('default');
    }

    return (
        
        lst.map( (item) => {
            let color = defaultStyles[item] ? defaultStyles[item].color : 'black';
            return (
                
                <MenuOption value={item} key={item} customStyles={{optionWrapper: { height:wp(34),width:wp(26)}}}>
                    {/* <MenuOption customStyles={{width:500,height:500}}/> */}
                    <MaterialCommunityIcons name="format-color-text" color={color}
                    size={wp(30)} />
                    
                </MenuOption>
                
            );
        })
        
    );
}

renderHighlightMenuOptions = () => {
    let lst = [];

    if(defaultStyles[this.state.selectedHighlight]) {
         lst = this.state.highlights.filter(x => x !== this.state.selectedHighlight);
         lst.push('default');
        lst.push(this.state.selectedHighlight);
    }
    else {
        lst = this.state.highlights.filter(x=> true);
        lst.push('default');
    }
    
    

    return (
        
        lst.map( (item) => {
            let bgColor = defaultStyles[item] ? defaultStyles[item].backgroundColor : 'black';
            return (
                <MenuOption value={item} key={item}>
                    <MaterialCommunityIcons name="marker" color={bgColor}
                    size={26} />
                </MenuOption>
            );
        })
        
    );
}

renderColorSelector() {
    //console.log("hiGUY")
    let selectedColor = 'black';
    if(defaultStyles[this.state.selectedColor])
    {
        selectedColor = defaultStyles[this.state.selectedColor].color;
    }
    

    return (
        <Menu renderer={SlideInMenu} onSelect={this.onColorSelectorClicked}>
        <MenuTrigger>
            {/* <MaterialCommunityIcons name="format-color-text" color={selectedColor}
                    size={28} style={{
                        top:2
                    }} />              */}
            <Text style={{color:selectedColor}}>Select color for nurse note</Text>         
        </MenuTrigger>
        <MenuOptions customStyles={optionsStyles}>
            {this.renderColorMenuOptions()}
        </MenuOptions>
        </Menu>
    );
}

renderHighlight() {
    let selectedColor = '#737373';
    if(defaultStyles[this.state.selectedHighlight])
    { 
        selectedColor = defaultStyles[this.state.selectedHighlight].backgroundColor;
    }
    return (
        <Menu renderer={SlideInMenu} onSelect={this.onHighlightSelectorClicked}>
        <MenuTrigger>
            <MaterialCommunityIcons name="marker" color={selectedColor}
                    size={24} style={{                          
                    }} />             
        </MenuTrigger>
        <MenuOptions customStyles={highlightOptionsStyles}>
            {this.renderHighlightMenuOptions()}
        </MenuOptions>
        </Menu>
    );
}
//endrich

async componentDidMount(){
    var local_post_BPup = window.post_BP.split("/")[0]
    var local_post_BPdown = window.post_BP.split("/")[1]

    this.setState({av_shunt_thrill:window.av_shunt_thrill})
    this.setState({av_shunt_bruit:window.av_shunt_bruit})
    this.setState({problem:window.problem})
    //var arr=[]
    if(window.nurse_note_object[0]!=undefined){
        this.setState({value:window.nurse_note_object})
        //console.log(window.nurse_note_object[0])
        //console.log("HI")
    }
    this.setState({post_BPup:local_post_BPup})
    if(local_post_BPdown!=undefined){
        this.setState({post_BPdown:local_post_BPdown})
    }
    this.setState({pulse:window.pulse})
    this.setState({post_RR:window.post_RR})
    this.setState({post_BW:window.post_BW})
    this.setState({weight_loss:window.weight_loss})
    this.setState({problem_desc:window.problem_desc})
    this.setState({TPN:window.TPN})
    this.setState({venofer:window.venofer})
    this.setState({ESA:window.ESA})
    
    
}

handleBack = ()=>{
    var local_post_BP = this.state.post_BPup+"/"+this.state.post_BPdown
    
    window.av_shunt_thrill=this.state.av_shunt_thrill
    window.av_shunt_bruit=this.state.av_shunt_bruit
    window.problem=this.state.problem
    //window.nurse_note=this.state.nurse_note
    window.nurse_note_object=this.state.value
    //window.nurse_note=convertToHtmlString(this.state.value)
    window.post_BP=local_post_BP
    window.pulse=this.state.pulse
    window.post_RR=this.state.post_RR
    window.post_BW=this.state.post_BW
    window.weight_loss=this.state.weight_loss
    window.problem_desc=this.state.problem_desc
    window.TPN=this.state.TPN
    window.venofer=this.state.venofer
    window.ESA=this.state.ESA
    //console.log(convertToHtmlString(this.state.value))
    this.goToForm9()
}

handleSubmit = values => {

    var local_post_BP = this.state.post_BPup+"/"+this.state.post_BPdown
    
    window.av_shunt_thrill=this.state.av_shunt_thrill
    window.av_shunt_bruit=this.state.av_shunt_bruit
    window.problem=this.state.problem
    //window.nurse_note=this.state.nurse_note
    window.nurse_note_object=this.state.value
    //window.nurse_note=convertToHtmlString(this.state.value)
    window.post_BP=local_post_BP
    window.pulse=this.state.pulse
    window.post_RR=this.state.post_RR
    window.post_BW=this.state.post_BW
    window.weight_loss=this.state.weight_loss
    window.problem_desc=this.state.problem_desc
    window.TPN=this.state.TPN
    window.venofer=this.state.venofer
    window.ESA=this.state.ESA
    //console.log(convertToHtmlString(this.state.value))
    this.goToForm11()
}


  render() {
    return (
        <Formik
            initialValues={{nurse_note:'',
                            post_BPup:'',
                            post_BPdown:'',
                            pulse:'',
                            post_RR:'',
                            post_BW:'',
                            weight_loss:'',
                            problem_desc:'',
                            TPN:'',
                            venofer:'',
                            ESA:'',

                            }}
            onSubmit={values => {
            this.handleSubmit(values)
            }}
        >
        {({handleSubmit,handleChange,values}) => (
            <MenuProvider>
            <SafeAreaView style={styles.container}>
                <KeyboardAwareScrollView>
                

            <View style={styles.up}>
            <View style={styles.up_line}>
            <View style={styles.up_left}>
                    <Ionicons.Button
                      name={'ios-arrow-back'}
                      size={hp('4')}
                      color='#000000'
                      backgroundColor='#fff'
                      onPress={this.handleBack}
                      title="back"
                    />
              </View>
            <View style={styles.up_right}>
                  <Text style={styles.head}></Text>
            </View>
            <View style={styles.up_right_page}>
                  <Text style={styles.head}>9/11</Text>
            </View>
          </View>
          </View>
          <Grid>
            <Col size={40} style={styles.left_content}>
            
            
                <View style={{flexDirection:'column'}}>
                    <View style={{flexDirection:'row'}}>
                        <View>
                            <Text style={{fontSize:hp('2.6'),margin:wp('2')}}>NURSE NOTE</Text>
                        </View>
                        <View>
                        <CNToolbar
                        style={{
                            height: hp(4.6),
                            marginTop:wp(1.6),
                            width:wp(26.6),
                            borderWidth:2
                        }}
                        iconSetContainerStyle={{
                            flexGrow: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                        size={wp('2.6')} 
                        iconSet={[
                            {
                                type: 'tool',
                                iconArray: [
                                
                                {
                                    toolTypeText: 'color',
                                    iconComponent: this.renderColorSelector()
                                 },
                                //  {
                                //     toolTypeText: 'highlight',
                                //     iconComponent: this.renderHighlight()
                                // }
                                ]
                            },
                        ]}
                        selectedTag={this.state.selectedTag}
                        selectedStyles={this.state.selectedStyles}
                        onStyleKeyPress={this.onStyleKeyPress} 
                        // backgroundColor="aliceblue" // optional (will override default backgroundColor)
                        // color="gray" // optional (will override default color)
                        // selectedColor='white' // optional (will override default selectedColor)
                        // selectedBackgroundColor='deepskyblue' // optional (will override default selectedBackgroundColor)
                        /> 
                        </View>
                    </View>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
                    <View style={{alignSelf:"center",height:hp(30)}}>
                        {/* <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,width:wp('36'),height:hp('36'),borderColor:'grey'}}
                            onChangeText={handleChange('nurse_note')}
                            value={this.state.nurse_note}
                            multiline={true}
                            fontSize={hp('2.6')}
                            
                        /> */}
                        <CNRichTextEditor                   
                            ref={input => this.editor = input}
                            onSelectedTagChanged={this.onSelectedTagChanged}
                            onSelectedStyleChanged={this.onSelectedStyleChanged}
                            value={this.state.value}
                            style={styles.editor}
                            styleList={this.customStyles}
                            //foreColor='black' // optional (will override default fore-color)
                            onValueChanged={this.onValueChanged}
                            onRemoveImage={this.onRemoveImage}
                        />  
                        {/* <RichTextEditor
                            ref={(r) => this.richtext = r}
                            initialTitleHTML={'Title!!'}
                            initialContentHTML={'Hello <b>World</b> <p>this is a new paragraph</p> <p>this is another new paragraph</p>'}
                            editorInitializedCallback={() => this.onEditorInitialized()}
                            />      */}
                    </View>
                    </TouchableWithoutFeedback>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{fontSize:hp('2.6'),margin:wp('2')}}>AV-Shunt</Text>
                    </View>
                    <View style={{flexDirection:'row',padding:hp('2'),justifyContent:'center',borderTopWidth:StyleSheet.hairlineWidth,borderBottomWidth:StyleSheet.hairlineWidth,marginBottom:wp('1')}}>
                            <CheckBox
                                title='Thrill'
                                checked={this.state.av_shunt_thrill}
                                onPress={() => this.setState({av_shunt_thrill: !this.state.av_shunt_thrill})}
                                
                                size={wp('2')}
                                textStyle={{
                                    fontSize:hp('2.6')
                                }}
                            />
                            <CheckBox
                                
                                title='Bruit'
                                checked={this.state.av_shunt_bruit}
                                onPress={() => this.setState({av_shunt_bruit: !this.state.av_shunt_bruit})}
                                size={wp('2')}
                                textStyle={{
                                    fontSize:hp('2.6')
                                }}
                            />
                    </View>
                    <View>
                            <CheckBox
                                
                                title='Problem'
                                checked={this.state.problem}
                                onPress={() => this.setState({problem: !this.state.problem})}
                                size={wp('2')}
                                textStyle={{
                                    fontSize:hp('2.6')
                                }}
                            />
                    </View>
                    <View>
                        <TextInput
                            style={{margin:wp('2'),borderBottomWidth:StyleSheet.hairlineWidth,borderColor:'grey'}}
                            onChangeText={(problem_desc) => this.setState({problem_desc})}
                            value={this.state.problem_desc}
                            placeholder='Please fill in Problem...'
                            fontSize={hp('2.6')}
                            ref={(input) => { this.t2 = input; }}
                            onSubmitEditing={() => { this.t3.focus(); }}
                            blurOnSubmit={false}
                        />
                    </View>

                </View>
            </Col>
            <Col size={23} style={styles.mid_content}>
                <View style={{flexDirection:'column'}}>
                    <View style={{alignSelf:'center',margin:wp('2')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>Post HD</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>BP (mmHg)</Text>

                    </View>
                    <View style={{flexDirection:'row'}}>
                            <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('7'),marginLeft:wp('4'),borderColor:'grey'}}
                                onChangeText={(post_BPup) => this.setState({post_BPup})}
                                value={this.state.post_BPup}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t3 = input; }}
                                onSubmitEditing={() => { this.t4.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                            <View style={{marginLeft:wp('1'),marginRight:wp('1')}}>
                                <Text style={{fontSize:wp('3')}}>
                                    /
                                </Text>
                            </View>

                            <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('7'),borderColor:'grey'}}
                                onChangeText={(post_BPdown) => this.setState({post_BPdown})}
                                value={this.state.post_BPdown}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t4 = input; }}
                                onSubmitEditing={() => { this.t5.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>Pulse (beats/min)</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('17'),height:hp('4.6'),borderColor:'grey'}}
                                onChangeText={(pulse) => this.setState({pulse})}
                                value={this.state.pulse}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t5 = input; }}
                                onSubmitEditing={() => { this.t6.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>RR (times/min)</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('17'),height:hp('4.6'),borderColor:'grey'}}
                                onChangeText={(post_RR) => this.setState({post_RR})}
                                value={this.state.post_RR}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t6 = input; }}
                                onSubmitEditing={() => { this.t7.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>BW (kg)</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('17'),height:hp('4.6'),borderColor:'grey'}}
                                onChangeText={(post_BW) => this.setState({post_BW})}
                                value={this.state.post_BW}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t7 = input; }}
                                onSubmitEditing={() => { this.t8.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>Weight loss (kg)</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('17'),height:hp('4.6'),borderColor:'grey'}}
                                onChangeText={(weight_loss) => this.setState({weight_loss})}
                                value={this.state.weight_loss}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t8 = input; }}
                                onSubmitEditing={() => { this.t9.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                </View>
            </Col>
            <Col size={27} style={styles.right_content}>
            <View style={{flexDirection:'column'}}>

                <View style={{alignSelf:'center',margin:wp('2')}}>
                <Text style={{
                        fontSize:hp('2.6')
                    }}>MEDICATION/ TIME / SIGN</Text>

                </View>
                <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>TPN</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('22'),height:hp('14.6'),borderColor:'grey'}}
                                onChangeText={(TPN) => this.setState({TPN})}
                                value={this.state.TPN}
                                multiline={true}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t9 = input; }}
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>Venofer 100 mg</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('22'),height:hp('10.6'),borderColor:'grey'}}
                                onChangeText={(venofer) => this.setState({venofer})}
                                value={this.state.venofer}
                                multiline={true}
                                fontSize={hp('2.6')}
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>ESA</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('22'),height:hp('10.6'),borderColor:'grey'}}
                                onChangeText={(ESA) => this.setState({ESA})}
                                value={this.state.ESA}
                                multiline={true}
                                fontSize={hp('2.6')}
                            />
                    </View>
                    
                    <View style={{flexDirection:'row',justifyContent:'flex-end',padding:wp('2')}}>
                                        <Button
                                                    onPress={handleSubmit}
                                                    title='Next'
                                                    containerStyle={{
                                                    width:wp('10')
                                                    }}
                                                />
                         </View>
            </View>
            </Col>
          </Grid>
          
          </KeyboardAwareScrollView>
          </SafeAreaView>
          </MenuProvider>

        )}
        </Formik>
    )
}}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      
    },
    editor: { 
        backgroundColor : '#fff',
        width:wp('40'),
        borderWidth:StyleSheet.hairlineWidth,
        borderColor:'grey'

    },
    up_left:{
      justifyContent:'flex-start',
      alignItems:'flex-start',
      width:wp('10%'),
      
    },
    up_right_page:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      width:wp('10%'),
      padding:hp('2')
    },
    up_line:{
      flexDirection:'row',
      
    },
    up_right:{
      width:wp('80%'),
      justifyContent:'center',
      alignItems:'center',
    },
    head:{
      fontSize:hp('2.6%'),
      fontWeight:'bold',
  
    },
    left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }

  })

  const optionsStyles = {
    optionsContainer: {
      backgroundColor: 'yellow',
      padding: 0,   
      width: 40,
      marginLeft: width - 40 - 30,
      alignItems: 'flex-end',
    },
    optionsWrapper: {
      //width: 40,
      backgroundColor: 'white',
    },
    optionWrapper: {
       //backgroundColor: 'yellow',
      margin: 2,
    },
    optionTouchable: {
      underlayColor: 'gold',
      activeOpacity: 70,
    },
    // optionText: {
    //   color: 'brown',
    // },
  };

const highlightOptionsStyles = {
optionsContainer: {
    backgroundColor: 'transparent',
    padding: 0,   
    width: 40,
    marginLeft: width - 40,

    alignItems: 'flex-end',
},
optionsWrapper: {
    //width: 40,
    backgroundColor: 'white',
},
optionWrapper: {
    //backgroundColor: 'yellow',
    margin: 2,
},
optionTouchable: {
    underlayColor: 'gold',
    activeOpacity: 70,
},
// optionText: {
//   color: 'brown',
// },
};