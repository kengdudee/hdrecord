import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
// import { TextInput } from 'react-native-gesture-handler';
import { Button, CheckBox } from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
console.disableYellowBox = true;

export default class Form7 extends React.Component {

goToForm6 = () => this.props.navigation.navigate('Form6')
goToForm9 = () => this.props.navigation.navigate('Form9')

constructor(props) {
    super(props);
    this.state = {
        i_mwe:false,
        i_ot:false,
        i_pu:false,
        i_rf:false,
        i_t:false,
        i_ldt:false,
        i_i:false,
        i_n:false,
        i_bt:false,
        i_oc:false,
        i_maf:false,
        i_cbd:false,
        i_se:false,
        i_ps:false,
        i_nd:false,
        i_other:false,
        i_other_desc:''
    };
}
async componentDidMount(){
    this.setState({i_mwe:window.i_mwe})
    this.setState({i_ot:window.i_ot})
    this.setState({i_pu:window.i_pu})
    this.setState({i_rf:window.i_rf})
    this.setState({i_t:window.i_t})
    this.setState({i_ldt:window.i_ldt})
    this.setState({i_i:window.i_i})
    this.setState({i_n:window.i_n})
    this.setState({i_bt:window.i_bt})
    this.setState({i_oc:window.i_oc})
    this.setState({i_maf:window.i_maf})
    this.setState({i_cbd:window.i_cbd})
    this.setState({i_se:window.i_se})
    this.setState({i_ps:window.i_ps})
    this.setState({i_nd:window.i_nd})
    this.setState({i_other:window.i_other})
    this.setState({i_other_desc:window.i_other_desc})
}
handleBack = () => {
    
    window.i_mwe=this.state.i_mwe
    window.i_ot=this.state.i_ot
    window.i_pu=this.state.i_pu
    window.i_rf=this.state.i_rf
    window.i_t=this.state.i_t
    window.i_ldt=this.state.i_ldt
    window.i_i=this.state.i_i
    window.i_n=this.state.i_n
    window.i_bt=this.state.i_bt
    window.i_oc=this.state.i_oc
    window.i_maf=this.state.i_maf
    window.i_cbd=this.state.i_cbd
    window.i_se=this.state.i_se
    window.i_ps=this.state.i_ps
    window.i_nd=this.state.i_nd
    window.i_other=this.state.i_other
    window.i_other_desc=this.state.i_other_desc
    
    this.goToForm6()
}


handleSubmit = values => {
    
    window.i_mwe=this.state.i_mwe
    window.i_ot=this.state.i_ot
    window.i_pu=this.state.i_pu
    window.i_rf=this.state.i_rf
    window.i_t=this.state.i_t
    window.i_ldt=this.state.i_ldt
    window.i_i=this.state.i_i
    window.i_n=this.state.i_n
    window.i_bt=this.state.i_bt
    window.i_oc=this.state.i_oc
    window.i_maf=this.state.i_maf
    window.i_cbd=this.state.i_cbd
    window.i_se=this.state.i_se
    window.i_ps=this.state.i_ps
    window.i_nd=this.state.i_nd
    window.i_other=this.state.i_other
    window.i_other_desc=this.state.i_other_desc
    
    this.goToForm9()
}

  render() {
    return (
        <Formik
        initialValues={{i_other_desc:''}}
        onSubmit={values => {
        this.handleSubmit(values)
        }}
    >
    {({ handleChange, values, handleSubmit}) => (
         <SafeAreaView style={styles.container}>
             <KeyboardAwareScrollView>
         <View style={styles.up}>
         <View style={styles.up_line}>
         <View style={styles.up_left}>
                 <Ionicons.Button
                   name={'ios-arrow-back'}
                   size={hp('4')}
                   color='#000000'
                   backgroundColor='#fff'
                   onPress={this.handleBack}
                   title="back"
                 />
           </View>
         <View style={styles.up_right}>
               <Text style={styles.head}>INTERVENTION</Text>
         </View>
         <View style={styles.up_right_page}>
               <Text style={styles.head}>7/11</Text>
         </View>
       </View>
       </View>
       <Grid>
         <Col size={50} style={styles.left_content}>
         <View style={{flexDirection:'column'}}>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Monitor WS, EKG'
                            checked={this.state.i_mwe}
                            onPress={() => this.setState({i_mwe: !this.state.i_mwe})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Oxygen Therapy'
                            checked={this.state.i_ot}
                            onPress={() => this.setState({i_ot: !this.state.i_ot})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Pause UF'
                            checked={this.state.i_pu}
                            onPress={() => this.setState({i_pu: !this.state.i_pu})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Replace fluid'
                            checked={this.state.i_rf}
                            onPress={() => this.setState({i_rf: !this.state.i_rf})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Trendelenburg'
                            checked={this.state.i_t}
                            onPress={() => this.setState({i_t: !this.state.i_t})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Lower dialysis Temp.'
                            checked={this.state.i_ldt}
                            onPress={() => this.setState({i_ldt: !this.state.i_ldt})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Increased/ De.QB'
                            checked={this.state.i_i}
                            onPress={() => this.setState({i_i: !this.state.i_i})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Na modeling'
                            checked={this.state.i_n}
                            onPress={() => this.setState({i_n: !this.state.i_n})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        
            </View>
        </Col>
        <Col size={50} style={styles.right_content}> 
                    <View style={{flexDirection:'column'}}>
                    <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Blood transfusion'
                            checked={this.state.i_bt}
                            onPress={() => this.setState({i_bt: !this.state.i_bt})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Observe Complication'
                            checked={this.state.i_oc}
                            onPress={() => this.setState({i_oc: !this.state.i_oc})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Monitor Access Flow'
                            checked={this.state.i_maf}
                            onPress={() => this.setState({i_maf: !this.state.i_maf})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Change BI.Line / DZ.'
                            checked={this.state.i_cbd}
                            onPress={() => this.setState({i_cbd: !this.state.i_cbd})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Strength Exercise'
                            checked={this.state.i_se}
                            onPress={() => this.setState({i_se: !this.state.i_se})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Psycho Support'
                            checked={this.state.i_ps}
                            onPress={() => this.setState({i_ps: !this.state.i_ps})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Notified Doctor'
                            checked={this.state.i_nd}
                            onPress={() => this.setState({i_nd: !this.state.i_nd})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Other'
                            checked={this.state.i_other}
                            onPress={() => this.setState({i_other: !this.state.i_other})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('12'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        <TextInput
                            style={{borderBottomWidth:StyleSheet.hairlineWidth,margin:hp('2'),width:wp('24')}}
                            onChangeText={(i_other_desc) => this.setState({i_other_desc})}
                            placeholder="Please fill in other..."
                            fontSize={hp('2.6')}
                            value={this.state.i_other_desc}
                        />
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'flex-end',padding:wp('2')}}>
                                        <Button
                                                    onPress={handleSubmit}
                                                    title='Next'
                                                    containerStyle={{
                                                    width:wp('10')
                                                    }}
                                                />
                         </View>

                    </View>
        </Col>
        </Grid>
        </KeyboardAwareScrollView>
        </SafeAreaView>


    )}
    </Formik>
    )

}}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      
    },
    up_left:{
      justifyContent:'flex-start',
      alignItems:'flex-start',
      width:wp('10%'),
      
    },
    up_right_page:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      width:wp('10%'),
      padding:hp('2')
    },
    up_line:{
      flexDirection:'row',
      
    },
    up_right:{
      width:wp('80%'),
      justifyContent:'center',
      alignItems:'center',
    },
    head:{
      fontSize:hp('2.6%'),
      fontWeight:'bold',
  
    },
    left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }
  })