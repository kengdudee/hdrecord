import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput, Alert } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
// import { TextInput } from 'react-native-gesture-handler';
import { Button, CheckBox } from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Html5Entities } from 'html-entities'; 
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
console.disableYellowBox = true;
export default class Form6 extends React.Component {


goToForm5 = () => this.props.navigation.navigate('Form5')
goToForm7 = () => this.props.navigation.navigate('Form7')


constructor(props) {
    super(props);
    this.state = {
        nd_u:false,
        nd_fve:false,
        nd_ei:false,
        nd_arr:false,
        nd_ane:false,
        nd_cf:false,
        nd_rh:false,
        nd_ri:false,
        nd_dfv:false,
        nd_i:false,
        nd_m:false,
        nd_scd:false,
        nd_rf:false,
        nd_rcdd:false,
        nd_other:false,

        nd_u_p:'',
        nd_fve_p:'',
        nd_ei_p:'',
        nd_arr_p:'',
        nd_ane_p:'',
        nd_cf_p:'',
        nd_rh_p:'',
        nd_ri_p:'',
        nd_dfv_p:'',
        nd_i_p:'',
        nd_m_p:'',
        nd_scd_p:'',
        nd_rf_p:'',
        nd_rcdd_p:'',
        nd_other_p:'',
        nd_rcdd_desc:'',
        nd_other_desc:'',
    };
}
async componentDidMount(){
    this.setState({nd_u:window.nd_u})
    this.setState({nd_fve:window.nd_fve})
    this.setState({nd_ei:window.nd_ei})
    this.setState({nd_arr:window.nd_arr})
    this.setState({nd_ane:window.nd_ane})
    this.setState({nd_cf:window.nd_cf})
    this.setState({nd_rh:window.nd_rh})
    this.setState({nd_ri:window.nd_ri})
    this.setState({nd_dfv:window.nd_dfv})
    this.setState({nd_i:window.nd_i})
    this.setState({nd_m:window.nd_m})
    this.setState({nd_scd:window.nd_scd})
    this.setState({nd_rf:window.nd_rf})
    this.setState({nd_rcdd:window.nd_rcdd})
    this.setState({nd_other:window.nd_other})

    this.setState({nd_u_p:window.nd_u_p})
    this.setState({nd_fve_p:window.nd_fve_p})
    this.setState({nd_ei_p:window.nd_ei_p})
    this.setState({nd_arr_p:window.nd_arr_p})
    this.setState({nd_ane_p:window.nd_ane_p})
    this.setState({nd_cf_p:window.nd_cf_p})
    this.setState({nd_rh_p:window.nd_rh_p})
    this.setState({nd_ri_p:window.nd_ri_p})
    this.setState({nd_dfv_p:window.nd_dfv_p})
    this.setState({nd_i_p:window.nd_i_p})
    this.setState({nd_m_p:window.nd_m_p})
    this.setState({nd_scd_p:window.nd_scd_p})
    this.setState({nd_rf_p:window.nd_rf_p})
    this.setState({nd_rcdd_p:window.nd_rcdd_p})
    this.setState({nd_other_p:window.nd_other_p})
    this.setState({nd_rcdd_desc:window.nd_rcdd_desc})
    this.setState({nd_other_desc:window.nd_other_desc})
    
}
handleBack=()=>{


    window.nd_u=this.state.nd_u
    window.nd_fve=this.state.nd_fve
    window.nd_ei=this.state.nd_ei
    window.nd_arr=this.state.nd_arr
    window.nd_ane=this.state.nd_ane
    window.nd_cf=this.state.nd_cf
    window.nd_rh=this.state.nd_rh
    window.nd_ri=this.state.nd_ri
    window.nd_dfv=this.state.nd_dfv
    window.nd_i=this.state.nd_i
    window.nd_m=this.state.nd_m
    window.nd_scd=this.state.nd_scd
    window.nd_rf=this.state.nd_rf
    window.nd_rcdd=this.state.nd_rcdd
    window.nd_other=this.state.nd_other
    window.nd_u_p=this.state.nd_u_p
    window.nd_fve_p=this.state.nd_fve_p
    window.nd_ei_p=this.state.nd_ei_p
    window.nd_arr_p=this.state.nd_arr_p
    window.nd_ane_p=this.state.nd_ane_p
    window.nd_cf_p=this.state.nd_cf_p
    window.nd_rh_p=this.state.nd_rh_p
    window.nd_ri_p=this.state.nd_ri_p
    window.nd_dfv_p=this.state.nd_dfv_p
    window.nd_i_p=this.state.nd_i_p
    window.nd_m_p=this.state.nd_m_p
    window.nd_scd_p=this.state.nd_scd_p
    window.nd_rf_p=this.state.nd_rf_p
    window.nd_rcdd_p=this.state.nd_rcdd_p
    window.nd_other_p=this.state.nd_other_p
    window.nd_rcdd_desc=this.state.nd_rcdd_desc
    window.nd_other_desc=this.state.nd_other_desc
    
    
    this.goToForm5()

}

handleSubmit = values => {

    window.nd_u=this.state.nd_u
    window.nd_fve=this.state.nd_fve
    window.nd_ei=this.state.nd_ei
    window.nd_arr=this.state.nd_arr
    window.nd_ane=this.state.nd_ane
    window.nd_cf=this.state.nd_cf
    window.nd_rh=this.state.nd_rh
    window.nd_ri=this.state.nd_ri
    window.nd_dfv=this.state.nd_dfv
    window.nd_i=this.state.nd_i
    window.nd_m=this.state.nd_m
    window.nd_scd=this.state.nd_scd
    window.nd_rf=this.state.nd_rf
    window.nd_rcdd=this.state.nd_rcdd
    window.nd_other=this.state.nd_other
    window.nd_u_p=this.state.nd_u_p
    window.nd_fve_p=this.state.nd_fve_p
    window.nd_ei_p=this.state.nd_ei_p
    window.nd_arr_p=this.state.nd_arr_p
    window.nd_ane_p=this.state.nd_ane_p
    window.nd_cf_p=this.state.nd_cf_p
    window.nd_rh_p=this.state.nd_rh_p
    window.nd_ri_p=this.state.nd_ri_p
    window.nd_dfv_p=this.state.nd_dfv_p
    window.nd_i_p=this.state.nd_i_p
    window.nd_m_p=this.state.nd_m_p
    window.nd_scd_p=this.state.nd_scd_p
    window.nd_rf_p=this.state.nd_rf_p
    window.nd_rcdd_p=this.state.nd_rcdd_p
    window.nd_other_p=this.state.nd_other_p
    window.nd_rcdd_desc=this.state.nd_rcdd_desc
    window.nd_other_desc=this.state.nd_other_desc
    
    
    this.goToForm7()
    
}

  render() {
    return (
        <Formik
                initialValues={{
                                nd_u_p:'',
                                nd_fve_p:'',
                                nd_ei_p:'',
                                nd_arr_p:'',
                                nd_ane_p:'',
                                nd_cf_p:'',
                                nd_rh_p:'',
                                nd_ri_p:'',
                                nd_dfv_p:'',
                                nd_i_p:'',
                                nd_m_p:'',
                                nd_scd_p:'',
                                nd_rf_p:'',
                                nd_rcdd_p:'',
                                nd_other_p:'',
                                nd_rcdd_desc:'',
                                nd_other_desc:'',
                }}
                onSubmit={values => {
                this.handleSubmit(values)
                }}
            >
            {({ handleChange, values, handleSubmit}) => (
                <SafeAreaView style={styles.container}>
                    <KeyboardAwareScrollView>
                <View style={styles.up}>
                <View style={styles.up_line}>
                <View style={styles.up_left}>
                        <Ionicons.Button
                          name={'ios-arrow-back'}
                          size={hp('4')}
                          color='#000000'
                          backgroundColor='#fff'
                          onPress={this.handleBack}
                          title="back"
                        />
                  </View>
                <View style={styles.up_right}>
                      <Text style={styles.head}>NURSING DIAGNOSIS</Text>
                </View>
                <View style={styles.up_right_page}>
                      <Text style={styles.head}>6/11</Text>
                </View>
              </View>
              </View>
              <Grid>
                <Col size={50} style={styles.left_content}>
                    <View style={{flexDirection:'column'}}>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_u_p) => this.setState({nd_u_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_u_p}
                        />
                        <CheckBox
                            title='Uremia'
                            checked={this.state.nd_u}
                            onPress={() => this.setState({nd_u: !this.state.nd_u})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_fve_p) => this.setState({nd_fve_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_fve_p}
                        />
                        <CheckBox
                            title='Fluid volume excess'
                            checked={this.state.nd_fve}
                            onPress={() => this.setState({nd_fve: !this.state.nd_fve})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_ei_p) => this.setState({nd_ei_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_ei_p}
                        />
                        <CheckBox
                            title='Electrolytes imbalance'
                            checked={this.state.nd_ei}
                            onPress={() => this.setState({nd_ei: !this.state.nd_ei})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_arr_p) => this.setState({nd_arr_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_arr_p}
                        />
                        <CheckBox
                            title='Arrhythmia'
                            checked={this.state.nd_arr}
                            onPress={() => this.setState({nd_arr: !this.state.nd_arr})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_ane_p) => this.setState({nd_ane_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_ane_p}
                        />
                        <CheckBox
                            title='Anemia'
                            checked={this.state.nd_ane}
                            onPress={() => this.setState({nd_ane: !this.state.nd_ane})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_cf_p) => this.setState({nd_cf_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_cf_p}
                        />
                        <CheckBox
                            title='Chill/Fever'
                            checked={this.state.nd_cf}
                            onPress={() => this.setState({nd_cf: !this.state.nd_cf})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_rh_p) => this.setState({nd_rh_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_rh_p}
                        />
                        <CheckBox
                            title='Risk of hypoxia'
                            checked={this.state.nd_rh}
                            onPress={() => this.setState({nd_rh: !this.state.nd_rh})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_ri_p) => this.setState({nd_ri_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_ri_p}
                        />
                        <CheckBox
                            title='Risk for infection'
                            checked={this.state.nd_ri}
                            onPress={() => this.setState({nd_ri: !this.state.nd_ri})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        
                    </View>
                  </Col>
                  <Col size={50} style={styles.right_content}> 
                  <View style={{flexDirection:'column'}}>
                    <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_dfv_p) => this.setState({nd_dfv_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_dfv_p}
                        />
                        <CheckBox
                            title='Deficient fluid volume'
                            checked={this.state.nd_dfv}
                            onPress={() => this.setState({nd_dfv: !this.state.nd_dfv})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_i_p) => this.setState({nd_i_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_i_p}
                        />
                        <CheckBox
                            title='Inadequate'
                            checked={this.state.nd_i}
                            onPress={() => this.setState({nd_i: !this.state.nd_i})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_m_p) => this.setState({nd_m_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_m_p}
                        />
                        <CheckBox
                            title='Malnutrition'
                            checked={this.state.nd_m}
                            onPress={() => this.setState({nd_m: !this.state.nd_m})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_scd_p) => this.setState({nd_scd_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_scd_p}
                        />
                        <CheckBox
                            title='Self-care deficits'
                            checked={this.state.nd_scd}
                            onPress={() => this.setState({nd_scd: !this.state.nd_scd})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_rf_p) => this.setState({nd_rf_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_rf_p}
                        />
                        <CheckBox
                            title='Risk for fall'
                            checked={this.state.nd_rf}
                            onPress={() => this.setState({nd_rf: !this.state.nd_rf})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_rcdd_p) => this.setState({nd_rcdd_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_rcdd_p}
                        />
                        <CheckBox
                            title='Risk Complication during dialysis'
                            checked={this.state.nd_rcdd}
                            onPress={() => this.setState({nd_rcdd: !this.state.nd_rcdd})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('40'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <TextInput
                                style={{borderBottomWidth:StyleSheet.hairlineWidth,margin:hp('2'),marginLeft:wp('10')}}
                                onChangeText={(nd_rcdd_desc) => this.setState({nd_rcdd_desc})}
                                placeholder="Please fill in Risk description here..."
                                fontSize={hp('2.6')}
                                value={this.state.nd_rcdd_desc}
                            />
                        </View>
                        <View style={{flexDirection:'row'}}>

                        <TextInput
                            style={{borderWidth:StyleSheet.hairlineWidth,height:hp('5'),width:wp('4'),margin:hp('2')}}
                            onChangeText={(nd_other_p) => this.setState({nd_other_p})}
                            fontSize={hp('2.6')}
                            value={this.state.nd_other_p}
                        />
                        <CheckBox
                            title='Other'
                            checked={this.state.nd_other}
                            onPress={() => this.setState({nd_other: !this.state.nd_other})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('12'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        
                        <TextInput
                            style={{borderBottomWidth:StyleSheet.hairlineWidth,margin:hp('2'),width:wp('24')}}
                            onChangeText={(nd_other_desc) => this.setState({nd_other_desc})}
                            placeholder="Please fill in other..."
                            fontSize={hp('2.6')}
                            value={this.state.nd_other_desc}
                        />
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'flex-end',padding:wp('2')}}>
                                        <Button
                                                    onPress={handleSubmit}
                                                    title='Next'
                                                    containerStyle={{
                                                    width:wp('10')
                                                    }}
                                                />
                         </View>
                        
                  </View>
                  </Col>
              </Grid>
              </KeyboardAwareScrollView>
              </SafeAreaView>

            )}
            </Formik>
            
    )

}}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      
    },
    up_left:{
      justifyContent:'flex-start',
      alignItems:'flex-start',
      width:wp('10%'),
      
    },
    up_right_page:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      width:wp('10%'),
      padding:hp('2')
    },
    up_line:{
      flexDirection:'row',
      
    },
    up_right:{
      width:wp('80%'),
      justifyContent:'center',
      alignItems:'center',
    },
    head:{
      fontSize:hp('2.6%'),
      fontWeight:'bold',
  
    },
    left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }

  })