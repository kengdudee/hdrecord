import React, { Component,Fragment } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput, PickerIOS,ScrollView } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
// import { TextInput } from 'react-native-gesture-handler';
import { Button,CheckBox } from 'react-native-elements';
import SearchableDropdown from 'react-native-searchable-dropdown';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import KeyboardSpacer from 'react-native-keyboard-spacer';
console.disableYellowBox = true;
var items = [
  {
    id: 1,
    name: 'นาย',
  },
  {
    id: 2,
    name: 'นาง',
  },
  {
    id: 3,
    name: 'นางสาว',
  },
  
];
const monthNames = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
  "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"
];
const dayNames = ["วันอาทิตย์","วันจันทร์","วันอังคาร","วันพุธ","วันพฤหัสบดี","วันศุกร์","วันเสาร์"]
export default class Form1 extends React.Component {

goToHome = () => this.props.navigation.navigate('App')
goToForm2 = () => this.props.navigation.navigate('Form2')

constructor(props) {
    super(props);
    this.state = {
      acute: false,
      chronic: false,
      CGD:false,
      SSS:false,
      NHSO:false,
      state_enterprise:false,
      self_pay:false,
      prefix_patient:'',
      date:'',
      patient:'',
      
    };
}
componentDidMount(){

  var time = new Date();
  var date
  var day = dayNames[time.getDay()]+'ที่'
  var year = time.getFullYear()+543
  date=day +' '+ time.getDate() + ' ' + monthNames[time.getMonth()] + ' ' + year          
  
  this.setState({date:date})

}

handleSubmit = values => {

    var middle=''
    if(!values.other_patient.localeCompare('')){
        middle=' '
    }
    else{
        middle=' '+values.other_patient+' '
    }
    var patient=this.state.prefix_patient+values.first_patient+middle+values.last_patient
    this.setState({patient:patient})
    window.acute= this.state.acute
    window.chronic= this.state.chronic
    window.CGD=this.state.CGD
    window.SSS=this.state.SSS
    window.NHSO=this.state.NHSO
    window.state_enterprise=this.state.state_enterprise
    window.self_pay=this.state.self_pay
    window.prefix_patient=this.state.prefix_patient
    window.patient=this.state.patient
    window.first_patient=values.first_patient
    window.last_patient=values.last_patient
    window.other_patient=values.other_patient
    window.id=values.id
    window.HN=values.HN
    window.HD_no=values.HD_no
    window.date=this.state.date
    window.ward=values.ward
    window.diagnosis=values.diagnosis
    window.notice=values.notice
    window.nurse_assign=values.nurse_assign
    
    this.goToForm2()
}

acuteOrChronic = key =>{
  var acute="a"
  if(!acute.localeCompare(key)){
    this.setState({acute: !this.state.acute})
    this.setState({chronic: false})
  }
  else {
    this.setState({acute: false})
    this.setState({chronic: !this.state.chronic})
  }
}
selectDisbur = key =>{
  var c ="c"
  var sss ="sss"
  var n ="n"
  var st ="st"
  var se ="se"
  if(!c.localeCompare(key)){
    this.setState({CGD:!this.state.CGD})
    this.setState({SSS:false})
    this.setState({NHSO:false})
    this.setState({state_enterprise:false})
    this.setState({self_pay:false})
  }
  if(!sss.localeCompare(key)){
    this.setState({CGD:false})
    this.setState({SSS:!this.state.SSS})
    this.setState({NHSO:false})
    this.setState({state_enterprise:false})
    this.setState({self_pay:false})
  }
  if(!n.localeCompare(key)){
    this.setState({CGD:false})
    this.setState({SSS:false})
    this.setState({NHSO:!this.state.NHSO})
    this.setState({state_enterprise:false})
    this.setState({self_pay:false})
  }
  if(!st.localeCompare(key)){
    this.setState({CGD:false})
    this.setState({SSS:false})
    this.setState({NHSO:false})
    this.setState({state_enterprise:!this.state.state_enterprise})
    this.setState({self_pay:false})
  }
  if(!se.localeCompare(key)){
    this.setState({CGD:false})
    this.setState({SSS:false})
    this.setState({NHSO:false})
    this.setState({state_enterprise:false})
    this.setState({self_pay:!this.state.self_pay})
  }
}

  render() {
    return (
      <Formik
          initialValues={{last_patient:'',other_patient:'',first_patient:'',id:'',HN:'',HD_no:'',date:'',ward:'',diagnosis:'',notice:'',nurse_assign:''}}
          onSubmit={values => {
            this.handleSubmit(values)
          }}
          >
          {({ handleChange, values, handleSubmit}) => (
              <SafeAreaView style={styles.container}>
              <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
              <View style={styles.up}>
              <View style={styles.up_line}>
              <View style={styles.up_left}>
                      <Ionicons.Button
                        name={'ios-arrow-back'}
                        size={hp('4')}
                        color='#000000'
                        backgroundColor='#fff'
                        onPress={this.goToHome}
                        title="back"
                      />
                </View>
              <View style={styles.up_right}>
                    <Text style={styles.head}></Text>
              </View>
              <View style={styles.up_right_page}>
                    <Text style={styles.head}>1/11</Text>
              </View>
            </View>
            </View>
            <Grid>
              <Col size={50} style={styles.left_content}>
                <View style={{flexDirection:'column'}}>
                  <View style={{flexDirection:'row',padding:hp('2')}}>
                    <Text style={{fontSize:hp('2.6')}}>คำนำหน้า-ชื่อ-นามสกุล-ชื่ออื่นๆ ของผู้ป่วย</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                  <View>
                          <SearchableDropdown
                                      onItemSelect={(item) => {
                                        this.setState({ prefix_patient: item.name });
                                      }}
                                      containerStyle={{  }}
                                      itemStyle={{
                                        padding: hp('1'),
                                        backgroundColor: '#ddd',
                                        borderColor: '#bbb',
                                        borderWidth: 1,
                                        borderRadius: 5,
                                        marginTop:hp('1'),
                                        marginLeft:hp('2')
                                        
                                      }}
                                      itemTextStyle={{ color: '#222',fontSize:hp('2.6') }}
                                      itemsContainerStyle={{  }}
                                      items={items}
                                      // defaultIndex={2}
                                      resetValue={false}
                                      textInputProps={
                                        {
                                          placeholder: "คำนำหน้า",
                                          underlineColorAndroid: "transparent",
                                          style: {
                                              padding: hp('1'),
                                              borderWidth: 1,
                                              borderColor: '#ccc',
                                              borderRadius: 5,
                                              fontSize:hp('2.6'),
                                              marginLeft:hp('2')
                                          },
                                          onTextChange: text => this.setState({prefix_patient:text})
                                        }
                                      }
                                      listProps={
                                        {
                                          nestedScrollEnabled: true,
                                          scrollEnabled:true,
                                          //horizontal:true,
                                          showsHorizontalScrollIndicator:true
                                        }
                                      }
                                  />
                          </View>
                        <View>
                        <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('11'),height:hp('5.4'),marginLeft:wp('1'),borderColor:'grey'}}
                                onChangeText={handleChange('first_patient')}
                                value={values.first_patient}
                                placeholder=" ชื่อ"
                                fontSize={hp('2.6')}
                                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                        </View>
                        <View>
                        <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('11'),height:hp('5.4'),marginLeft:wp('1'),borderColor:'grey'}}
                                onChangeText={handleChange('last_patient')}
                                value={values.last_patient}
                                placeholder=" นามสกุล"
                                fontSize={hp('2.6')}
                                ref={(input) => { this.secondTextInput = input; }}
                                onSubmitEditing={() => { this.thirdTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                        </View>
                        <View>
                        <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('11'),height:hp('5.4'),marginLeft:wp('1'),borderColor:'grey'}}
                                onChangeText={handleChange('other_patient')}
                                placeholder=" ชื่ออื่นๆ"
                                value={values.other_patient}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.thirdTextInput = input; }}
                                onSubmitEditing={() => { this.fourthTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                        </View>
                      </View>
                  <View style={{flexDirection:'row',padding:hp('2')}}>
                    <Text style={{fontSize:hp('2.6')}}>เลขบัตรประชาชนของผู้ป่วย</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      <View>
                        <TextInput
                                style={styles.longTextInput}
                                onChangeText={handleChange('id')}
                                value={values.id}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.fourthTextInput = input; }}
                                onSubmitEditing={() => { this.fifthTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                      </View>
                  </View>
                  <View style={{flexDirection:'row',padding:hp('2')}}>
                    <Text style={{fontSize:hp('2.6')}}>HN</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      <View>
                        <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('46'),height:hp('4.4'),marginLeft:hp('2'),marginBottom:hp('2'),borderColor:'grey'}}
                                onChangeText={handleChange('HN')}
                                value={values.HN}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.fifthTextInput = input; }}
                                onSubmitEditing={() => { this.sixthTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                      </View>
                  </View>
                  <View style={{flexDirection:'row',padding:hp('2'),justifyContent:'center',borderTopWidth:StyleSheet.hairlineWidth,borderBottomWidth:StyleSheet.hairlineWidth}}>
                  <CheckBox
                        title='Acute'
                        checked={this.state.acute}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        onPress={() => this.acuteOrChronic("a")}
                        size={wp('2')}
                        textStyle={{
                            fontSize:hp('2.6')
                        }}
                    />
                    <CheckBox
                        title='Chronic'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.chronic}
                        onPress={() => this.acuteOrChronic("c")}
                        size={wp('2')}
                        textStyle={{
                            fontSize:hp('2.6')
                        }}
                    />
                  </View>
                  <View style={{flexDirection:'row',padding:hp('2')}}>
                    <Text style={{fontSize:hp('2.6')}}>HD No.</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      <View>
                        <TextInput
                                style={styles.longTextInput}
                                onChangeText={handleChange('HD_no')}
                                value={values.HD_no}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.sixthTextInput = input; }}
                                onSubmitEditing={() => { this.seventhTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                      </View>
                  </View>
                  <View style={{flexDirection:'row',padding:hp('2')}}>
                    <Text style={{fontSize:hp('2.6')}}>Date</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      <View>
                        <TextInput
                                style={styles.longTextInput}
                                onChangeText={(value)=>{this.setState({date:value})}}
                                value={this.state.date}
                                fontSize={hp('2.6')}
                            />
                      </View>
                  </View>
                  <View style={{flexDirection:'row',padding:hp('2')}}>
                    <Text style={{fontSize:hp('2.6')}}>Ward</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      <View>
                        <TextInput
                                style={styles.longTextInput}
                                onChangeText={handleChange('ward')}
                                value={values.ward}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.seventhTextInput = input; }}
                                onSubmitEditing={() => { this.eighthTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                      </View>
                  </View>
                </View>
              </Col>
              <Col size={50} style={styles.right_content}> 
                <View style={{flexDirection:'column'}}>
                   <View style={{flexDirection:'row',padding:hp('2')}}>
                    <Text style={{fontSize:hp('2.6')}}>Diagnosis</Text>
                    </View>
                  <View style={{flexDirection:'row'}}>
                      <View>
                        <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('46'),height:hp('4.4'),marginLeft:hp('2'),marginBottom:hp('2'),borderColor:'grey'}}
                                onChangeText={handleChange('diagnosis')}
                                value={values.diagnosis}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.eighthTextInput = input; }}
                                onSubmitEditing={() => { this.ninthTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                      </View>
                  </View>
                  <View style={{flexDirection:'column',padding:hp('1'),justifyContent:'center',borderTopWidth:StyleSheet.hairlineWidth,borderBottomWidth:StyleSheet.hairlineWidth}}>
                  <View style={{flexDirection:'row',padding:hp('2')}}>
                    <Text style={{fontSize:hp('2.6')}}>Disbursement</Text>
                    </View>
                  <CheckBox
                        title='CGD'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.CGD}
                        onPress={() => this.selectDisbur("c")}
                        size={wp('2')}
                        textStyle={{
                            fontSize:hp('2.6')
                        }}
                        
                    />
                    <CheckBox
                        title='SSS'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.SSS}
                        onPress={() => this.selectDisbur("sss")}
                        size={wp('2')}
                        textStyle={{
                            fontSize:hp('2.6')
                        }}
                        
                    />
                    <CheckBox
                        title='NHSO'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.NHSO}
                        onPress={() => this.selectDisbur("n")}
                        size={wp('2')}
                        textStyle={{
                            fontSize:hp('2.6')
                        }}
                        
                    />
                    <CheckBox
                        title='State Enterprises'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.state_enterprise}
                        onPress={() => this.selectDisbur("st")}
                        size={wp('2')}
                        textStyle={{
                            fontSize:hp('2.6')
                        }}
                        
                    />
                    <CheckBox
                        title='Self-pay'
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.self_pay}
                        onPress={() => this.selectDisbur("se")}
                        size={wp('2')}
                        textStyle={{
                            fontSize:hp('2.6')
                        }}
                        containerStyle={{
                          marginBottom:wp('1')
                        }}
                    />
                  </View>
                  <View style={{flexDirection:'row',padding:hp('2')}}>
                    <Text style={{fontSize:hp('2.6')}}>Notice</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      <View>
                        <TextInput
                                style={styles.longTextInput}
                                onChangeText={handleChange('notice')}
                                value={values.notice}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.ninthTextInput = input; }}
                                onSubmitEditing={() => { this.tenthTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                      </View>
                  </View>
                  <View style={{flexDirection:'row',padding:hp('2')}}>
                    <Text style={{fontSize:hp('2.6')}}>Nurse assignment</Text>
                  </View>
                  <View style={{flexDirection:'row'}}>
                      <View>
                        <TextInput
                                style={styles.longTextInput}
                                onChangeText={handleChange('nurse_assign')}
                                value={values.nurse_assign}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.tenthTextInput = input; }}
                            />
                      </View>
                  </View>
                  <View style={{flexDirection:'row',justifyContent:'flex-end',padding:wp('1')}}>
                  <Button
                            onPress={handleSubmit}
                            title='Next'
                            containerStyle={{
                              width:wp('10')
                            }}
                        />
                  </View>
                </View>
              </Col>
            </Grid>
            </KeyboardAwareScrollView>
            </SafeAreaView>
          )}
      </Formik>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    
  },
  up_left:{
    justifyContent:'flex-start',
    alignItems:'flex-start',
    width:wp('10%'),
    
  },
  up_right_page:{
    justifyContent:'flex-end',
    alignItems:'flex-end',
    width:wp('10%'),
    padding:hp('2')
  },
  up_line:{
    flexDirection:'row',
    
  },
  up_right:{
    width:wp('80%'),
    justifyContent:'center',
    alignItems:'center',
  },
  head:{
    fontSize:hp('2.6%'),
    fontWeight:'bold',

  },
  longTextInput:{
    borderWidth:StyleSheet.hairlineWidth,
    width:wp('46'),
    height:hp('4.4'),
    marginLeft:hp('2'),
    borderColor:'grey'

  },
  left_content:{
    borderTopWidth: StyleSheet.hairlineWidth,
    borderLeftWidth: StyleSheet.hairlineWidth,
    borderRightWidth: StyleSheet.hairlineWidth
  },
  mid_content:{
    borderTopWidth: StyleSheet.hairlineWidth,
    borderRightWidth: StyleSheet.hairlineWidth
  },
  right_content:{
    borderTopWidth: StyleSheet.hairlineWidth,
    borderRightWidth: StyleSheet.hairlineWidth
  }

})