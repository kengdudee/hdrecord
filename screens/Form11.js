import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Alert, useWindowDimensions, SafeAreaView, Dimensions, TouchableWithoutFeedback, Keyboard} from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
import AwesomeAlert from 'react-native-awesome-alerts';
// import { TextInput } from 'react-native-gesture-handler';
import { Button, CheckBox } from 'react-native-elements';

import CalendarPicker from 'react-native-calendar-picker';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Permissions, ImagePicker } from 'expo';
import axios from 'axios';
import {
    Menu,
    MenuOptions, 
    MenuOption,
    MenuTrigger,
    MenuContext,
    MenuProvider,
    renderers
  } from 'react-native-popup-menu';
//import RichTextEditor from "react-native-zss-rich-text-editor"
//rich
import  CNRichTextEditor , { CNToolbar , getDefaultStyles, convertToObject ,getInitialObject ,convertToHtmlString} from "react-native-cn-richtext-editor";
const defaultStyles = getDefaultStyles();
const IS_IOS = Platform.OS === 'ios';
const { width, height } = Dimensions.get('window');
const { SlideInMenu } = renderers;
//endrich

console.disableYellowBox = true;
const monthNames = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
  "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"
];
const dayNames = ["วันอาทิตย์","วันจันทร์","วันอังคาร","วันพุธ","วันพฤหัสบดี","วันศุกร์","วันเสาร์"]

//window.gnextDate=''
export default class Form11 extends React.Component {

goToForm10 = () => this.props.navigation.navigate('Form10')
goToForm8 = () => this.props.navigation.navigate('Form8')
goToHome = () => this.props.navigation.navigate('App')

constructor(props) {
    super(props);
    //rich
    this.customStyles = {...defaultStyles, body: {fontSize:hp('2.6')}, heading : {fontSize: 16}
        , title : {fontSize: 20}, ol : {fontSize: 12 }, ul: {fontSize: 12}, bold: {fontSize: 12, fontWeight: 'bold', color: ''}
        }; 
    //endrich
    this.state = {
        
        showAlert:false,
    doctor_border:'grey',
      refer_to_home:false,
      refer_to_ward:false,
      selectedStartDate: null,
      NSS:'',
    glucose:'',
    extra_f:'',
    total_intake:'',
    total_UF:'',
    net_UF:'',
    doctor_note:'',
    refer_to_ward_desc:'',
    
    next_time_h:'',
    next_time_m:'',
      //rich
      selectedTag : 'body',
            selectedColor : 'default',
            selectedHighlight: 'default',
            colors : ['red'],
            highlights:['yellow_hl','pink_hl', 'orange_hl', 'green_hl','purple_hl','blue_hl'],
            selectedStyles : [],
            value: [getInitialObject()] //get empty editor
            // value: convertToObject('<div><p><span>สีดำ black </span><span style="color: #d23431;">red สีแดง </span><span>ดำ </span><span style="color: #d23431;">แดง </span><span>black </span><span style="color: #d23431;">red</span></p></div>'
            // , this.customStyles)
            // value: convertToObject('<div><p><span>:</span></p><p><span style="color: #d23431;"> \n *</span></p></div>'
            // , this.customStyles)
            //<p><span style="color: #d23431;"> \n </span></p>
        //endrich
    };

    
    //rich
    this.editor = null;
    //endrich
    this.onDateChange = this.onDateChange.bind(this);
}
//rich
onStyleKeyPress = (toolType) => {
        
    if (toolType == 'image') {
        return;
    }
    else {
        this.editor.applyToolbar(toolType);
    }

}

showAlert = () => {
    this.setState({
      showAlert: true
    });
  };
  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };

onSelectedTagChanged = (tag) => {

    this.setState({
        selectedTag: tag
    })
}

onSelectedStyleChanged = (styles) => { 
    const colors = this.state.colors;  
    const highlights = this.state.highlights;  
    let sel = styles.filter(x=> colors.indexOf(x) >= 0);

    let hl = styles.filter(x=> highlights.indexOf(x) >= 0);
    this.setState({
        selectedStyles: styles,
        selectedColor : (sel.length > 0) ? sel[sel.length - 1] : 'default',
        selectedHighlight : (hl.length > 0) ? hl[hl.length - 1] : 'default',
    })
   
}

onValueChanged = (value) => {
    this.setState({
        value: value
    });
}

insertImage(url) {
    
    this.editor.insertImage(url);
}

askPermissionsAsync = async () => {
    const camera = await Permissions.askAsync(Permissions.CAMERA);
    const cameraRoll = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    this.setState({
    hasCameraPermission: camera.status === 'granted',
    hasCameraRollPermission: cameraRoll.status === 'granted'
    });
};

useLibraryHandler = async () => {
    await this.askPermissionsAsync();
    let result = await ImagePicker.launchImageLibraryAsync({
    allowsEditing: true,
    aspect: [4, 4],
    base64: false,
    });
    
    this.insertImage(result.uri);
};

useCameraHandler = async () => {
    await this.askPermissionsAsync();
    let result = await ImagePicker.launchCameraAsync({
    allowsEditing: true,
    aspect: [4, 4],
    base64: false,
    });
    console.log(result);
    
    this.insertImage(result.uri);
};

onImageSelectorClicked = (value) => {
    if(value == 1) {
        this.useCameraHandler();    
    }
    else if(value == 2) {
        this.useLibraryHandler();         
    }
    
}

onColorSelectorClicked = (value) => {
    
    if(value === 'default') {
        this.editor.applyToolbar(this.state.selectedColor);
    }
    else {
        this.editor.applyToolbar(value);
       
    }

    this.setState({
        selectedColor: value
    });
}

onHighlightSelectorClicked = (value) => {
    if(value === 'default') {
        this.editor.applyToolbar(this.state.selectedHighlight);
    }
    else {
        this.editor.applyToolbar(value);
       
    }

    this.setState({
        selectedHighlight: value
    });
}

onRemoveImage = ({url, id}) => {        
    // do what you have to do after removing an image
    console.log(`image removed (url : ${url})`);
    
}

renderImageSelector() {
    return (
        <Menu renderer={SlideInMenu} onSelect={this.onImageSelectorClicked}>
        <MenuTrigger>
            <MaterialCommunityIcons name="image" size={28} color="#737373" />
        </MenuTrigger>
        <MenuOptions>
            <MenuOption value={1}>
                <Text style={styles.menuOptionText}>
                    Take Photo
                </Text>
            </MenuOption>
            <View style={styles.divider}/>
            <MenuOption value={2} >
                <Text style={styles.menuOptionText}>
                    Photo Library
                </Text>
            </MenuOption> 
            <View style={styles.divider}/>
            <MenuOption value={3}>
                <Text style={styles.menuOptionText}>
                    Cancel
                </Text>
            </MenuOption>
        </MenuOptions>
        </Menu>
    );

}

renderColorMenuOptions = () => {

    let lst = [];

    if(defaultStyles[this.state.selectedColor]) {
         lst = this.state.colors.filter(x => x !== this.state.selectedColor);
         lst.push('default');
        lst.push(this.state.selectedColor);
    }
    else {
        lst = this.state.colors.filter(x=> true);
        lst.push('default');
    }

    return (
        
        lst.map( (item) => {
            let color = defaultStyles[item] ? defaultStyles[item].color : 'black';
            return (
                // <MenuOption value={item} key={item}>
                //     <MaterialCommunityIcons name="format-color-text" color={color}
                //     size={28} />
                // </MenuOption>
                <MenuOption value={item} key={item} customStyles={{optionWrapper: { height:wp(34),width:wp(26)} }}>
                    {/* <MenuOption customStyles={{width:500,height:500}}/> */}
                    <MaterialCommunityIcons name="format-color-text" color={color}
                    size={wp(30)} />
                    
                </MenuOption>
            );
        })
        
    );
}

renderHighlightMenuOptions = () => {
    let lst = [];

    if(defaultStyles[this.state.selectedHighlight]) {
         lst = this.state.highlights.filter(x => x !== this.state.selectedHighlight);
         lst.push('default');
        lst.push(this.state.selectedHighlight);
    }
    else {
        lst = this.state.highlights.filter(x=> true);
        lst.push('default');
    }
    
    

    return (
        
        lst.map( (item) => {
            let bgColor = defaultStyles[item] ? defaultStyles[item].backgroundColor : 'black';
            return (
                <MenuOption value={item} key={item}>
                    <MaterialCommunityIcons name="marker" color={bgColor}
                    size={26} />
                </MenuOption>
            );
        })
        
    );
}

renderColorSelector() {
    //console.log("hii")
   
    let selectedColor = 'black';
    if(defaultStyles[this.state.selectedColor])
    {
        selectedColor = defaultStyles[this.state.selectedColor].color;
    }
    //this.setState({doctor_border:selectedColor})
    return (
        <Menu renderer={SlideInMenu} onSelect={this.onColorSelectorClicked}>
        <MenuTrigger>
            {/* <MaterialCommunityIcons name="format-color-text" color={selectedColor}
                    size={28} style={{
                        top:2
                    }} />              */}
            <Text style={{color:selectedColor}}>Select color for doctor note</Text>
        </MenuTrigger>
        <MenuOptions customStyles={optionsStyles}>
            {this.renderColorMenuOptions()}
        </MenuOptions>
        </Menu>
    );
}

renderHighlight() {
    let selectedColor = '#737373';
    if(defaultStyles[this.state.selectedHighlight])
    { 
        selectedColor = defaultStyles[this.state.selectedHighlight].backgroundColor;
    }
    return (
        <Menu renderer={SlideInMenu} onSelect={this.onHighlightSelectorClicked}>
        <MenuTrigger>
            <MaterialCommunityIcons name="marker" color={selectedColor}
                    size={24} style={{                          
                    }} />             
        </MenuTrigger>
        <MenuOptions customStyles={highlightOptionsStyles}>
            {this.renderHighlightMenuOptions()}
        </MenuOptions>
        </Menu>
    );
}
//endrich

onDateChange(date) {
    this.setState({
      selectedStartDate: date,
    });
  }
modifyDate= date =>{
    var dateSplited=date.startDate.split(" ")
    //0 วัน
    //1 เดือน
    //2 วันที่
    //3 ปี
    var day=dateSplited[0]
    if(!day.localeCompare('Mon')){
        day=dayNames[1]
    }
    if(!day.localeCompare('Tue')){
        day=dayNames[2]
    }
    if(!day.localeCompare('Wed')){
        day=dayNames[3]
    }
    if(!day.localeCompare('Thu')){
        day=dayNames[4]
    }
    if(!day.localeCompare('Fri')){
        day=dayNames[5]
    }
    if(!day.localeCompare('Sat')){
        day=dayNames[6]
    }
    if(!day.localeCompare('Sun')){
        day=dayNames[0]
    }
    var month=dateSplited[1]
    var monthOld=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    for(var i=0;i<12;i=i+1){
        if(month){
            var currMonth=monthOld[i]
            if(!month.localeCompare(currMonth)){
                month=monthNames[i]
            }
        }
    }

    var date=dateSplited[2]
    var yearInt=Number.parseInt(dateSplited[3])+543
    var year = yearInt.toString()
    if(day||date||month){
        var trueDate = day+'ที่'+' '+date+" "+month+" "+year
        window.gnextDate=trueDate
    }
    
}
async componentDidMount() {
    var local_next_timeh = window.next_time.split(":")[0]
    var local_next_timem = window.next_time.split(":")[1].split(" ")[0]

    this.setState({next_time_h:local_next_timeh})
    this.setState({next_time_m:local_next_timem})

    this.setState({refer_to_home:window.refer_to_home})
    this.setState({refer_to_ward:window.refer_to_ward})
    this.setState({NSS:window.NSS})
    this.setState({glucose:window.glucose})
    this.setState({extra_f:window.extra_f})
    this.setState({total_intake:window.total_intake})
    this.setState({total_UF:window.total_UF})
    this.setState({net_UF:window.net_UF})
    this.setState({refer_to_ward_desc:window.refer_to_ward_desc})
    if(window.doctor_note_object[0]!=undefined){
        this.setState({value:window.doctor_note_object})
    }
    
    //this.setState({next_date:window.gnextDate})
    //console.log(window.gnextDate)

}
handleBack=()=>{
    var local_next_time=this.state.next_time_h+":"+this.state.next_time_m+" "+"น."

    window.refer_to_home=this.state.refer_to_home
    window.refer_to_ward=this.state.refer_to_ward
    window.NSS=this.state.NSS
    window.glucose=this.state.glucose
    window.extra_f=this.state.extra_f
    window.total_intake=this.state.total_intake
    window.total_UF=this.state.total_UF
    window.net_UF=this.state.net_UF
    window.doctor_note_object=this.state.value
    //window.doctor_note=convertToHtmlString(this.state.value)
    window.refer_to_ward_desc=this.state.refer_to_ward_desc
    window.next_date=window.gnextDate
    window.next_time=local_next_time

    this.goToForm10()
}


handleSubmit = values => {


    var local_next_time=this.state.next_time_h+":"+this.state.next_time_m+" "+"น."

    window.refer_to_home=this.state.refer_to_home
    window.refer_to_ward=this.state.refer_to_ward
    window.NSS=this.state.NSS
    window.glucose=this.state.glucose
    window.extra_f=this.state.extra_f
    window.total_intake=this.state.total_intake
    window.total_UF=this.state.total_UF
    window.net_UF=this.state.net_UF
    window.doctor_note_object=this.state.value
    //window.doctor_note=convertToHtmlString(this.state.value)
    window.refer_to_ward_desc=this.state.refer_to_ward_desc
    window.next_date=window.gnextDate
    window.next_time=local_next_time

    
    this.goToForm8()
    //this.showAlert()
    
    //this.submitRecord()
    
}
hOrW = key =>{
    var h="h"
    if(!h.localeCompare(key)){
        this.setState({refer_to_home:!this.state.refer_to_home})
        this.setState({refer_to_ward:false})
    }
    else{
        this.setState({refer_to_home:false})
        this.setState({refer_to_ward:!this.state.refer_to_ward})
    }

}
handleShowDate=()=>{
if(!window.gnextDate.localeCompare('')){
    return<View><Text style={{fontSize:hp('2.6')}} >กดเลือกวันที่บนปฏิทินแล้ววันที่จะแสดงที่นี่</Text></View>
}
else{
    return<View ><Text style={{fontSize:hp('2.6')}}>{window.gnextDate}</Text></View>
}
}

  render() {
    const { selectedStartDate,showAlert } = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : '';
    
    return (
        <Formik
                initialValues={{
                                NSS:'',
                                glucose:'',
                                extra_f:'',
                                total_intake:'',
                                total_UF:'',
                                net_UF:'',
                                doctor_note:'',
                                refer_to_ward_desc:'',
                                
                                next_time_h:'',
                                next_time_m:'',

                }}
                onSubmit={values => {
                this.handleSubmit(values)
                }}
            >
            {({handleSubmit,handleChange,values}) => (
                <MenuProvider>
                <SafeAreaView style={styles.container}>
                    <KeyboardAwareScrollView>
                    
                <View style={styles.up}>
                <View style={styles.up_line}>
                <View style={styles.up_left}>
                        <Ionicons.Button
                          name={'ios-arrow-back'}
                          size={hp('4')}
                          color='#000000'
                          backgroundColor='#fff'
                          onPress={this.handleBack}
                          title="back"
                        />
                  </View>
                <View style={styles.up_right}>
                      <Text style={styles.head}></Text>
                </View>
                <View style={styles.up_right_page}>
                      <Text style={styles.head}>10/11</Text>
                </View>
              </View>
              </View>
              <Grid>
              <Col size={30} style={styles.left_content}>
                  <View style={{flexDirection:'column'}}>
                    <View style={{alignSelf:'center',margin:wp('2')}}>
                        <Text style={{
                                fontSize:hp('2.6')
                            }}>FLUID MANAGEMENT</Text>

                        </View>
                        <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>NSS (ml)</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('22'),height:hp('4.6'),borderColor:'grey'}}
                                onChangeText={(NSS) => this.setState({NSS})}
                                value={this.state.NSS}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t1 = input; }}
                                onSubmitEditing={() => { this.t2.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>Glucose (ml)</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('22'),height:hp('4.6'),borderColor:'grey'}}
                                onChangeText={(glucose) => this.setState({glucose})}
                                value={this.state.glucose}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t2 = input; }}
                                onSubmitEditing={() => { this.t3.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>Extra fluid (ml)</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('22'),height:hp('4.6'),borderColor:'grey'}}
                                onChangeText={(extra_f) => this.setState({extra_f})}
                                value={this.state.extra_f}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t3 = input; }}
                                onSubmitEditing={() => { this.t4.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>TOTAL INTAKE (ml)</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('22'),height:hp('4.6'),borderColor:'grey'}}
                                onChangeText={(total_intake) => this.setState({total_intake})}
                                value={this.state.total_intake}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t4 = input; }}
                                onSubmitEditing={() => { this.t5.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>TOTAL UF (ml)</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('22'),height:hp('4.6'),borderColor:'grey'}}
                                onChangeText={(total_UF) => this.setState({total_UF})}
                                value={this.state.total_UF}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t5 = input; }}
                                onSubmitEditing={() => { this.t6.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                    <View style={{alignSelf:'flex-start',margin:wp('1'),marginLeft:wp('4')}}>
                    <Text style={{
                            fontSize:hp('2.6')
                        }}>NET UF (ml)</Text>

                    </View>
                    <View style={{alignSelf:'flex-start',marginLeft:wp('4')}}>
                    <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('22'),height:hp('4.6'),borderColor:'grey'}}
                                onChangeText={(net_UF) => this.setState({net_UF})}
                                value={this.state.net_UF}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.t6 = input; }}
                                //onSubmitEditing={() => { this.t7.focus(); }}
                                blurOnSubmit={false}
                                
                            />
                    </View>
                  </View>
              </Col>
              <Col size={40} style={styles.right_content}>
                
                <View style={{flexDirection:'column'}} >
                    <View style={{flexDirection:'row'}}>
                        <View>
                        <Text style={{fontSize:hp('2.6'),margin:wp('2')}}>DOCTOR NOTE</Text>
                        </View>
                        <View>
                        <CNToolbar
                        style={{
                            height: hp(4.6),
                            marginTop:wp(1.6),
                            width:wp(20.6),
                            borderWidth:2
                        }}
                        iconSetContainerStyle={{
                            flexGrow: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                        size={wp('2.6')} 
                        iconSet={[
                            {
                                type: 'tool',
                                iconArray: [
                                
                                {
                                    toolTypeText: 'color',
                                    iconComponent: this.renderColorSelector()
                                 },
                                ]
                            },
                        ]}
                        selectedTag={this.state.selectedTag}
                        selectedStyles={this.state.selectedStyles}
                        onStyleKeyPress={this.onStyleKeyPress} 
                        color='black'
                        
                        /> 
                        </View>
                    </View>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
                    <View style={{alignSelf:"center",height:hp('24')}}>
                        
                        <CNRichTextEditor                   
                            ref={input => this.editor = input}
                            onSelectedTagChanged={this.onSelectedTagChanged}
                            onSelectedStyleChanged={this.onSelectedStyleChanged}
                            value={this.state.value}
                            style={{backgroundColor : '#fff',width:wp('36'),borderWidth:StyleSheet.hairlineWidth,borderColor:this.state.doctor_border}}
                            styleList={this.customStyles}
                            //foreColor='black' // optional (will override default fore-color)
                            onValueChanged={this.onValueChanged}
                            onRemoveImage={this.onRemoveImage}
                        />  
                     </View>
                     </TouchableWithoutFeedback>
                     
                   
                    <View style={{flexDirection:'row'}}>
                        <Text style={{fontSize:hp('2.6'),margin:wp('2')}}>REFER TO :</Text>
                    </View>
                  <CheckBox
                        title='Home'
                        checked={this.state.refer_to_home}
                        onPress={()=>{this.hOrW('h')}}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        size={wp('2')}
                        textStyle={{
                            fontSize:hp('2.6')
                        }}
                    />
                    <View View style={{flexDirection:'row'}}>

                    <CheckBox
                        title='Ward'
                        checked={this.state.refer_to_ward}
                        onPress={()=>{this.hOrW('w')}}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        size={wp('2')}
                        textStyle={{
                            fontSize:hp('2.6')
                        }}
                        containerStyle={{
                            width:wp('14')
                        }}
                    />
                    <View>
                        <TextInput
                            style={{borderBottomWidth:StyleSheet.hairlineWidth,margin:wp('1'),width:wp('22'),height:hp('5'),borderColor:'grey'}}
                            onChangeText={(refer_to_ward_desc) => this.setState({refer_to_ward_desc})}
                            value={this.state.refer_to_ward_desc}
                            placeholder="Please fill in..."
                            fontSize={24}
                        />
                    </View>
                    </View>
                  
                </View>
              </Col>
              <Col size={30} style={{borderTopWidth: StyleSheet.hairlineWidth,borderRightWidth: StyleSheet.hairlineWidth}}>
              <View style={{flexDirection:'column'}} >
                    <View style={{flexDirection:'row'}}>
                        <Text style={{fontSize:hp('2.6'),margin:wp('2')}}>NEXT F/U</Text>
                    </View>
                    <CalendarPicker
                            onDateChange={this.onDateChange}
                            width={wp('40')}
                            height={hp('40')}
                            weekdays={["อา","จ","อ","พ","พฤ","ศ","ส"]}
                            months={["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
                            "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"
                          ]}
                          textStyle={{fontSize:16}}
                          allowRangeSelection={false}
                          minDate={{startDate}}
                          monthYearHeaderWrapperStyle={{}}

                            />
                
                          {this.modifyDate({startDate})}
               
                        <View style={{margin:wp('2')}}>
                            <Text style={{fontSize:hp('2.6')}}>Date</Text>
                        </View>

                        <View style={{margin:wp('1'),justifyContent:'center',alignItems:'center'}}>
                                    {this.handleShowDate()}
                        </View>

                        <View style={{margin:wp('2')}}>
                            <Text style={{fontSize:hp('2.6')}}>Time</Text>
                        </View>

                        <View style={{flexDirection:'row'}}>
                            <View>
                            <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('10'),height:wp('3.4'),marginLeft:wp('2'),borderColor:'grey'}}
                                    onChangeText={(next_time_h) => this.setState({next_time_h})}
                                    value={this.state.next_time_h}
                                    fontSize={hp('2.6')}
                                    placeholder="เช่น 7"
                                    ref={(input) => { this.t8 = input; }}
                                    onSubmitEditing={() => { this.t9.focus(); }}
                                    blurOnSubmit={false}
                                    
                                />
                            </View>
                            <View style={{marginLeft:wp('1'),marginRight:wp('1')}}>
                                <Text style={{fontSize:wp('2')}}> : </Text>
                                </View>
                            <View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('10'),height:wp('3.4'),borderColor:'grey'}}
                                    onChangeText={(next_time_m) => this.setState({next_time_m})}
                                    value={this.state.next_time_m}
                                    fontSize={hp('2.6')}
                                    placeholder="20"
                                    ref={(input) => { this.t9 = input; }}
                                    
                                />
                            </View>
                            
                   </View>
                   <View style={{flexDirection:'row',justifyContent:'flex-end',padding:wp('2')}}>
                                        <Button
                                                    onPress={handleSubmit}
                                                    title='Next'
                                                    containerStyle={{
                                                    width:wp('10')
                                                    }}
                                                />
                         </View>

                </View>
              </Col>
              </Grid>
              
              </KeyboardAwareScrollView>
              </SafeAreaView>
              <AwesomeAlert
          show={showAlert}
          showProgress={false}
          title="Are you sure to save this form?"
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="Save"
          confirmButtonColor="blue"
          cancelButtonColor="#606060"
          onCancelPressed={() => {
            //window.deleteKey=''
            this.hideAlert();
          }}
          onConfirmPressed={() => {
            //this.toDeleteRecord(window.deleteKey)
            this.submitRecord()
            this.hideAlert()
          }}
        />
              </MenuProvider>

            )}
            </Formik>

    )
}}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      
    },
    // editor: { 
    //     {backgroundColor : '#fff',width:wp('36'),borderWidth:StyleSheet.hairlineWidth,borderColor:'red'}
    // },
    up_left:{
      justifyContent:'flex-start',
      alignItems:'flex-start',
      width:wp('10%'),
      
    },
    up_right_page:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      width:wp('10%'),
      padding:hp('2')
    },
    up_line:{
      flexDirection:'row',
      
    },
    up_right:{
      width:wp('80%'),
      justifyContent:'center',
      alignItems:'center',
    },
    head:{
      fontSize:hp('2.6%'),
      fontWeight:'bold',
  
    },
    left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }
  })


  const optionsStyles = {
    optionsContainer: {
      backgroundColor: 'yellow',
      padding: 0,   
      width: 40,
      marginLeft: width - 40 - 30,
      alignItems: 'flex-end',
    },
    optionsWrapper: {
      //width: 40,
      backgroundColor: 'white',
    },
    optionWrapper: {
       //backgroundColor: 'yellow',
      margin: 2,
    },
    optionTouchable: {
      underlayColor: 'gold',
      activeOpacity: 70,
    },
    // optionText: {
    //   color: 'brown',
    // },
  };

const highlightOptionsStyles = {
optionsContainer: {
    backgroundColor: 'transparent',
    padding: 0,   
    width: 40,
    marginLeft: width - 40,

    alignItems: 'flex-end',
},
optionsWrapper: {
    //width: 40,
    backgroundColor: 'white',
},
optionWrapper: {
    //backgroundColor: 'yellow',
    margin: 2,
},
optionTouchable: {
    underlayColor: 'gold',
    activeOpacity: 70,
},
// optionText: {
//   color: 'brown',
// },
};