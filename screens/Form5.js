import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
// import { TextInput } from 'react-native-gesture-handler';
import { Button, CheckBox } from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Html5Entities } from 'html-entities'; 
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
console.disableYellowBox = true;
export default class Form5 extends React.Component {

goToForm4 = () => this.props.navigation.navigate('Form4')
goToForm6 = () => this.props.navigation.navigate('Form6')


constructor(props) {
    super(props);
    this.state = {
       heparin_init_dose:false,
       LMW:false,
       no_heparin:false,

       dialysate_flow:'',
        dialysate_temp:'',
        conductivity:'',
        heparin_init_unit:'',
        maintenance_unit:'',
        LMW_unit:'',
        nss_flush:'',
        q:'',
        pre_time:'',
        BW:'',
        DW:'',
        weight_gain:'',
        last_BW:'',
        UFG:'',
        set_UF:'',
        dialysis_len:'',
        hd_timeonh:'',
        hd_timeonm:'',
        hd_timeoffh:'',
        hd_timeoffm:'',

    };
}
async componentDidMount(){
    this.setState({heparin_init_dose:heparin_init_dose})
    this.setState({LMW:LMW})
    this.setState({no_heparin:no_heparin})
    this.setState({dialysate_flow:dialysate_flow})
    this.setState({dialysate_temp:dialysate_temp})
    this.setState({conductivity:conductivity})
    this.setState({heparin_init_unit:heparin_init_unit})
    this.setState({maintenance_unit:maintenance_unit})
    this.setState({LMW_unit:LMW_unit})
    this.setState({nss_flush:nss_flush})
    this.setState({q:q})
    this.setState({pre_time:pre_time})
    this.setState({BW:BW})
    this.setState({DW:DW})
    this.setState({weight_gain:weight_gain})
    this.setState({last_BW:last_BW})
    this.setState({UFG:UFG})
    this.setState({set_UF:set_UF})
    this.setState({dialysis_len:dialysis_len})
    var local_hd_timeonh =window.hd_timeon.split(":")[0]
    var local_hd_timeonm =window.hd_timeon.split(":")[1].split(" ")[0]
    this.setState({hd_timeonh:local_hd_timeonh})
    this.setState({hd_timeonm:local_hd_timeonm})
    
    var local_hd_timeoffh =window.hd_timeoff.split(":")[0]
    var local_hd_timeoffm =window.hd_timeoff.split(":")[1].split(" ")[0]
    this.setState({hd_timeoffh:local_hd_timeoffh})
    this.setState({hd_timeoffm:local_hd_timeoffm})
    this.setState({})
    this.setState({})
    
}
handleBack = ()=>{
    var local_hd_timeon
    var local_hd_timeoff

    local_hd_timeon=this.state.hd_timeonh+":"+this.state.hd_timeonm+" "+"น."
    local_hd_timeoff=this.state.hd_timeoffh+":"+this.state.hd_timeoffm+" "+"น."
    
    window.heparin_init_dose=this.state.heparin_init_dose
    window.LMW=this.state.LMW
    window.no_heparin=this.state.no_heparin
    window.dialysate_flow=this.state.dialysate_flow
    window.dialysate_temp=this.state.dialysate_temp
    window.conductivity=this.state.conductivity
    window.heparin_init_unit=this.state.heparin_init_unit
    window.maintenance_unit=this.state.maintenance_unit
    window.LMW_unit=this.state.LMW_unit
    window.nss_flush=this.state.nss_flush
    window.q=this.state.q
    window.pre_time=this.state.pre_time
    window.BW=this.state.BW
    window.DW=this.state.DW
    window.weight_gain=this.state.weight_gain
    window.last_BW=this.state.last_BW
    window.UFG=this.state.UFG
    window.set_UF=this.state.set_UF
    window.dialysis_len=this.state.dialysis_len
    window.hd_timeon=local_hd_timeon
    window.hd_timeoff=local_hd_timeoff
    console.log(window.set_UF)
    
    this.goToForm4()

}

handleSubmit = values => {

    var local_hd_timeon
    var local_hd_timeoff

    local_hd_timeon=this.state.hd_timeonh+":"+this.state.hd_timeonm+" "+"น."
    local_hd_timeoff=this.state.hd_timeoffh+":"+this.state.hd_timeoffm+" "+"น."
    
    window.heparin_init_dose=this.state.heparin_init_dose
    window.LMW=this.state.LMW
    window.no_heparin=this.state.no_heparin
    window.dialysate_flow=this.state.dialysate_flow
    window.dialysate_temp=this.state.dialysate_temp
    window.conductivity=this.state.conductivity
    window.heparin_init_unit=this.state.heparin_init_unit
    window.maintenance_unit=this.state.maintenance_unit
    window.LMW_unit=this.state.LMW_unit
    window.nss_flush=this.state.nss_flush
    window.q=this.state.q
    window.pre_time=this.state.pre_time
    window.BW=this.state.BW
    window.DW=this.state.DW
    window.weight_gain=this.state.weight_gain
    window.last_BW=this.state.last_BW
    window.UFG=this.state.UFG
    window.set_UF=this.state.set_UF
    window.dialysis_len=this.state.dialysis_len
    window.hd_timeon=local_hd_timeon
    window.hd_timeoff=local_hd_timeoff
    console.log(window.set_UF)
    
    this.goToForm6()
}

hOrNo = key =>{
    var h="h"
    if(!h.localeCompare(key)){
        this.setState({heparin_init_dose:!this.state.heparin_init_dose})
        this.setState({no_heparin:false})
    }
    else{
        this.setState({heparin_init_dose:false})
        this.setState({no_heparin:!this.state.no_heparin})
    }
}
arabic = "&#8451;"; 
entities = new Html5Entities();
co =()=>{
    return <Text style={{fontSize:hp('3')}}>( {this.entities.decode(this.arabic)} )</Text>

}
arabic2 = "q&#772;"; 
q=()=>{
    return <Text style={{fontSize:hp('3')}}>{this.entities.decode(this.arabic2)} (hrs.)</Text>
}

  render() {
    return (
        <Formik
        initialValues={{dialysate_flow:'',
                        dialysate_temp:'',
                        conductivity:'',
                        heparin_init_unit:'',
                        maintenance_unit:'',
                        LMW_unit:'',
                        nss_flush:'',
                        q:'',
                        pre_time:'',
                        BW:'',
                        DW:'',
                        weight_gain:'',
                        last_BW:'',
                        UFG:'',
                        set_UF:'',
                        dialysis_len:'',
                        hd_timeonh:'',
                        hd_timeonm:'',
                        hd_timeoffh:'',
                        hd_timeoffm:'',
                        }}
        onSubmit={values => {
          this.handleSubmit(values)
        }}
        >
        {({ handleChange, values, handleSubmit}) => (
            <SafeAreaView style={styles.container}>
            <KeyboardAwareScrollView>
            <View style={styles.up}>
            <View style={styles.up_line}>
            <View style={styles.up_left}>
                    <Ionicons.Button
                      name={'ios-arrow-back'}
                      size={hp('4')}
                      color='#000000'
                      backgroundColor='#fff'
                      onPress={this.handleBack}
                      title="back"
                    />
              </View>
            <View style={styles.up_right}>
                  <Text style={styles.head}>PRESCRIPTION</Text>
            </View>
            <View style={styles.up_right_page}>
                  <Text style={styles.head}>5/11</Text>
            </View>
          </View>
          </View>
            <Grid>
                <Col size={50} style={styles.left_content}>
                <View style={{flexDirection:'column'}}>
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Dialysate Flow (ml/min)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('46'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(dialysate_flow) => this.setState({dialysate_flow})}
                                    value={this.state.dialysate_flow}
                                    fontSize={hp('2.6')}
                                    onSubmitEditing={() => { this.t2.focus(); }}
                                    blurOnSubmit={false}

                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                        <Text style={{fontSize:hp('2.6')}}>Dialysate Temp {this.co()}</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('46'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(dialysate_temp) => this.setState({dialysate_temp})}
                                    value={this.state.dialysate_temp}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t2 = input; }}
                                    onSubmitEditing={() => { this.t3.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                        <Text style={{fontSize:hp('2.6')}}>Conductivity (mS/cm)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('46'),height:hp('4.4'),marginLeft:hp('2'),marginBottom:wp('2'),borderColor:'grey'}}
                                    onChangeText={(conductivity) => this.setState({conductivity})}
                                    value={this.state.conductivity}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t3 = input; }}
                                    onSubmitEditing={() => { this.t4.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'column',padding:hp('2'),justifyContent:'flex-start',borderTopWidth:StyleSheet.hairlineWidth}}>
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                        <Text style={{fontSize:hp('2.6')}}>Anicoagulant</Text>
                                </View>
                                <View style={{flexDirection:'row'}}>
                                    <CheckBox
                                            title='HAD Heparin initial dose (Unit)'
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            checked={this.state.heparin_init_dose}
                                            onPress={()=>{this.hOrNo("h")}}
                                            size={wp('2')}
                                            textStyle={{
                                            fontSize:hp('2.6')
                                            
                                            }}    
                                            containerStyle={{
                                                width:wp('32')
                                            }} 
                                    />
                                    <TextInput
                                        style={{borderBottomWidth:StyleSheet.hairlineWidth,width:wp('13'),height:hp('6.4'),borderColor:'grey'}}
                                        onChangeText={(heparin_init_unit) => this.setState({heparin_init_unit})}
                                        value={this.state.heparin_init_unit}
                                        placeholder='Please fill in ...'
                                        fontSize={hp('2.6')}
                                        ref={(input) => { this.t4 = input; }}
                                        onSubmitEditing={() => { this.t5.focus(); }}
                                        blurOnSubmit={false}
                                    />
                                </View>
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                        <Text style={{fontSize:hp('2.6')}}>Maintenance dose (Unit/hr.)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('40'),height:hp('4.4'),marginLeft:hp('2'),marginBottom:wp('2'),borderColor:'grey'}}
                                    onChangeText={(maintenance_unit) => this.setState({maintenance_unit})}
                                    value={this.state.maintenance_unit}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t5 = input; }}
                                    onSubmitEditing={() => { this.t6.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row'}}>
                                    <CheckBox
                                            title='LMW (UNIT) (Bolus Dose)'
                                            checked={this.state.LMW}
                                            onPress={() => this.setState({LMW: !this.state.LMW})}
                                            size={wp('2')}
                                            textStyle={{
                                            fontSize:hp('2.6')
                                            
                                            }}    
                                            containerStyle={{
                                                width:wp('32')
                                            }} 
                                    />
                                    <TextInput
                                        style={{borderBottomWidth:StyleSheet.hairlineWidth,width:wp('13'),height:hp('6.4'),borderColor:'grey'}}
                                        onChangeText={(LMW_unit) => this.setState({LMW_unit})}
                                        value={this.state.LMW_unit}
                                        placeholder='Please fill in ...'
                                        fontSize={hp('2.6')}
                                        ref={(input) => { this.t6 = input; }}
                                        onSubmitEditing={() => { this.t7.focus(); }}
                                        blurOnSubmit={false}
                                    />
                                </View>
                                <View style={{flexDirection:'row'}}>
                                    <CheckBox
                                            title='No Heparin'
                                            checked={this.state.no_heparin}
                                            onPress={()=>{this.hOrNo("no")}}
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            size={wp('2')}
                                            textStyle={{
                                            fontSize:hp('2.6')
                                            
                                            }}    
                                            containerStyle={{
                                                width:wp('32')
                                            }} 
                                    />
                                </View>
                        </View>
                </View>
                </Col>
                <Col size={25} style={styles.mid_content}>
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>NSS Flush(ml)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(nss_flush) => this.setState({nss_flush})}
                                    value={this.state.nss_flush}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t7 = input; }}
                                    onSubmitEditing={() => { this.t8.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                        <Text style={{fontSize:hp('2.6')}}>{this.q()}</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(q) => this.setState({q})}
                                    value={this.state.q}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t8 = input; }}
                                    onSubmitEditing={() => { this.t9.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Time</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(pre_time) => this.setState({pre_time})}
                                    value={this.state.pre_time}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t9 = input; }}
                                    onSubmitEditing={() => { this.t10.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>BW (kg)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(BW) => this.setState({BW})}
                                    value={this.state.BW}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t10 = input; }}
                                    onSubmitEditing={() => { this.t11.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>DW (kg)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(DW) => this.setState({DW})}
                                    value={this.state.DW}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t11 = input; }}
                                    onSubmitEditing={() => { this.t12.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Weight gain (kg)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(weight_gain) => this.setState({weight_gain})}
                                    value={this.state.weight_gain}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t12 = input; }}
                                    onSubmitEditing={() => { this.t13.focus(); }}
                                    blurOnSubmit={false}
                                />
                </Col>
                <Col size={25} style={styles.right_content}>
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>LAST BW (kg)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(last_BW) => this.setState({last_BW})}
                                    value={this.state.last_BW}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t13 = input; }}
                                    onSubmitEditing={() => { this.t14.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>UFG (litre)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(UFG) => this.setState({UFG})}
                                    value={this.state.UFG}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t14 = input; }}
                                    onSubmitEditing={() => { this.t15.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Set UF (ml)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(set_UF) => this.setState({set_UF})}
                                    value={this.state.set_UF}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t15 = input; }}
                                    onSubmitEditing={() => { this.t16.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Dialysis Length (hrs.)</Text>
                                </View>
                                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                    onChangeText={(dialysis_len) => this.setState({dialysis_len})}
                                    value={this.state.dialysis_len}
                                    fontSize={hp('2.6')}
                                    ref={(input) => { this.t16 = input; }}
                                    onSubmitEditing={() => { this.t17.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Start HD : Time ON</Text>
                                </View>
                                <View style={{flexDirection:'row'}}>
                                    <TextInput
                                            style={{borderWidth:StyleSheet.hairlineWidth,width:wp('8'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                            onChangeText={(hd_timeonh) => this.setState({hd_timeonh})}
                                            value={this.state.hd_timeonh}
                                            fontSize={hp('2.6')}
                                            placeholder=" ชม."
                                            ref={(input) => { this.t17 = input; }}
                                            onSubmitEditing={() => { this.t18.focus(); }}
                                            blurOnSubmit={false}
                                        />
                                        <View style={{paddingLeft:wp('1')}}>
                                            <Text style={{fontSize:hp('3')}}>
                                                :
                                            </Text>
                                        </View>
                                    <TextInput
                                        style={{borderWidth:StyleSheet.hairlineWidth,width:wp('8'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                        onChangeText={(hd_timeonm) => this.setState({hd_timeonm})}
                                        value={this.state.hd_timeonm}
                                        fontSize={hp('2.6')}
                                        placeholder=" นาที"
                                        ref={(input) => { this.t18 = input; }}
                                        onSubmitEditing={() => { this.t19.focus(); }}
                                        blurOnSubmit={false}
                                    />
                                    </View>
                                    <View style={{flexDirection:'row',padding:hp('2')}}>
                                    <Text style={{fontSize:hp('2.6')}}>Time OFF</Text>
                                </View>
                                <View style={{flexDirection:'row'}}>
                                    <TextInput
                                            style={{borderWidth:StyleSheet.hairlineWidth,width:wp('8'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                            onChangeText={(hd_timeoffh) => this.setState({hd_timeoffh})}
                                            value={this.state.hd_timeoffh}
                                            fontSize={hp('2.6')}
                                            placeholder=" ชม."
                                            ref={(input) => { this.t19 = input; }}
                                            onSubmitEditing={() => { this.t20.focus(); }}
                                            blurOnSubmit={false}
                                        />
                                        <View style={{paddingLeft:wp('1')}}>
                                            <Text style={{fontSize:hp('3')}}>
                                                :
                                            </Text>
                                        </View>
                                    <TextInput
                                        style={{borderWidth:StyleSheet.hairlineWidth,width:wp('8'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                        onChangeText={(hd_timeoffm) => this.setState({hd_timeoffm})}
                                        value={this.state.hd_timeoffm}
                                        fontSize={hp('2.6')}
                                        placeholder=" นาที"
                                        ref={(input) => { this.t20 = input; }}
                                        
                                    />
                                    </View>
                                    <View style={{flexDirection:'row',justifyContent:'flex-end',padding:wp('2')}}>
                                        <Button
                                                    onPress={handleSubmit}
                                                    title='Next'
                                                    containerStyle={{
                                                    width:wp('10')
                                                    }}
                                                />
                                        </View>
                </Col>
            </Grid>
            </KeyboardAwareScrollView>

          </SafeAreaView>
        )}
        </Formik>
    )
}}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      
    },
    up_left:{
      justifyContent:'flex-start',
      alignItems:'flex-start',
      width:wp('10%'),
      
    },
    up_right_page:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      width:wp('10%'),
      padding:hp('2')
    },
    up_line:{
      flexDirection:'row',
      
    },
    up_right:{
      width:wp('80%'),
      justifyContent:'center',
      alignItems:'center',
    },
    head:{
      fontSize:hp('2.6%'),
      fontWeight:'bold',
  
    },
    left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }

  })