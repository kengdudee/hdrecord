import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
// import { TextInput } from 'react-native-gesture-handler';
import axios from 'axios';
import AwesomeAlert from 'react-native-awesome-alerts';
import { Button, CheckBox } from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
console.disableYellowBox = true;
import  CNRichTextEditor , { CNToolbar , getDefaultStyles, convertToObject ,getInitialObject ,convertToHtmlString} from "react-native-cn-richtext-editor";


const monthNames = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
  "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"
];
const dayNames = ["วันอาทิตย์","วันจันทร์","วันอังคาร","วันพุธ","วันพฤหัสบดี","วันศุกร์","วันเสาร์"]
export default class Form8 extends React.Component {

goToForm7 = () => this.props.navigation.navigate('Form7')
goToForm11 = () => this.props.navigation.navigate('Form11')
goToForm9 = () => this.props.navigation.navigate('Form9')
goToHome = () => this.props.navigation.navigate('App')


constructor(props) {
    super(props);
    this.state = {
      eph_nocom:false,
      eph_osat:false,
      eph_fb:false,
      eph_eb:false,
      eph_ad:false,
      eph_hn:false,
      eph_nop:false,
      eph_s:false,
      eph_com:false,
      eph_hypo:false,
      eph_hyper:false,
      eph_arr:false,
      eph_cp:false,
      eph_mc:false,
      eph_ap:false,
      eph_cc:false,
      showAlert:false
    };
}
showAlert = () => {
  this.setState({
    showAlert: true
  });
};
hideAlert = () => {
  this.setState({
    showAlert: false
  });
};
async componentDidMount(){

  this.setState({eph_nocom:window.eph_nocom})
  this.setState({eph_osat:window.eph_osat})
  this.setState({eph_fb:window.eph_fb})
  this.setState({eph_eb:window.eph_eb})
  this.setState({eph_ad:window.eph_ad})
  this.setState({eph_hn:window.eph_hn})
  this.setState({eph_nop:window.eph_nop})
  this.setState({eph_s:window.eph_s})
  this.setState({eph_com:window.eph_com})
  this.setState({eph_hypo:window.eph_hypo})
  this.setState({eph_hyper:window.eph_hyper})
  this.setState({eph_arr:window.eph_arr})
  this.setState({eph_cp:window.eph_cp})
  this.setState({eph_mc:window.eph_mc})
  this.setState({eph_ap:window.eph_ap})
  this.setState({eph_cc:window.eph_cc})


}
handleBack = () => {
    
  window.eph_nocom=this.state.eph_nocom
  window.eph_osat=this.state.eph_osat
  window.eph_fb=this.state.eph_fb
  window.eph_eb=this.state.eph_eb
  window.eph_ad=this.state.eph_ad
  window.eph_hn=this.state.eph_hn
  window.eph_nop=this.state.eph_nop
  window.eph_s=this.state.eph_s
  window.eph_com=this.state.eph_com
  window.eph_hypo=this.state.eph_hypo
  window.eph_hyper=this.state.eph_hyper
  window.eph_arr=this.state.eph_arr
  window.eph_cp=this.state.eph_cp
  window.eph_mc=this.state.eph_mc
  window.eph_ap=this.state.eph_ap
  window.eph_cc=this.state.eph_cc
  
  
  this.goToForm11()
}

async submitRecord(){

  var time = new Date();

  var newmin=time.getMinutes()
  if(parseInt(newmin)<10){
  newmin='0'+newmin
  }

  var local_record_timestamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
  var year = time.getFullYear()+543

  window.created=time.getDate() + ' ' + monthNames[time.getMonth()] + ' ' + year + ' ' + time.getHours() + ':' + newmin           
  //push to db here
  var url = 'http://'+window.ip_server+':'+window.port_server+'/addRecord';
      axios.post(url, {
          
      acute:+window.acute,
      chronic:+window.chronic,
      CGD:+window.CGD,
      SSS:+window.SSS,
      NHSO:+window.NHSO,
      state_enterprise:+window.state_enterprise,
      self_pay:+window.self_pay,
      patient:window.patient,
      prefix_patient:window.prefix_patient,
      first_patient:window.first_patient,
      last_patient:window.last_patient,
      other_patient:window.other_patient,
      id:window.id,
      HN:window.HN,
      HD_no:window.HD_no,
      date:window.date,
      ward:window.ward,
      diagnosis:window.diagnosis,
      notice:window.notice,
      nurse_assign:window.nurse_assign,
      //form2
      aph_e:+window.aph_e,
      aph_lc:+window.aph_lc,
      aph_env:+window.aph_env,
      aph_cd:+window.aph_cd,
      aph_d:+window.aph_d,
      aph_cp:+window.aph_cp,
      aph_p:+window.aph_p,
      aph_f:+window.aph_f,
      aph_nv:+window.aph_nv,
      aph_h:+window.aph_h,
      aph_bt:+window.aph_bt,
      aph_i:+window.aph_i,
      aph_ano:+window.aph_ano,
      aph_anx:+window.aph_anx,
      aph_sd:+window.aph_sd,
      aph_c:+window.aph_c,
      aph_ps:+window.aph_ps,
      aph_other:+window.aph_other,
      aph_e_n:+window.aph_e_n,
      aph_lc_n:+window.aph_lc_n,
      aph_env_n:+window.aph_env_n,
      aph_cd_n:+window.aph_cd_n,
      aph_d_n:+window.aph_d_n,
      aph_cp_n:+window.aph_cp_n,
      aph_p_n:+window.aph_p_n,
      aph_f_n:+window.aph_f_n,
      aph_nv_n:+window.aph_nv_n,
      aph_h_n:+window.aph_h_n,
      aph_bt_n:+window.aph_bt_n,
      aph_i_n:+window.aph_i_n,
      aph_ano_n:+window.aph_ano_n,
      aph_anx_n:+window.aph_anx_n,
      aph_sd_n:+window.aph_sd_n,
      aph_c_n:+window.aph_c_n,
      aph_ps_n:+window.aph_ps_n,
      aph_other_desc:window.aph_other_desc,
      //form3
      DLC:+window.DLC,
      PERM:+window.PERM,
      AVF:+window.AVF,
      AVG:+window.AVG,
      thrill:+window.thrill,
      bruit:+window.bruit,
      catheter_site:window.catheter_site,
      insertion_date:window.insertion_date,
      needle_no:window.needle_no,
      characteristics_found:window.characteristics_found,
      BP:window.BP,
      PR:window.PR,
      RR:window.RR,
      sign_symp:window.sign_symp,
      //form4
      normal_formula:+window.normal_formula,
      v_Na:+window.v_Na,
      v_K:+window.v_K,
      v_HCO3:+window.v_HCO3,
      v_other:+window.v_other,
      physician:window.physician,
      machine:window.machine,
      self_tpb:window.self_tpb,
      dialyzer:window.dialyzer,
      AS_m2:window.AS,
      Kuf:window.Kuf,
      TCV:window.TCV,
      last_TCV:window.last_TCV,
      use_no:window.use_no,
      v_Na_unit:window.v_Na_unit,
      v_K_unit:window.v_K_unit,
      v_HCO3_unit:window.v_HCO3_unit,
      v_other_desc:window.v_other_desc,
      //form5
      heparin_init_dose:+window.heparin_init_dose,
      LMW:+window.LMW,
      no_heparin:+window.no_heparin,
      dialysate_flow:window.dialysate_flow,
      dialysate_temp:window.dialysate_temp,
      conductivity:window.conductivity,
      heparin_init_unit:window.heparin_init_unit,
      maintenance_unit:window.maintenance_unit,
      LMW_unit:window.LMW_unit,
      nss_flush:window.nss_flush,
      q:window.q,
      pre_time:window.pre_time,
      BW:window.BW,
      DW:window.DW,
      weight_gain:window.weight_gain,
      last_BW:window.last_BW,
      UFG:window.UFG,
      set_UF:window.set_UF,
      dialysis_len:window.dialysis_len,
      hd_timeon:window.hd_timeon,
      hd_timeoff:window.hd_timeoff,
      //form6
      nd_u:+window.nd_u,
      nd_fve:+window.nd_fve,
      nd_ei:+window.nd_ei,
      nd_arr:+window.nd_arr,
      nd_ane:+window.nd_ane,
      nd_cf:+window.nd_cf,
      nd_rh:+window.nd_rh,
      nd_ri:+window.nd_ri,
      nd_dfv:+window.nd_dfv,
      nd_i:+window.nd_i,
      nd_m:+window.nd_m,
      nd_scd:+window.nd_scd,
      nd_rf:+window.nd_rf,
      nd_rcdd:+window.nd_rcdd,
      nd_other:+window.nd_other,
      nd_u_p:window.nd_u_p,
      nd_fve_p:window.nd_fve_p,
      nd_ei_p:window.nd_ei_p,
      nd_arr_p:window.nd_arr_p,
      nd_ane_p:window.nd_ane_p,
      nd_cf_p:window.nd_cf_p,
      nd_rh_p:window.nd_rh_p,
      nd_ri_p:window.nd_ri_p,
      nd_dfv_p:window.nd_dfv_p,
      nd_i_p:window.nd_i_p,
      nd_m_p:window.nd_m_p,
      nd_scd_p:window.nd_scd_p,
      nd_rf_p:window.nd_rf_p,
      nd_rcdd_p:window.nd_rcdd_p,
      nd_other_p:window.nd_other_p,
      nd_rcdd_desc:window.nd_rcdd_desc,
      nd_other_desc:window.nd_other_desc,
      //form7
      i_mwe:+window.i_mwe,
      i_ot:+window.i_ot,
      i_pu:+window.i_pu,
      i_rf:+window.i_rf,
      i_t:+window.i_t,
      i_ldt:+window.i_ldt,
      i_i:+window.i_i,
      i_n:+window.i_n,
      i_bt:+window.i_bt,
      i_oc:+window.i_oc,
      i_maf:+window.i_maf,
      i_cbd:+window.i_cbd,
      i_se:+window.i_se,
      i_ps:+window.i_ps,
      i_nd:+window.i_nd,
      i_other:+window.i_other,
      i_other_desc:window.i_other_desc,
      //form8
      eph_nocom:+window.eph_nocom,
      eph_osat:+window.eph_osat,
      eph_fb:+window.eph_fb,
      eph_eb:+window.eph_eb,
      eph_ad:+window.eph_ad,
      eph_hn:+window.eph_hn,
      eph_nop:+window.eph_nop,
      eph_s:+window.eph_s,
      eph_com:+window.eph_com,
      eph_hypo:+window.eph_hypo,
      eph_hyper:+window.eph_hyper,
      eph_arr:+window.eph_arr,
      eph_cp:+window.eph_cp,
      eph_mc:+window.eph_mc,
      eph_ap:+window.eph_ap,
      eph_cc:+window.eph_cc,
      //form9
      tableHead: JSON.stringify(window.tableHead),
      tableO2sat: JSON.stringify(window.tableO2sat),
      tableQB: JSON.stringify(window.tableQB),
      tableVP: JSON.stringify(window.tableVP),
      tableTMP: JSON.stringify(window.tableTMP),
      tableUFR: JSON.stringify(window.tableUFR),
      tableUF: JSON.stringify(window.tableUF),
      g_time: JSON.stringify(window.g_time),
      g_bpup: JSON.stringify(window.g_bpup),
      g_bpdown: JSON.stringify(window.g_bpdown),
      g_pr: JSON.stringify(window.g_pr),
      g_rr: JSON.stringify(window.g_rr),
      tableSubmitTime: JSON.stringify(window.tableSubmitTime),
      //form10
      av_shunt_thrill:+window.av_shunt_thrill,
      av_shunt_bruit:+window.av_shunt_bruit,
      problem:+window.problem,
      nurse_note:convertToHtmlString(window.nurse_note_object),
      post_BP:window.post_BP,
      pulse:window.pulse,
      post_RR:window.post_RR,
      post_BW:window.post_BW,
      weight_loss:window.weight_loss,
      problem_desc:window.problem_desc,
      TPN:window.TPN,
      venofer:window.venofer,
      ESA:window.ESA,
      //form11
      refer_to_home:+window.refer_to_home,
      refer_to_ward:+window.refer_to_ward,
      NSS:window.NSS,
      glucose:window.glucose,
      extra_f:window.extra_f,
      total_intake:window.total_intake,
      total_UF:window.total_UF,
      net_UF:window.net_UF,
      doctor_note:convertToHtmlString(window.doctor_note_object),
      refer_to_ward_desc:window.refer_to_ward_desc,
      next_date:window.next_date,
      next_time:window.next_time,
      created:window.created,
      record_timestamp:local_record_timestamp,
      toShow:+window.toShow,
      
      })
      .then(function (response) {
      //console.log(response);
      })
      .catch(function (error) {
      //console.log(error);
      });

  this.goToHome()


}

handleSubmit = values => {
    
    window.eph_nocom=this.state.eph_nocom
    window.eph_osat=this.state.eph_osat
    window.eph_fb=this.state.eph_fb
    window.eph_eb=this.state.eph_eb
    window.eph_ad=this.state.eph_ad
    window.eph_hn=this.state.eph_hn
    window.eph_nop=this.state.eph_nop
    window.eph_s=this.state.eph_s
    window.eph_com=this.state.eph_com
    window.eph_hypo=this.state.eph_hypo
    window.eph_hyper=this.state.eph_hyper
    window.eph_arr=this.state.eph_arr
    window.eph_cp=this.state.eph_cp
    window.eph_mc=this.state.eph_mc
    window.eph_ap=this.state.eph_ap
    window.eph_cc=this.state.eph_cc

    this.showAlert()

    
    
    //this.goToForm9()
}

  render() {

    const { showAlert } = this.state;

    return (
        <Formik
            initialValues={{}}
            onSubmit={values => {
            this.handleSubmit(values)
            }}
        >
        {({handleSubmit}) => (
            <SafeAreaView style={styles.container}>
              <KeyboardAwareScrollView>
            <View style={styles.up}>
            <View style={styles.up_line}>
            <View style={styles.up_left}>
                    <Ionicons.Button
                      name={'ios-arrow-back'}
                      size={hp('4')}
                      color='#000000'
                      backgroundColor='#fff'
                      onPress={this.handleBack}
                      title="back"
                    />
              </View>
            <View style={styles.up_right}>
                  <Text style={styles.head}>EVALUATION post HD</Text>
            </View>
            <View style={styles.up_right_page}>
                  <Text style={styles.head}>11/11</Text>
            </View>
          </View>
          </View>
          <Grid>
            <Col size={50} style={styles.left_content}>
            <View style={{flexDirection:'column'}}>
                    <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='No complication'
                            checked={this.state.eph_nocom}
                            onPress={() => this.setState({eph_nocom: !this.state.eph_nocom})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='O2 sat > 95%'
                            // title=""
                            checked={this.state.eph_osat}
                            onPress={() => this.setState({eph_osat: !this.state.eph_osat})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Fluid balance'
                            checked={this.state.eph_fb}
                            onPress={() => this.setState({eph_fb: !this.state.eph_fb})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Electrolyte balance'
                            checked={this.state.eph_eb}
                            onPress={() => this.setState({eph_eb: !this.state.eph_eb})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Adequate Dialysis'
                            checked={this.state.eph_ad}
                            onPress={() => this.setState({eph_ad: !this.state.eph_ad})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Have nutrition'
                            checked={this.state.eph_hn}
                            onPress={() => this.setState({eph_hn: !this.state.eph_hn})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='No pain'
                            checked={this.state.eph_nop}
                            onPress={() => this.setState({eph_nop: !this.state.eph_nop})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Safety/No accident'
                            checked={this.state.eph_s}
                            onPress={() => this.setState({eph_s: !this.state.eph_s})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        
                </View>
            </Col>
            <Col size={50} style={styles.right_content}> 
            <View style={{flexDirection:'column'}}>
                    <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Complication'
                            checked={this.state.eph_com}
                            onPress={() => this.setState({eph_com: !this.state.eph_com})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Hypotension'
                            checked={this.state.eph_hypo}
                            onPress={() => this.setState({eph_hypo: !this.state.eph_hypo})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Hypertension'
                            checked={this.state.eph_hyper}
                            onPress={() => this.setState({eph_hyper: !this.state.eph_hyper})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Arrhythmia'
                            checked={this.state.eph_arr}
                            onPress={() => this.setState({eph_arr: !this.state.eph_arr})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                           title='Chest pain'
                           checked={this.state.eph_cp}
                           onPress={() => this.setState({eph_cp: !this.state.eph_cp})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Muscle clamp'
                            checked={this.state.eph_mc}
                            onPress={() => this.setState({eph_mc: !this.state.eph_mc})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Access problem'
                            checked={this.state.eph_ap}
                            onPress={() => this.setState({eph_ap: !this.state.eph_ap})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <CheckBox
                            title='Circuit clotted'
                            checked={this.state.eph_cc}
                            onPress={() => this.setState({eph_cc: !this.state.eph_cc})}
                            size={wp('3')}
                            textStyle={{
                            fontSize:hp('2.6')
                            }}
                            containerStyle={{
                                    width:wp('48'),
                                    marginBottom:wp('1'),
                                    marginTop:wp('1')
                            }}       
                            
                        />
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'flex-end',padding:wp('2')}}>
                                        <Button
                                                    onPress={handleSubmit}
                                                    title='Done'
                                                    containerStyle={{
                                                    width:wp('10')
                                                    }}
                                                />
                         </View>
                        
                </View>
            </Col>
            </Grid>
            </KeyboardAwareScrollView>
            <AwesomeAlert
          show={showAlert}
          showProgress={false}
          title="Are you sure to save this form?"
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="Save"
          confirmButtonColor="blue"
          cancelButtonColor="#606060"
          onCancelPressed={() => {
            //window.deleteKey=''
            this.hideAlert();
          }}
          onConfirmPressed={() => {
            //this.toDeleteRecord(window.deleteKey)
            this.submitRecord()
            this.hideAlert()
          }}
        />
            </SafeAreaView>
            

        )}
        </Formik>
        
    )
    

}}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      
    },
    up_left:{
      justifyContent:'flex-start',
      alignItems:'flex-start',
      width:wp('10%'),
      
    },
    up_right_page:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      width:wp('10%'),
      padding:hp('2')
    },
    up_line:{
      flexDirection:'row',
      
    },
    up_right:{
      width:wp('80%'),
      justifyContent:'center',
      alignItems:'center',
    },
    head:{
      fontSize:hp('2.6%'),
      fontWeight:'bold',
  
    },
    left_content:{
      borderTopWidth: StyleSheet.hairlineWidth,
      borderLeftWidth: StyleSheet.hairlineWidth,
      borderRightWidth: StyleSheet.hairlineWidth
    },
    mid_content:{
      borderTopWidth: StyleSheet.hairlineWidth,
      borderRightWidth: StyleSheet.hairlineWidth
    },
    right_content:{
      borderTopWidth: StyleSheet.hairlineWidth,
      borderRightWidth: StyleSheet.hairlineWidth
    }

  })