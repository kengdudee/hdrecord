import React from 'react'
import { StyleSheet, Text, View, Button, Dimensions, Image, ScrollView, SafeAreaView, Alert } from 'react-native'
import {Header} from 'react-native-elements'
import { TextInput } from 'react-native-gesture-handler'
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import { CheckBox, ListItem, SearchBar } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
console.disableYellowBox = true;
import axios from 'axios';
export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        search:'',
        alist:[],
        clist:[],
        acuteFound:0,
        chronicFound:0,
        prefixChecked:true,
        firstNameChecked:true,
        lastNameChecked:true,
        dateChecked:true,
        monthChecked:true,
        yearChecked:true,
        unCheckAll:false,
        selectOrNot:'เลือก / ไม่เลือก ทั้งหมด',
        showAlert:false,
    };
  }
  
  async componentDidMount() {
    
  }

goToHome = () => this.props.navigation.navigate('App')

updateSearch = search => {
  //console.log(window.patientList)
    
    this.setState({ search });
    
    //DON'T USE indexOf!!!

    if(this.state.prefixChecked&&this.state.firstNameChecked&&this.state.lastNameChecked&&this.state.monthChecked&&this.state.dateChecked&&this.state.yearChecked){
      
      var indexlist=[]
    
      for(var i=0;i<window.patientList.length;i++){
        if(window.patientList[i].includes(search)){
            indexlist.push(i)
        }
      }
      if(indexlist.length==0){
      for(var i=0;i<window.createdList.length;i++){
        if(window.createdList[i].includes(search)){
            indexlist.push(i)
        }
      }}

    indexlist.reverse()
    var localalist=[]
    var localclist=[]
    var j=0
    var k=0
    var aFound=0
    var cFound=0

      for(var i=0;i<indexlist.length;i=i+1){
        if(indexlist.length!=0){
        if(!window.typeList[indexlist[i]].localeCompare('Acute')){
            aFound=aFound+1
            localalist.push({})
            localalist[j].title = window.patientList[indexlist[i]]
            localalist[j].icon="description"
            localalist[j].subtitle=window.createdList[indexlist[i]]
            localalist[j].rightTitle=window.keyList[indexlist[i]]
  
            j=j+1
        }
        else{
            cFound=cFound+1
            localclist.push({})
            localclist[k].title=window.patientList[indexlist[i]]
            localclist[k].icon="description"
            localclist[k].subtitle=window.createdList[indexlist[i]]
            localclist[k].rightTitle=window.keyList[indexlist[i]]
  
            k=k+1
        }
      }
          
    }
      
      this.setState({alist:localalist})
      this.setState({clist:localclist})
  
      this.setState({acuteFound:indexlist.length-cFound})
      this.setState({chronicFound:indexlist.length-aFound})
  
    
    
  }

  if(this.state.prefixChecked&&!this.state.firstNameChecked&&!this.state.lastNameChecked&&!this.state.monthChecked&&!this.state.dateChecked&&!this.state.yearChecked){

    //prefix

    var indexlist=[]

    for(var i=0;i<window.prefixList.length;i++){
      if(window.prefixList[i].includes(search)){
          indexlist.push(i)
      }
    }
    indexlist.reverse()
    var localalist=[]
    var localclist=[]
    var j=0
    var k=0
    var aFound=0
    var cFound=0

      for(var i=0;i<indexlist.length;i=i+1){
        if(indexlist.length!=0){
        if(!window.typeList[indexlist[i]].localeCompare('Acute')){
            aFound=aFound+1
            localalist.push({})
            localalist[j].title = window.patientList[indexlist[i]]
            localalist[j].icon="description"
            localalist[j].subtitle=window.createdList[indexlist[i]]
            localalist[j].rightTitle=window.keyList[indexlist[i]]
  
            j=j+1
        }
        else{
            cFound=cFound+1
            localclist.push({})
            localclist[k].title=window.patientList[indexlist[i]]
            localclist[k].icon="description"
            localclist[k].subtitle=window.createdList[indexlist[i]]
            localclist[k].rightTitle=window.keyList[indexlist[i]]
  
            k=k+1
        }
      }
          
    }
      
      this.setState({alist:localalist})
      this.setState({clist:localclist})
  
      this.setState({acuteFound:indexlist.length-cFound})
      this.setState({chronicFound:indexlist.length-aFound})
  
    
    
  }

  if(!this.state.prefixChecked&&this.state.firstNameChecked&&!this.state.lastNameChecked&&!this.state.monthChecked&&!this.state.dateChecked&&!this.state.yearChecked){
      //firstname
    var indexlist=[]

    for(var i=0;i<window.firstNameList.length;i++){
      if(window.firstNameList[i].includes(search)){
          indexlist.push(i)
      }
    }
    indexlist.reverse()
    var localalist=[]
    var localclist=[]
    var j=0
    var k=0
    var aFound=0
    var cFound=0

      for(var i=0;i<indexlist.length;i=i+1){
        if(indexlist.length!=0){
        if(!window.typeList[indexlist[i]].localeCompare('Acute')){
            aFound=aFound+1
            localalist.push({})
            localalist[j].title = window.patientList[indexlist[i]]
            localalist[j].icon="description"
            localalist[j].subtitle=window.createdList[indexlist[i]]
            localalist[j].rightTitle=window.keyList[indexlist[i]]
  
            j=j+1
        }
        else{
            cFound=cFound+1
            localclist.push({})
            localclist[k].title=window.patientList[indexlist[i]]
            localclist[k].icon="description"
            localclist[k].subtitle=window.createdList[indexlist[i]]
            localclist[k].rightTitle=window.keyList[indexlist[i]]
  
            k=k+1
        }
      }
          
    }
      
      this.setState({alist:localalist})
      this.setState({clist:localclist})
  
      this.setState({acuteFound:indexlist.length-cFound})
      this.setState({chronicFound:indexlist.length-aFound})
  
    
  }
  if(!this.state.prefixChecked&&!this.state.firstNameChecked&&this.state.lastNameChecked&&!this.state.monthChecked&&!this.state.dateChecked&&!this.state.yearChecked){
    //lasttname
    var indexlist=[]

    for(var i=0;i<window.lastNameList.length;i++){
      if(window.lastNameList[i].includes(search)){
          indexlist.push(i)
      }
    }
    indexlist.reverse()
    var localalist=[]
    var localclist=[]
    var j=0
    var k=0
    var aFound=0
    var cFound=0

      for(var i=0;i<indexlist.length;i=i+1){
        if(indexlist.length!=0){
        if(!window.typeList[indexlist[i]].localeCompare('Acute')){
            aFound=aFound+1
            localalist.push({})
            localalist[j].title = window.patientList[indexlist[i]]
            localalist[j].icon="description"
            localalist[j].subtitle=window.createdList[indexlist[i]]
            localalist[j].rightTitle=window.keyList[indexlist[i]]
  
            j=j+1
        }
        else{
            cFound=cFound+1
            localclist.push({})
            localclist[k].title=window.patientList[indexlist[i]]
            localclist[k].icon="description"
            localclist[k].subtitle=window.createdList[indexlist[i]]
            localclist[k].rightTitle=window.keyList[indexlist[i]]
  
            k=k+1
        }
      }
          
    }
      
      this.setState({alist:localalist})
      this.setState({clist:localclist})
  
      this.setState({acuteFound:indexlist.length-cFound})
      this.setState({chronicFound:indexlist.length-aFound})
  

    
  }
  if(!this.state.prefixChecked&&!this.state.firstNameChecked&&!this.state.lastNameChecked&&!this.state.monthChecked&&this.state.dateChecked&&!this.state.yearChecked){
    //date
    var indexlist=[]
    for(var i=0;i<createdList.length;i++){
      if(window.createdList[i].split(' ')[0].includes(search)){
        indexlist.push(i)
    }  
    }
    indexlist.reverse()
    var localalist=[]
    var localclist=[]
    var j=0
    var k=0
    var aFound=0
    var cFound=0

      for(var i=0;i<indexlist.length;i=i+1){
        if(indexlist.length!=0){
        if(!window.typeList[indexlist[i]].localeCompare('Acute')){
            aFound=aFound+1
            localalist.push({})
            localalist[j].title = window.patientList[indexlist[i]]
            localalist[j].icon="description"
            localalist[j].subtitle=window.createdList[indexlist[i]]
            localalist[j].rightTitle=window.keyList[indexlist[i]]
  
            j=j+1
        }
        else{
            cFound=cFound+1
            localclist.push({})
            localclist[k].title=window.patientList[indexlist[i]]
            localclist[k].icon="description"
            localclist[k].subtitle=window.createdList[indexlist[i]]
            localclist[k].rightTitle=window.keyList[indexlist[i]]
  
            k=k+1
        }
      }
          
    }
      
      this.setState({alist:localalist})
      this.setState({clist:localclist})
  
      this.setState({acuteFound:indexlist.length-cFound})
      this.setState({chronicFound:indexlist.length-aFound})
    
  }

  if(!this.state.prefixChecked&&!this.state.firstNameChecked&&!this.state.lastNameChecked&&this.state.monthChecked&&!this.state.dateChecked&&!this.state.yearChecked){
    //month
    var indexlist=[]
    for(var i=0;i<createdList.length;i++){
      if(window.createdList[i].split(' ')[1].includes(search)){
        indexlist.push(i)
    }  
    }
    indexlist.reverse()
    var localalist=[]
    var localclist=[]
    var j=0
    var k=0
    var aFound=0
    var cFound=0

      for(var i=0;i<indexlist.length;i=i+1){
        if(indexlist.length!=0){
        if(!window.typeList[indexlist[i]].localeCompare('Acute')){
            aFound=aFound+1
            localalist.push({})
            localalist[j].title = window.patientList[indexlist[i]]
            localalist[j].icon="description"
            localalist[j].subtitle=window.createdList[indexlist[i]]
            localalist[j].rightTitle=window.keyList[indexlist[i]]
  
            j=j+1
        }
        else{
            cFound=cFound+1
            localclist.push({})
            localclist[k].title=window.patientList[indexlist[i]]
            localclist[k].icon="description"
            localclist[k].subtitle=window.createdList[indexlist[i]]
            localclist[k].rightTitle=window.keyList[indexlist[i]]
  
            k=k+1
        }
      }
          
    }
      
      this.setState({alist:localalist})
      this.setState({clist:localclist})
  
      this.setState({acuteFound:indexlist.length-cFound})
      this.setState({chronicFound:indexlist.length-aFound})
    
    
  }
  if(!this.state.prefixChecked&&!this.state.firstNameChecked&&!this.state.lastNameChecked&&!this.state.monthChecked&&!this.state.dateChecked&&this.state.yearChecked){
    //year
    var indexlist=[]
    for(var i=0;i<createdList.length;i++){
      if(window.createdList[i].split(' ')[2].includes(search)){
        indexlist.push(i)
    }  
    }
    indexlist.reverse()
    var localalist=[]
    var localclist=[]
    var j=0
    var k=0
    var aFound=0
    var cFound=0

      for(var i=0;i<indexlist.length;i=i+1){
        if(indexlist.length!=0){
        if(!window.typeList[indexlist[i]].localeCompare('Acute')){
            aFound=aFound+1
            localalist.push({})
            localalist[j].title = window.patientList[indexlist[i]]
            localalist[j].icon="description"
            localalist[j].subtitle=window.createdList[indexlist[i]]
            localalist[j].rightTitle=window.keyList[indexlist[i]]
  
            j=j+1
        }
        else{
            cFound=cFound+1
            localclist.push({})
            localclist[k].title=window.patientList[indexlist[i]]
            localclist[k].icon="description"
            localclist[k].subtitle=window.createdList[indexlist[i]]
            localclist[k].rightTitle=window.keyList[indexlist[i]]
  
            k=k+1
        }
      }
          
    }
      
      this.setState({alist:localalist})
      this.setState({clist:localclist})
  
      this.setState({acuteFound:indexlist.length-cFound})
      this.setState({chronicFound:indexlist.length-aFound})
    
  }
  

};

  
cancelSearch = search => {
    this.goToHome()
  };
clearSearch = search => {
  this.setState({alist:[]})
  this.setState({clist:[]})
  this.setState({acuteFound:0})
  this.setState({chronicFound:0})
};

goToViewContent = key =>{
  window.fromHome=false
  window.key=key
  this.props.navigation.navigate('ViewContent')
}
deleteRecord = key =>{
  window.deleteKey=key
  this.showAlert()
}

async toDeleteRecord(key){
  
  var url = 'http://'+window.ip_server+':'+window.port_server+'/deleteRecord';
  axios.post(url,{
      record_id:key
  })
  .then(function (response) {
  console.log(response);
  })
  .catch(function (error) {
  console.log(error);
  });
  //console.log(key)
  //this.refreshListSerch()

  //ลบปลอม

  var localalist=this.state.alist
  var localclist=this.state.clist

  for(let i=0;i<localalist.length;i++){
    if(key==localalist[i].rightTitle){
        localalist.splice(i,1)
        
    }

  }
  for(let i=0;i<localclist.length;i++){
    if(key==localclist[i].rightTitle){
      localclist.splice(i,1)
  }
  }

  this.setState({alist:localalist})
  this.setState({clist:localclist})

  this.setState({acuteFound:localalist.length})
  this.setState({chronicFound:localclist.length})

  //ลบจริง

  var findIndex
  // console.log("THIS IS KEY")
  // console.log(key)
  // console.log("THIS IS KEY IN WINDOW")
  // console.log(window.keyList[0])
  //console.log("THIS IS KEY")
  for(let i=0;i<window.patientList.length;i++){
    if(key===window.keyList[i]){
      findIndex=i
    }
  }
  //console.log("THIS IS KEY END")

  window.patientList.splice(findIndex,1)
  window.typeList.splice(findIndex,1)
  window.keyList.splice(findIndex,1)
  window.createdList.splice(findIndex,1)
  window.prefixList.splice(findIndex,1)
  window.firstNameList.splice(findIndex,1)
  window.lastNameList.splice(findIndex,1)

  

}
async refreshListSerch(){

  var patientList =[]
  var keyList =[]
  var createdList =[]
  var typeList =[]
  var num=0


  const config = {
    method: 'get',
    url: 'http://'+window.ip_server+':'+window.port_server+'/getForRefreshList'
  }

  let res = await axios(config)

  //console.log(res.data);

  //this.setState({db_record_list:res.data})

  for(let i=0;i<res.data.length;i++){
        
        if(res.data[i].toShow){
          patientList.push(res.data[i].patient)
          keyList.push(res.data[i].record_id)
          createdList.push(res.data[i].created)

          if(res.data[i].acute){
              typeList.push('Acute')
          }
          else{
              typeList.push('Chronic')
          }
          num=num+1
       }

  }
  
  var localList =[]
  var j=0
  for(var i=num-1;i>=0;i=i-1){
    
      localList.push({})
    localList[j].title = patientList[i]
    localList[j].icon = "description"
    localList[j].subtitle= createdList[i]
    localList[j].rightSubtitle = typeList[i]
    localList[j].rightTitle = keyList[i]

    j=j+1
    
  }
  this.setState({fullList:localList})
  
  window.patientList=patientList
  window.typeList=typeList
  window.keyList=keyList
  window.createdList=createdList

  var indexlist=[]
    for(let patientWant of window.patientList){
        if(patientWant.startsWith(this.state.search)){
            indexlist.push(window.patientList.indexOf(patientWant))
        }
    }
    for(let createdWant of window.createdList){
        if(createdWant.startsWith(this.state.search)){
          indexlist.push(window.createdList.indexOf(createdWant))
        }
    }
    
    var localalist=[]
    var localclist=[]
    var j=0
    var k=0
    var aFound=0
    var cFound=0
    for(var i=0;i<indexlist.length;i=i+1){
        
        if(!window.typeList[indexlist[i]].localeCompare('Acute')){
            aFound=aFound+1
            localalist.push({})
            localalist[j].title = window.patientList[indexlist[i]]
            localalist[j].icon="description"
            localalist[j].subtitle=window.createdList[indexlist[i]]
            localalist[j].rightTitle=window.keyList[indexlist[i]]

            j=j+1
        }
        else{
            cFound=cFound+1
            localclist.push({})
            localclist[k].title=window.patientList[indexlist[i]]
            localclist[k].icon="description"
            localclist[k].subtitle=window.createdList[indexlist[i]]
            localclist[k].rightTitle=window.keyList[indexlist[i]]

            k=k+1
        }
          
    }
    
    this.setState({alist:localalist})
    this.setState({clist:localclist})

    this.setState({acuteFound:indexlist.length-cFound})
    this.setState({chronicFound:indexlist.length-aFound})

    this.hideAlert()
    
}
uncheckAll=()=>{
  if(this.state.unCheckAll==false){
    this.setState({prefixChecked: false})
    this.setState({firstNameChecked: false })
    this.setState({lastNameChecked: false})
    this.setState({dateChecked: false})
    this.setState({monthChecked: false})
    this.setState({yearChecked: false})
    this.setState({unCheckAll: true})
  }else{
    this.setState({prefixChecked: true})
    this.setState({firstNameChecked: true})
    this.setState({lastNameChecked: true})
    this.setState({dateChecked: true})
    this.setState({monthChecked: true})
    this.setState({yearChecked: true})
    this.setState({unCheckAll: false})

  }
  
}
prefixCheck=()=>{
  this.setState({prefixChecked: !this.state.prefixChecked})
  this.setState({unCheckAll: false})

}
firstNCheck=()=>{
  this.setState({unCheckAll: false})
  this.setState({firstNameChecked: !this.state.firstNameChecked})

}
lastNCheck=()=>{
  this.setState({unCheckAll: false})
  this.setState({lastNameChecked: !this.state.lastNameChecked})

}
dateCheck=()=>{
  this.setState({unCheckAll: false})
  this.setState({dateChecked: !this.state.dateChecked})

}

monthCheck=()=>{
  this.setState({unCheckAll: false})
  this.setState({monthChecked: !this.state.monthChecked})

}
yearCheck=()=>{
  this.setState({unCheckAll: false})
  this.setState({yearChecked: !this.state.yearChecked})

}
showAlert = () => {
  this.setState({
    showAlert: true
  });
};
hideAlert = () => {
  this.setState({
    showAlert: false
  });
};
  render(){
    const { search,showAlert } = this.state;
    return(
      <SafeAreaView style={styles.container}>
        <View style={styles.main}>
          <View style={styles.up}>
                  <SearchBar
                      placeholder="Search Here..."
                      onChangeText={this.updateSearch}
                      onCancel={this.cancelSearch}
                      onClear={this.clearSearch}
                      value={search}
                      lightTheme
                      platform="ios"
                    //   showLoading
                    />
          </View>
        </View>
        <View style={styles.search_filter}>
                <View style={{padding:hp('2')}}>
                  <Text style={{fontSize:hp('2')}}>
                    ค้นหาด้วย
                  </Text>
                </View>
                <CheckBox
                    title='คำนำหน้า'
                    checked={this.state.prefixChecked}
                    onPress={() => this.prefixCheck()}
                    size={hp('2.2')}
                    textStyle={{
                      fontSize:hp('2.2')
                    }}
                    containerStyle={{
                      marginRight:wp('-1')
                    }}
                />
                <CheckBox
                    title='ชื่อ'
                    checked={this.state.firstNameChecked}
                    onPress={() => this.firstNCheck()}
                    size={hp('2.2')}
                    textStyle={{
                      fontSize:hp('2.2')
                    }}
                    containerStyle={{
                      marginRight:wp('-1')
                    }}
                />
                <CheckBox
                    title='นามสกุล'
                    checked={this.state.lastNameChecked}
                    onPress={() => this.lastNCheck()}
                    size={hp('2.2')}
                    textStyle={{
                      fontSize:hp('2.2')
                    }}
                    containerStyle={{
                      marginRight:wp('-1')
                    }}
                />
                <CheckBox
                    title='วันที่'
                    checked={this.state.dateChecked}
                    onPress={() => this.dateCheck()}
                    size={hp('2.2')}
                    textStyle={{
                      fontSize:hp('2.2')
                    }}
                    containerStyle={{
                      marginRight:wp('-1')
                    }}
                />
                <CheckBox
                    title='เดือน'
                    checked={this.state.monthChecked}
                    onPress={() => this.monthCheck()}
                    size={hp('2.2')}
                    textStyle={{
                      fontSize:hp('2.2')
                    }}
                    containerStyle={{
                      marginRight:wp('-1')
                    }}
                />
                <CheckBox
                    title='ปี'
                    checked={this.state.yearChecked}
                    onPress={() => this.yearCheck()}
                    size={hp('2.2')}
                    textStyle={{
                      fontSize:hp('2.2')
                    }}
                    containerStyle={{
                      marginRight:wp('-1')
                    }}
                />
                <CheckBox
                    title={this.state.selectOrNot}
                    checked={this.state.unCheckAll}
                    onPress={() => this.uncheckAll()}
                    size={hp('2')}
                    textStyle={{
                      fontSize:hp('2')
                    }}
                    containerStyle={{
                      marginRight:wp('-1')
                    }}
                />
                <View>
                      <Text style={{color:"grey", fontSize:hp('2'), padding:hp('2'),width:wp('20')}}>1 HEMODIALYSIS RECORD</Text>
                </View>
          </View>
          <Grid>
            <Col size={50} style={{borderTopWidth: StyleSheet.hairlineWidth,borderRightWidth: StyleSheet.hairlineWidth}}>
              <View style={{padding:hp('2'),alignSelf:'center'}}>
                  <Text style={{color:"black", fontSize:24}}>Acute ({this.state.acuteFound})</Text>
              </View>
              <ScrollView style={styles.scrollView}>
                        <View>
                      {
                        this.state.alist.map((item, i) => (
                          <ListItem
                            key={i}
                            onPress={() => this.goToViewContent(item.rightTitle)}
                            onLongPress={() => this.deleteRecord(item.rightTitle)}
                            title={item.title}
                            subtitle={item.subtitle}
                            leftIcon={{ name: item.icon }}
                            containerStyle={{
                            }}
                            titleStyle={{
                                  fontSize:hp('2.6%')
                            }}
                            subtitleStyle={{
                                  fontSize:hp('2.6%'),
                                  color:'grey'
                            }}
                            bottomDivider={true}
                            chevron
                          />
                        ))
                      }
                      </View>
                  </ScrollView>
            </Col>
            <Col size={50} style={{borderTopWidth: StyleSheet.hairlineWidth}}>
              <View style={{padding:hp('2'),alignSelf:'center'}}>
                  <Text style={{color:"black", fontSize:24}}>Chronic ({this.state.chronicFound})</Text>
              </View>
              <ScrollView style={styles.scrollView}>
              <View>
                  {
                  this.state.clist.map((item, i) => (
                      <ListItem
                          key={i}
                          onPress={() => this.goToViewContent(item.rightTitle)}
                          onLongPress={() => this.deleteRecord(item.rightTitle)}
                          title={item.title}
                          subtitle={item.subtitle}
                          leftIcon={{ name: item.icon }}
                          titleStyle={{
                                fontSize:hp('2.6%')
                          }}
                          subtitleStyle={{
                                fontSize:hp('2.6%'),
                                color:'grey'
                          }}
                          bottomDivider={true}
                          chevron
                      />
                      ))
                  }
                  </View>
                  </ScrollView>
            </Col>
          </Grid>
        <AwesomeAlert
          show={showAlert}
          showProgress={false}
          title="Are you sure to delete this record?"
          
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="Delete"
          confirmButtonColor="red"
          cancelButtonColor='#606060'
          onCancelPressed={() => {
            window.deleteKey=''
            this.hideAlert();
          }}
          onConfirmPressed={() => {
            this.toDeleteRecord(window.deleteKey)
            this.hideAlert()
          }}
        />
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    
  },
  main:{
    flexDirection:'row'
  },
  up:{
      backgroundColor:'white'
  },
  search_filter:{
    backgroundColor:'#f1f1f1',
    flexDirection:'row',
    justifyContent:'flex-start'
  },
})