import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput, Alert } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
// import { TextInput } from 'react-native-gesture-handler';
import { Button, CheckBox } from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { G } from 'react-native-svg';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
console.disableYellowBox = true;
import AwesomeAlert from 'react-native-awesome-alerts';

export default class Form2 extends React.Component {


goToForm1 = () => this.props.navigation.navigate('Form1')
goToForm3 = () => this.props.navigation.navigate('Form3')


constructor(props) {
    super(props);
    this.state = {
        aph_e:false,
        aph_lc:false,
        aph_env:false,
        aph_cd:false,
        aph_d:false,
        aph_cp:false,
        aph_p:false,
        aph_f:false,
        aph_nv:false,
        aph_h:false,
        aph_bt:false,
        aph_i:false,
        aph_ano:false,
        aph_anx:false,
        aph_sd:false,
        aph_c:false,
        aph_ps:false,
        aph_other:false,

        aph_e_n,
        aph_lc_n:false,
        aph_env_n:false,
        aph_cd_n:false,
        aph_d_n:false,
        aph_cp_n:false,
        aph_p_n:false,
        aph_f_n:false,
        aph_nv_n:false,
        aph_h_n:false,
        aph_bt_n:false,
        aph_i_n:false,
        aph_ano_n:false,
        aph_anx_n:false,
        aph_sd_n:false,
        aph_c_n:false,
        aph_ps_n:false,

        aph_other_desc:'',
        showAlert:false
        
    };
}



async componentDidMount() {
    
    
    this.setState({aph_e:window.aph_e})
    this.setState({aph_lc:window.aph_lc})
    this.setState({aph_env:window.aph_env})
    this.setState({aph_cd:window.aph_cd})
    this.setState({aph_d:window.aph_d})
    this.setState({aph_cp:window.aph_cp})
    this.setState({aph_p:window.aph_p})
    this.setState({aph_f:window.aph_f})
    this.setState({aph_nv:window.aph_nv})
    this.setState({aph_h:window.aph_h})
    this.setState({aph_bt:window.aph_bt})
    this.setState({aph_i:window.aph_i})
    this.setState({aph_ano:window.aph_ano})
    this.setState({aph_anx:window.aph_anx})
    this.setState({aph_sd:window.aph_sd})
    this.setState({aph_c:window.aph_c})
    this.setState({aph_ps:window.aph_ps})
    this.setState({aph_other:window.aph_other})

    this.setState({aph_e_n:window.aph_e_n})
    this.setState({aph_lc_n:window.aph_lc_n})
    this.setState({aph_env_n:window.aph_env_n})
    this.setState({aph_cd_n:window.aph_cd_n})
    this.setState({aph_d_n:window.aph_d_n})
    this.setState({aph_cp_n:window.aph_cp_n})
    this.setState({aph_p_n:window.aph_p_n})
    this.setState({aph_f_n:window.aph_f_n})
    this.setState({aph_nv_n:window.aph_nv_n})
    this.setState({aph_h_n:window.aph_h_n})
    this.setState({aph_bt_n:window.aph_bt_n})
    this.setState({aph_i_n:window.aph_i_n})
    this.setState({aph_ano_n:window.aph_ano_n})
    this.setState({aph_anx_n:window.aph_anx_n})
    this.setState({aph_sd_n:window.aph_sd_n})
    this.setState({aph_c_n:window.aph_c_n})
    this.setState({aph_ps_n:window.aph_ps_n})
    this.setState({aph_other:window.aph_other})

    
    this.setState({aph_other_desc:window.aph_other_desc})
}

showAlert = () => {
    this.setState({
      showAlert: true
    });
  };
  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };
  

handleBack = () =>{

    window.aph_e=this.state.aph_e
    window.aph_e_n=this.state.aph_e_n
    window.aph_lc=this.state.aph_lc
    window.aph_env=this.state.aph_env
    window.aph_cd=this.state.aph_cd
    window.aph_d=this.state.aph_d
    window.aph_cp=this.state.aph_cp
    window.aph_p=this.state.aph_p
    window.aph_f=this.state.aph_f
    window.aph_nv=this.state.aph_nv
    window.aph_h=this.state.aph_h
    window.aph_bt=this.state.aph_bt
    window.aph_i=this.state.aph_i
    window.aph_ano=this.state.aph_ano
    window.aph_anx=this.state.aph_anx
    window.aph_sd=this.state.aph_sd
    window.aph_c=this.state.aph_c
    window.aph_ps=this.state.aph_ps
    window.aph_other=this.state.aph_other

    window.aph_lc_n=this.state.aph_lc_n
    window.aph_env_n=this.state.aph_env_n
    window.aph_cd_n=this.state.aph_cd_n
    window.aph_d_n=this.state.aph_d_n
    window.aph_cp_n=this.state.aph_cp_n
    window.aph_p_n=this.state.aph_p_n
    window.aph_f_n=this.state.aph_f_n
    window.aph_nv_n=this.state.aph_nv_n
    window.aph_h_n=this.state.aph_h_n
    window.aph_bt_n=this.state.aph_bt_n
    window.aph_i_n=this.state.aph_i_n
    window.aph_ano_n=this.state.aph_ano_n
    window.aph_anx_n=this.state.aph_anx_n
    window.aph_sd_n=this.state.aph_sd_n
    window.aph_c_n=this.state.aph_c_n
    window.aph_ps_n=this.state.aph_ps_n
    
    window.aph_other_desc=this.state.aph_other_desc
    this.goToForm1()
}
handleSubmit = values => {

    if((this.state.aph_e||this.state.aph_e_n)
    &&(this.state.aph_lc||this.state.aph_lc_n)
    &&(this.state.aph_env||this.state.aph_env_n)
    &&(this.state.aph_cd||this.state.aph_cd_n)
    &&(this.state.aph_d||this.state.aph_d_n)
    &&(this.state.aph_cp||this.state.aph_cp_n)
    &&(this.state.aph_p||this.state.aph_p_n)
    &&(this.state.aph_f||this.state.aph_f_n)
    &&(this.state.aph_nv||this.state.aph_nv_n)
    &&(this.state.aph_h||this.state.aph_h_n)
    &&(this.state.aph_bt||this.state.aph_bt_n)
    &&(this.state.aph_i||this.state.aph_i_n)
    &&(this.state.aph_ano||this.state.aph_ano_n)
    &&(this.state.aph_anx||this.state.aph_anx_n)
    &&(this.state.aph_sd||this.state.aph_sd_n)
    &&(this.state.aph_c||this.state.aph_c_n)
    &&(this.state.aph_ps||this.state.aph_ps_n)
    
    ){
        window.aph_e=this.state.aph_e
        window.aph_lc=this.state.aph_lc
        window.aph_env=this.state.aph_env
        window.aph_cd=this.state.aph_cd
        window.aph_d=this.state.aph_d
        window.aph_cp=this.state.aph_cp
        window.aph_p=this.state.aph_p
        window.aph_f=this.state.aph_f
        window.aph_nv=this.state.aph_nv
        window.aph_h=this.state.aph_h
        window.aph_bt=this.state.aph_bt
        window.aph_i=this.state.aph_i
        window.aph_ano=this.state.aph_ano
        window.aph_anx=this.state.aph_anx
        window.aph_sd=this.state.aph_sd
        window.aph_c=this.state.aph_c
        window.aph_ps=this.state.aph_ps
        window.aph_other=this.state.aph_other
    
        window.aph_e_n=this.state.aph_e_n
        window.aph_lc_n=this.state.aph_lc_n
        window.aph_env_n=this.state.aph_env_n
        window.aph_cd_n=this.state.aph_cd_n
        window.aph_d_n=this.state.aph_d_n
        window.aph_cp_n=this.state.aph_cp_n
        window.aph_p_n=this.state.aph_p_n
        window.aph_f_n=this.state.aph_f_n
        window.aph_nv_n=this.state.aph_nv_n
        window.aph_h_n=this.state.aph_h_n
        window.aph_bt_n=this.state.aph_bt_n
        window.aph_i_n=this.state.aph_i_n
        window.aph_ano_n=this.state.aph_ano_n
        window.aph_anx_n=this.state.aph_anx_n
        window.aph_sd_n=this.state.aph_sd_n
        window.aph_c_n=this.state.aph_c_n
        window.aph_ps_n=this.state.aph_ps_n
    //window.aph_other_desc=values.aph_other_desc
        window.aph_other_desc=this.state.aph_other_desc
        this.goToForm3()
    
    }else{
        this.showAlert()
    }

    // window.aph_lc=this.state.aph_lc
    // window.aph_env=this.state.aph_env
    // window.aph_cd=this.state.aph_cd
    // window.aph_d=this.state.aph_d
    // window.aph_cp=this.state.aph_cp
    // window.aph_p=this.state.aph_p
    // window.aph_f=this.state.aph_f
    // window.aph_nv=this.state.aph_nv
    // window.aph_h=this.state.aph_h
    // window.aph_bt=this.state.aph_bt
    // window.aph_i=this.state.aph_i
    // window.aph_ano=this.state.aph_ano
    // window.aph_anx=this.state.aph_anx
    // window.aph_sd=this.state.aph_sd
    // window.aph_c=this.state.aph_c
    // window.aph_ps=this.state.aph_ps
    // window.aph_other=this.state.aph_other
    
    
    // window.aph_lc_n=this.state.aph_lc_n
    // window.aph_env_n=this.state.aph_env_n
    // window.aph_cd_n=this.state.aph_cd_n
    // window.aph_d_n=this.state.aph_d_n
    // window.aph_cp_n=this.state.aph_cp_n
    // window.aph_p_n=this.state.aph_p_n
    // window.aph_f_n=this.state.aph_f_n
    // window.aph_nv_n=this.state.aph_nv_n
    // window.aph_h_n=this.state.aph_h_n
    // window.aph_bt_n=this.state.aph_bt_n
    // window.aph_i_n=this.state.aph_i_n
    // window.aph_ano_n=this.state.aph_ano_n
    // window.aph_anx_n=this.state.aph_anx_n
    // window.aph_sd_n=this.state.aph_sd_n
    // window.aph_c_n=this.state.aph_c_n
    // window.aph_ps_n=this.state.aph_ps_n
    // //window.aph_other_desc=values.aph_other_desc
    // window.aph_other_desc=this.state.aph_other_desc

    
}
yesOrNo = key =>{
    var a1="1"
    var a01="01"
    var a2="2"
    var a02 ="02"
    var a3="3"
    var a03="03"
    var a4="4"
    var a04="04"
    var a5="5"
    var a05="05"
    var a6="6"
    var a06="06"
    var a7="7"
    var a07="07"
    var a8="8"
    var a08="08"
    var a9="9"
    var a09="09"
    var a10="10"
    var a010="010"
    var a11="11"
    var a011="011"
    var a12="12"
    var a012="012"
    var a13="13"
    var a013="013"
    var a14="14"
    var a014="014"
    var a15="15"
    var a015="015"
    var a16="16"
    var a016="016"
    var a17="17"
    var a017="017"
    if(!a1.localeCompare(key)){
        this.setState({aph_e: !this.state.aph_e})
        this.setState({aph_e_n: false})
    }
    if(!a2.localeCompare(key)){
        this.setState({aph_lc: !this.state.aph_lc})
        this.setState({aph_lc_n: false})
    }
    if(!a3.localeCompare(key)){
        this.setState({aph_env: !this.state.aph_env})
        this.setState({aph_env_n: false})
    }
    if(!a4.localeCompare(key)){
        this.setState({aph_cd: !this.state.aph_cd})
        this.setState({aph_cd_n: false})
    }
    if(!a5.localeCompare(key)){
        this.setState({aph_d: !this.state.aph_d})
        this.setState({aph_d_n: false})
    }
    if(!a6.localeCompare(key)){
        this.setState({aph_cp: !this.state.aph_cp})
        this.setState({aph_cp_n: false})
    }
    if(!a7.localeCompare(key)){
        this.setState({aph_p: !this.state.aph_p})
        this.setState({aph_p_n: false})
    }
    if(!a8.localeCompare(key)){
        this.setState({aph_f: !this.state.aph_f})
        this.setState({aph_f_n: false})
    }
    if(!a9.localeCompare(key)){
        this.setState({aph_nv: !this.state.aph_nv})
        this.setState({aph_nv_n: false})
    }
    if(!a10.localeCompare(key)){
        this.setState({aph_h: !this.state.aph_h})
        this.setState({aph_h_n: false})
    }
    if(!a11.localeCompare(key)){
        this.setState({aph_bt: !this.state.aph_bt})
        this.setState({aph_bt_n: false})
    }
    if(!a12.localeCompare(key)){
        this.setState({aph_i: !this.state.aph_i})
        this.setState({aph_i_n: false})
    }
    if(!a13.localeCompare(key)){
        this.setState({aph_ano: !this.state.aph_ano})
        this.setState({aph_ano_n: false})
    }
    if(!a14.localeCompare(key)){
        this.setState({aph_anx: !this.state.aph_anx})
        this.setState({aph_anx_n: false})
    }
    if(!a15.localeCompare(key)){
        this.setState({aph_sd: !this.state.aph_sd})
        this.setState({aph_sd_n: false})
    }
    if(!a16.localeCompare(key)){
        this.setState({aph_c: !this.state.aph_c})
        this.setState({aph_c_n: false})
    }
    if(!a17.localeCompare(key)){
        this.setState({aph_ps: !this.state.aph_ps})
        this.setState({aph_ps_n: false})
    }

    if(!a01.localeCompare(key)){
        this.setState({aph_e: false})
        this.setState({aph_e_n: !this.state.aph_e_n})
    }
    
    if(!a02.localeCompare(key)){
        this.setState({aph_lc: false})
        this.setState({aph_lc_n: !this.state.aph_lc_n})
    }
    if(!a03.localeCompare(key)){
        this.setState({aph_env: false})
        this.setState({aph_env_n: !this.state.aph_env_n})
    }
    if(!a04.localeCompare(key)){
        this.setState({aph_cd: false})
        this.setState({aph_cd_n: !this.state.aph_cd_n})
    }
    if(!a05.localeCompare(key)){
        this.setState({aph_d: false})
        this.setState({aph_d_n: !this.state.aph_d_n})
    }
    if(!a06.localeCompare(key)){
        this.setState({aph_cp: false})
        this.setState({aph_cp_n: !this.state.aph_cp_n})
    }
    if(!a07.localeCompare(key)){
        this.setState({aph_p: false})
        this.setState({aph_p_n: !this.state.aph_p_n})
    }
    if(!a08.localeCompare(key)){
        this.setState({aph_f: false})
        this.setState({aph_f_n: !this.state.aph_f_n})
    }
    if(!a09.localeCompare(key)){
        this.setState({aph_nv: false})
        this.setState({aph_nv_n: !this.state.aph_nv_n})
    }
    if(!a010.localeCompare(key)){
        this.setState({aph_h: false})
        this.setState({aph_h_n: !this.state.aph_h_n})
    }
    if(!a011.localeCompare(key)){
        this.setState({aph_bt: false})
        this.setState({aph_bt_n: !this.state.aph_bt_n})
    }
    if(!a012.localeCompare(key)){
        this.setState({aph_i: false})
        this.setState({aph_i_n: !this.state.aph_i_n})
    }
    if(!a013.localeCompare(key)){
        this.setState({aph_ano: false})
        this.setState({aph_ano_n: !this.state.aph_ano_n})
    }
    if(!a014.localeCompare(key)){
        this.setState({aph_anx: false})
        this.setState({aph_anx_n: !this.state.aph_anx_n})
    }
    if(!a015.localeCompare(key)){
        this.setState({aph_sd: false})
        this.setState({aph_sd_n: !this.state.aph_sd_n})
    }
    if(!a016.localeCompare(key)){
        this.setState({aph_c: false})
        this.setState({aph_c_n: !this.state.aph_c_n})
    }
    if(!a017.localeCompare(key)){
        this.setState({aph_ps: false})
        this.setState({aph_ps_n: !this.state.aph_ps_n})
    }
}

  render() {
    const {showAlert} = this.state;
  
    return (
        <Formik
            initialValues={{aph_other_desc:''}}
            onSubmit={values => {
            this.handleSubmit(values)
            }}
        >
        {({ handleChange, values, handleSubmit}) => (
        <SafeAreaView style={styles.container}>
            <KeyboardAwareScrollView>
              <View style={styles.up}>
              <View style={styles.up_line}>
              <View style={styles.up_left}>
                      <Ionicons.Button
                        name={'ios-arrow-back'}
                        size={hp('4')}
                        color='#000000'
                        backgroundColor='#fff'
                        onPress={this.handleBack}
                        title="back"
                      />
                </View>
              <View style={styles.up_right}>
                    <Text style={styles.head}>ASSESSMENT per HD</Text>
              </View>
              <View style={styles.up_right_page}>
                    <Text style={styles.head}>2/11</Text>
              </View>
            </View>
            </View>
            <Grid>
              <Col size={50} style={styles.left_content}>
                  <View style={{flexDirection:'column'}}>
                    <View style={{flexDirection:'row',padding:hp('2')}}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_e}
                            onPress={() => this.yesOrNo("1")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_e_n}
                            onPress={() => this.yesOrNo("01")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Edema</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_lc}
                            onPress={() => this.yesOrNo("2")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_lc_n}
                            onPress={() => this.yesOrNo("02")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Lung crepitation</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_env}
                            onPress={() => this.yesOrNo("3")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_env_n}
                            onPress={() => this.yesOrNo("03")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Engorge neck vein</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_cd}
                            onPress={() => this.yesOrNo("4")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_cd_n}
                            onPress={() => this.yesOrNo("04")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Chest discomfort</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_d}
                            onPress={() => this.yesOrNo("5")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_d_n}
                            onPress={() => this.yesOrNo("05")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Dyspnea</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_cp}
                            onPress={() => this.yesOrNo("6")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_cp_n}
                            onPress={() => this.yesOrNo("06")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Chest pain</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_p}
                            onPress={() => this.yesOrNo("7")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_p_n}
                            onPress={() => this.yesOrNo("07")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Pale</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_f}
                            onPress={() => this.yesOrNo("8")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_f_n}
                            onPress={() => this.yesOrNo("08")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Fever/chill</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_nv}
                            onPress={() => this.yesOrNo("9")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_nv_n}
                            onPress={() => this.yesOrNo("09")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Nausea /Vomiting</Text>
                        </View>
                    </View>
                  </View>
              </Col>
              <Col size={50} style={styles.right_content}> 
                    <View style={{flexDirection:'column'}}>
                    <View style={{flexDirection:'row',padding:hp('2')}}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_h}
                            onPress={() => this.yesOrNo("10")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_h_n}
                            onPress={() => this.yesOrNo("010")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Headache</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_bt}
                            onPress={() => this.yesOrNo("11")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_bt_n}
                            onPress={() => this.yesOrNo("011")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Bleeding tendency</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_i}
                            onPress={() => this.yesOrNo("12")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_i_n}
                            onPress={() => this.yesOrNo("012")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Itching</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_ano}
                            onPress={() => this.yesOrNo("13")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_ano_n}
                            onPress={() => this.yesOrNo("013")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Anorexia</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_anx}
                            onPress={() => this.yesOrNo("14")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_anx_n}
                            onPress={() => this.yesOrNo("014")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Anxiety</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_sd}
                            onPress={() => this.yesOrNo("15")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_sd_n}
                            onPress={() => this.yesOrNo("015")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Sleep disturbance</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_c}
                            onPress={() => this.yesOrNo("16")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_c_n}
                            onPress={() => this.yesOrNo("016")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Constipation</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Y'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_ps}
                            onPress={() => this.yesOrNo("17")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <CheckBox
                            title='N'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_ps_n}
                            onPress={() => this.yesOrNo("017")}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}
                        />
                        <View style={styles.textLable}>
                            <Text style={{fontSize:hp('3')}}>Pain score</Text>
                        </View>
                    </View>
                    <View style={styles.yesAndNo}>
                        <CheckBox
                            title='Other'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.state.aph_other}
                            onPress={() => this.setState({aph_other: !this.state.aph_other})}
                            size={hp('3')}
                            textStyle={{
                                fontSize:hp('2.6')
                            }}   
                        />
                        <View>
                        <TextInput
                            style={{borderBottomWidth:StyleSheet.hairlineWidth,padding:hp('2'),width:wp('32')}}
                            //onChangeText={handleChange('aph_other_desc')}
                            onChangeText={(aph_other_desc) => this.setState({aph_other_desc})}
                            placeholder="Please fill in Other's description here..."
                            fontSize={hp('2.6')}
                            value={this.state.aph_other_desc}
                        />
                        </View>
                        
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'flex-end',padding:wp('1')}}>
                        <Button
                                    onPress={handleSubmit}
                                    title='Next'
                                    containerStyle={{
                                    width:wp('10')
                                    }}
                                />
                    </View>
                 </View>
                </Col>
            </Grid>
            </KeyboardAwareScrollView>
            <AwesomeAlert
          show={showAlert}
          showProgress={false}
          title="Not complete"
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="red"
          cancelButtonColor="#606060"
          onCancelPressed={() => {
            this.hideAlert();
          }}
          onConfirmPressed={() => {
            
            this.hideAlert()
          }}
        />
            </SafeAreaView>


        )}
        </Formik>
    )
}
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      
    },
    up_left:{
        justifyContent:'flex-start',
        alignItems:'flex-start',
        width:wp('10%'),
        
      },
      up_right_page:{
        justifyContent:'flex-end',
        alignItems:'flex-end',
        width:wp('10%'),
        padding:hp('2')
      },
      up_line:{
        flexDirection:'row',
        
      },
      up_right:{
        width:wp('80%'),
        justifyContent:'center',
        alignItems:'center',
      },
      head:{
        fontSize:hp('2.6%'),
        fontWeight:'bold',
    
      },
      yesAndNo:{
        flexDirection:'row',
        padding:hp('2'),
        marginTop:wp('-2')
        
      },
      textLable:{
        padding:hp('2'),
      },
      left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }

})