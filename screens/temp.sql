-- CREATE DATABASE "HDRecord"
--     WITH 
--     OWNER = postgres
--     ENCODING = 'UTF8'
--     LC_COLLATE = 'C'
--     LC_CTYPE = 'C'
--     TABLESPACE = pg_default
--     CONNECTION LIMIT = -1;

-- Table: public.records

-- DROP TABLE public.records;
CREATE TABLE users
(
  id              INT unsigned NOT NULL AUTO_INCREMENT, 
  name            VARCHAR(255) ,                
  email           VARCHAR(255) UNIQUE,
  password        VARCHAR(255) UNIQUE,
  role             ENUM('admin','user'),
  PRIMARY KEY     (id)                                  
);

-- insert into
--   users (name, email, password, role)
--     values(
--         'admin',
--    '5910680361@student.tu.ac.th',
--         '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b',
--         'admin'
--           );

-- insert into
--   users (name, email, password, role)
--     values(
--         'test',
--    'test@gmail.com',
--         '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',
--         'user'
--           );
-- update users set password='6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b' where id=1;

CREATE TABLE records
(
    record_id INT unsigned NOT NULL AUTO_INCREMENT,
    AVF boolean,
    AVG boolean,
    CGD boolean,
    DLC boolean,
    LMW boolean,
    NHSO boolean,
    PERM boolean,
    SSS boolean,
    acute boolean,
    aph_ano boolean,
    aph_ano_n boolean,
    aph_anx boolean,
    aph_anx_n boolean,
    aph_bt boolean,
    aph_bt_n boolean,
    aph_c boolean,
    aph_c_n boolean,
    aph_cd boolean,
    aph_cd_n boolean,
    aph_cp boolean,
    aph_cp_n boolean,
    aph_d boolean,
    aph_d_n boolean,
    aph_e boolean,
    aph_e_n boolean,
    aph_env boolean,
    aph_env_n boolean,
    aph_f boolean,
    aph_f_n boolean,
    aph_h boolean,
    aph_h_n boolean,
    aph_i boolean,
    aph_i_n boolean,
    aph_lc boolean,
    aph_lc_n boolean,
    aph_nv boolean,
    aph_nv_n boolean,
    aph_other boolean,
    aph_p boolean,
    aph_p_n boolean,
    aph_ps boolean,
    aph_ps_n boolean,
    aph_sd boolean,
    aph_sd_n boolean,
    av_shunt_bruit boolean,
    av_shunt_thrill boolean,
    bruit boolean,
    chronic boolean,
    eph_ad boolean,
    eph_ap boolean,
    eph_arr boolean,
    eph_cc boolean,
    eph_com boolean,
    eph_cp boolean,
    eph_eb boolean,
    eph_fb boolean,
    eph_hn boolean,
    eph_hyper boolean,
    eph_hypo boolean,
    eph_mc boolean,
    eph_nocom boolean,
    eph_nop boolean,
    eph_osat boolean,
    eph_s boolean,
    AS_m2 TEXT,
    BP TEXT,
    BW TEXT,
    DW TEXT,
    ESA TEXT,
    HD_no TEXT,
    HN TEXT,
    Kuf TEXT,
    LMW_unit TEXT,
    NSS TEXT,
    PR TEXT,
    RR TEXT,
    TCV TEXT,
    TPN TEXT,
    UFG TEXT,
    aph_other_desc TEXT,
    catheter_site TEXT,
    characteristics_found TEXT,
    conductivity TEXT,
    created TEXT,
    date TEXT,
    diagnosis TEXT,
    dialysate_flow TEXT,
    dialysate_temp TEXT,
    dialysis_len TEXT,
    dialyzer TEXT,
    doctor_note LONGTEXT,
    extra_f TEXT,
    first_patient TEXT,
    glucose TEXT,
    hd_timeoff TEXT,
    g_time JSON,
    g_bpdown JSON,
    g_bpup JSON,
    g_pr JSON,
    g_rr JSON,
    hd_timeon TEXT,
    heparin_init_dose boolean,
    heparin_init_unit TEXT,
    i_bt boolean,
    i_cbd boolean,
    i_i boolean,
    i_ldt boolean,
    i_maf boolean,
    i_mwe boolean,
    i_n boolean,
    i_nd boolean,
    i_oc boolean,
    i_ot boolean,
    i_other boolean,
    i_other_desc TEXT,
    i_ps boolean,
    i_pu boolean,
    i_rf boolean,
    i_se boolean,
    i_t boolean,
    id TEXT,
    insertion_date TEXT,
    last_BW TEXT,
    last_TCV TEXT,
    last_patient TEXT,
    machine TEXT,
    maintenance_unit TEXT,
    nd_ane_p TEXT,
    nd_ane boolean,
    nd_arr_p TEXT,
    nd_arr boolean,
    nd_cf_p TEXT,
    nd_cf boolean,
    nd_dfv_p TEXT,
    nd_dfv boolean,
    nd_ei_p TEXT,
    nd_ei boolean,
    nd_fve_p TEXT,
    nd_fve boolean,
    nd_i_p TEXT,
    nd_i boolean,
    nd_m_p TEXT,
    nd_m boolean,
    nd_other_desc TEXT,
    nd_other boolean,
    nd_other_p TEXT,
    nd_rcdd_desc TEXT,
    nd_rcdd boolean,
    nd_rcdd_p TEXT,
    nd_rf_p TEXT,
    nd_rf boolean,
    nd_rh_p TEXT,
    nd_rh boolean,
    nd_ri_p TEXT,
    nd_ri boolean,
    nd_scd_p TEXT,
    nd_scd boolean,
    nd_u_p TEXT,
    nd_u boolean,
    needle_no TEXT,
    net_UF TEXT,
    next_date TEXT,
    next_time TEXT,
    no_heparin boolean,
    normal_formula boolean,
    notice TEXT,
    nss_flush TEXT,
    nurse_assign TEXT,
    nurse_note LONGTEXT,
    other_patient TEXT,
    patient TEXT,
    physician TEXT,
    post_BP TEXT,
    post_BW TEXT,
    post_RR TEXT,
    pre_time TEXT,
    prefix_patient TEXT,
    problem boolean,
    problem_desc TEXT,
    pulse TEXT,
    q TEXT,
    refer_to_home boolean,
    refer_to_ward boolean,
    refer_to_ward_desc TEXT,
    self_pay boolean,
    self_tpb TEXT,
    set_UF TEXT,
    sign_symp TEXT,
    state_enterprise boolean,
    tableHead JSON,
    tableO2sat JSON,
    tableQB JSON,
    tableTMP JSON,
    tableUF JSON,
    tableUFR JSON,
    tableVP JSON,
    tableSubmitTime JSON,
    thrill boolean,
    record_timestamp date,
    toShow boolean,
    total_UF TEXT,
    total_intake TEXT,
    use_no TEXT,
    v_HCO3 boolean,
    v_HCO3_unit TEXT,
    v_K boolean,
    v_K_unit TEXT,
    v_Na boolean,
    v_Na_unit TEXT,
    v_other boolean,
    v_other_desc TEXT,
    venofer TEXT,
    ward TEXT,
    weight_gain TEXT,
    weight_loss TEXT,
    PRIMARY KEY     (record_id)    
);

-- CONSTRAINT records_pkey PRIMARY KEY (key)

-- TABLESPACE pg_default;

-- ALTER TABLE public.records
--     OWNER to postgres;

    AVF 
    AVG 
    CGD 
    DLC 
    LMW 
    NHSO 
    PERM 
    SSS 
    acute 
    aph_ano 
    aph_ano_n 
    aph_anx 
    aph_anx_n 
    aph_bt 
    aph_bt_n 
    aph_c 
    aph_c_n 
    aph_cd 
    aph_cd_n 
    aph_cp 
    aph_cp_n 
    aph_d 
    aph_d_n 
    aph_e 
    aph_e_n 
    aph_env 
    aph_env_n 
    aph_f 
    aph_f_n 
    aph_h 
    aph_h_n 
    aph_i 
    aph_i_n 
    aph_lc 
    aph_lc_n 
    aph_nv 
    aph_nv_n 
    aph_other 
    aph_p 
    aph_p_n 
    aph_ps 
    aph_ps_n 
    aph_sd 
    aph_sd_n 
    av_shunt_bruit 
    av_shunt_thrill 
    bruit 
    chronic 
    eph_ad 
    eph_ap 
    eph_arr 
    eph_cc 
    eph_com 
    eph_cp 
    eph_eb 
    eph_fb 
    eph_hn 
    eph_hyper 
    eph_hypo 
    eph_mc 
    eph_nocom 
    eph_nop 
    eph_osat 
    eph_s 
    AS_m2 
    BP 
    BW 
    DW 
    ESA 
    HD_no 
    HN 
    Kuf 
    LMW_unit 
    NSS 
    PR 
    RR 
    TCV 
    TPN 
    UFG 
    aph_other_desc 
    catheter_site 
    characteristics_found 
    conductivity 
    created 
    date 
    diagnosis 
    dialysate_flow 
    dialysate_temp 
    dialysis_len 
    dialyzer 
    doctor_note LONG
    extra_f 
    first_patient 
    glucose 
    hd_timeoff 
    g_time 
    g_bpdown 
    g_bpup 
    g_pr 
    g_rr 
    hd_timeon 
    heparin_init_dose 
    heparin_init_unit 
    i_bt 
    i_cbd 
    i_i 
    i_ldt 
    i_maf 
    i_mwe 
    i_n 
    i_nd 
    i_oc 
    i_ot 
    i_other 
    i_other_desc 
    i_ps 
    i_pu 
    i_rf 
    i_se 
    i_t 
    id 
    insertion_date 
    last_BW 
    last_TCV 
    last_patient 
    machine 
    maintenance_unit 
    nd_ane_p 
    nd_ane 
    nd_arr_p 
    nd_arr 
    nd_cf_p 
    nd_cf 
    nd_dfv_p 
    nd_dfv 
    nd_ei_p 
    nd_ei 
    nd_fve_p 
    nd_fve 
    nd_i_p 
    nd_i 
    nd_m_p 
    nd_m 
    nd_other_desc 
    nd_other 
    nd_other_p 
    nd_rcdd_desc 
    nd_rcdd 
    nd_rcdd_p 
    nd_rf_p 
    nd_rf 
    nd_rh_p 
    nd_rh 
    nd_ri_p 
    nd_ri 
    nd_scd_p 
    nd_scd 
    nd_u_p 
    nd_u 
    needle_no 
    net_UF 
    next_date 
    next_time 
    no_heparin 
    normal_formula 
    notice 
    nss_flush 
    nurse_assign 
    nurse_note LONG
    other_patient 
    patient 
    physician 
    post_BP 
    post_BW 
    post_RR 
    pre_time 
    prefix_patient 
    problem 
    problem_desc 
    pulse 
    q 
    refer_to_home 
    refer_to_ward 
    refer_to_ward_desc 
    self_pay 
    self_tpb 
    set_uf 
    sign_symp 
    state_enterprise 
    tableHead 
    tableO2sat 
    tablEQB 
    tableTMP 
    tableUF 
    tableUFR 
    tableVP 
    tableSubmitTime 
    thrill 
    record_timestamp 
    toShow 
    total_UF 
    total_intake 
    use_no 
    v_HCO3 
    v_HCO3_unit 
    v_K 
    v_K_unit 
    v_Na 
    v_Na_unit 
    v_other 
    v_other_desc 
    venofer 
    ward 
    weight_gain 
    weight_loss 

AVF ,AVG ,CGD ,DLC ,LMW ,NHSO ,PERM ,SSS ,acute ,aph_ano ,aph_ano_n ,aph_anx ,aph_anx_n ,aph_bt ,aph_bt_n ,aph_c ,aph_c_n ,aph_cd ,aph_cd_n ,aph_cp ,aph_cp_n ,aph_d ,aph_d_n ,aph_e ,aph_e_n ,aph_env ,aph_env_n ,aph_f ,aph_f_n ,aph_h ,aph_h_n ,aph_i ,aph_i_n ,aph_lc ,aph_lc_n ,aph_nv ,aph_nv_n ,aph_other ,aph_p ,aph_p_n ,aph_ps ,aph_ps_n ,aph_sd ,aph_sd_n ,av_shunt_bruit ,av_shunt_thrill ,bruit ,chronic ,eph_ad ,eph_ap ,eph_arr ,eph_cc ,eph_com ,eph_cp ,eph_eb ,eph_fb ,eph_hn ,eph_hyper ,eph_hypo ,eph_mc ,eph_nocom ,eph_nop ,eph_osat ,eph_s ,AS_m2 ,BP ,BW ,DW ,ESA ,HD_no ,HN ,Kuf ,LMW_unit ,NSS ,PR ,RR ,TCV ,TPN ,UFG ,aph_other_desc ,catheter_site ,characteristics_found ,conductivity ,created ,date ,diagnosis ,dialysate_flow ,dialysate_temp ,dialysis_len ,dialyzer ,doctor_note LONG ,extra_f ,first_patient ,glucose ,hd_timeoff ,g_time ,g_bpdown ,g_bpup ,g_pr ,g_rr ,hd_timeon ,heparin_init_dose ,heparin_init_unit ,i_bt ,i_cbd ,i_i ,i_ldt ,i_maf ,i_mwe ,i_n ,i_nd ,i_oc ,i_ot ,i_other ,i_other_desc ,i_ps ,i_pu ,i_rf ,i_se ,i_t ,id ,insertion_date ,last_BW ,last_TCV ,last_patient ,machine ,maintenance_unit ,nd_ane_p ,nd_ane ,nd_arr_p ,nd_arr ,nd_cf_p ,nd_cf ,nd_dfv_p ,nd_dfv ,nd_ei_p ,nd_ei ,nd_fve_p ,nd_fve ,nd_i_p ,nd_i ,nd_m_p ,nd_m ,nd_other_desc ,nd_other ,nd_other_p ,nd_rcdd_desc ,nd_rcdd ,nd_rcdd_p ,nd_rf_p ,nd_rf ,nd_rh_p ,nd_rh ,nd_ri_p ,nd_ri ,nd_scd_p ,nd_scd ,nd_u_p ,nd_u ,needle_no ,net_UF ,next_date ,next_time ,no_heparin ,normal_formula ,notice ,nss_flush ,nurse_assign ,nurse_note LONG ,other_patient ,patient ,physician ,post_BP ,post_BW ,post_RR ,pre_time ,prefix_patient ,problem ,problem_desc ,pulse ,q ,refer_to_home ,refer_to_ward ,refer_to_ward_desc ,self_pay ,self_tpb ,set_uf ,sign_symp ,state_enterprise ,tableHead ,tableO2sat ,tablEQB ,tableTMP ,tableUF ,tableUFR ,tableVP ,tableSubmitTime ,thrill ,record_timestamp ,toShow ,total_UF ,total_intake ,use_no ,v_HCO3 ,v_HCO3_unit ,v_K ,v_K_unit ,v_Na ,v_Na_unit ,v_other ,v_other_desc ,venofer ,ward ,weight_gain ,weight_loss


('"+ req.body.AVF + "', '"+ data + "', '"+ data + "','" + date + "')

req.body.AVF + "', '"+ data + "','" + date

    req.body.AVF 
    + "', '"+req.body.AVG 
    + "', '"+req.body.CGD 
    + "', '"+req.body.DLC 
    + "', '"+req.body.LMW 
    + "', '"+req.body.NHSO 
    + "', '"+req.body.PERM 
    + "', '"+req.body.SSS 
    + "', '"+req.body.acute 
    + "', '"+req.body.aph_ano 
    + "', '"+req.body.aph_ano_n 
    + "', '"+req.body.aph_anx 
    + "', '"+req.body.aph_anx_n 
    + "', '"+req.body.aph_bt 
    + "', '"+req.body.aph_bt_n 
    + "', '"+req.body.aph_c 
    + "', '"+req.body.aph_c_n 
    + "', '"+req.body.aph_cd 
    + "', '"+req.body.aph_cd_n 
    + "', '"+req.body.aph_cp 
    + "', '"+req.body.aph_cp_n 
    + "', '"+req.body.aph_d 
    + "', '"+req.body.aph_d_n 
    + "', '"+req.body.aph_e 
    + "', '"+req.body.aph_e_n 
    + "', '"+req.body.aph_env 
    + "', '"+req.body.aph_env_n 
    + "', '"+req.body.aph_f 
    + "', '"+req.body.aph_f_n 
    + "', '"+req.body.aph_h 
    + "', '"+req.body.aph_h_n 
    + "', '"+req.body.aph_i 
    + "', '"+req.body.aph_i_n 
    + "', '"+req.body.aph_lc 
    + "', '"+req.body.aph_lc_n 
    + "', '"+req.body.aph_nv 
    + "', '"+req.body.aph_nv_n 
    + "', '"+req.body.aph_other 
    + "', '"+req.body.aph_p 
    + "', '"+req.body.aph_p_n 
    + "', '"+req.body.aph_ps 
    + "', '"+req.body.aph_ps_n 
    + "', '"+req.body.aph_sd 
    + "', '"+req.body.aph_sd_n 
    + "', '"+req.body.av_shunt_bruit 
    + "', '"+req.body.av_shunt_thrill 
    + "', '"+req.body.bruit 
    + "', '"+req.body.chronic 
    + "', '"+req.body.eph_ad 
    + "', '"+req.body.eph_ap 
    + "', '"+req.body.eph_arr 
    + "', '"+req.body.eph_cc 
    + "', '"+req.body.eph_com 
    + "', '"+req.body.eph_cp 
    + "', '"+req.body.eph_eb 
    + "', '"+req.body.eph_fb 
    + "', '"+req.body.eph_hn 
    + "', '"+req.body.eph_hyper 
    + "', '"+req.body.eph_hypo 
    + "', '"+req.body.eph_mc 
    + "', '"+req.body.eph_nocom 
    + "', '"+req.body.eph_nop 
    + "', '"+req.body.eph_osat 
    + "', '"+req.body.eph_s 
    + "', '"+req.body.AS_m2 
    + "', '"+req.body.BP 
    + "', '"+req.body.BW 
    + "', '"+req.body.DW 
    + "', '"+req.body.ESA 
    + "', '"+req.body.HD_no 
    + "', '"+req.body.HN 
    + "', '"+req.body.Kuf 
    + "', '"+req.body.LMW_unit 
    + "', '"+req.body.NSS 
    + "', '"+req.body.PR 
    + "', '"+req.body.RR 
    + "', '"+req.body.TCV 
    + "', '"+req.body.TPN 
    + "', '"+req.body.UFG 
    + "', '"+req.body.aph_other_desc 
    + "', '"+req.body.catheter_site 
    + "', '"+req.body.characteristics_found 
    + "', '"+req.body.conductivity 
    + "', '"+req.body.created 
    + "', '"+req.body.date 
    + "', '"+req.body.diagnosis 
    + "', '"+req.body.dialysate_flow 
    + "', '"+req.body.dialysate_temp 
    + "', '"+req.body.dialysis_len 
    + "', '"+req.body.dialyzer 
    + "', '"+req.body.doctor_note LONG
    + "', '"+req.body.extra_f 
    + "', '"+req.body.first_patient 
    + "', '"+req.body.glucose 
    + "', '"+req.body.hd_timeoff 
    + "', '"+req.body.g_time 
    + "', '"+req.body.g_bpdown 
    + "', '"+req.body.g_bpup 
    + "', '"+req.body.g_pr 
    + "', '"+req.body.g_rr 
    + "', '"+req.body.hd_timeon 
    + "', '"+req.body.heparin_init_dose 
    + "', '"+req.body.heparin_init_unit 
    + "', '"+req.body.i_bt 
    + "', '"+req.body.i_cbd 
    + "', '"+req.body.i_i 
    + "', '"+req.body.i_ldt 
    + "', '"+req.body.i_maf 
    + "', '"+req.body.i_mwe 
    + "', '"+req.body.i_n 
    + "', '"+req.body.i_nd 
    + "', '"+req.body.i_oc 
    + "', '"+req.body.i_ot 
    + "', '"+req.body.i_other 
    + "', '"+req.body.i_other_desc 
    + "', '"+req.body.i_ps 
    + "', '"+req.body.i_pu 
    + "', '"+req.body.i_rf 
    + "', '"+req.body.i_se 
    + "', '"+req.body.i_t 
    + "', '"+req.body.id 
    + "', '"+req.body.insertion_date 
    + "', '"+req.body.last_BW 
    + "', '"+req.body.last_TCV 
    + "', '"+req.body.last_patient 
    + "', '"+req.body.machine 
    + "', '"+req.body.maintenance_unit 
    + "', '"+req.body.nd_ane_p 
    + "', '"+req.body.nd_ane 
    + "', '"+req.body.nd_arr_p 
    + "', '"+req.body.nd_arr 
    + "', '"+req.body.nd_cf_p 
    + "', '"+req.body.nd_cf 
    + "', '"+req.body.nd_dfv_p 
    + "', '"+req.body.nd_dfv 
    + "', '"+req.body.nd_ei_p 
    + "', '"+req.body.nd_ei 
    + "', '"+req.body.nd_fve_p 
    + "', '"+req.body.nd_fve 
    + "', '"+req.body.nd_i_p 
    + "', '"+req.body.nd_i 
    + "', '"+req.body.nd_m_p 
    + "', '"+req.body.nd_m 
    + "', '"+req.body.nd_other_desc 
    + "', '"+req.body.nd_other 
    + "', '"+req.body.nd_other_p 
    + "', '"+req.body.nd_rcdd_desc 
    + "', '"+req.body.nd_rcdd 
    + "', '"+req.body.nd_rcdd_p 
    + "', '"+req.body.nd_rf_p 
    + "', '"+req.body.nd_rf 
    + "', '"+req.body.nd_rh_p 
    + "', '"+req.body.nd_rh 
    + "', '"+req.body.nd_ri_p 
    + "', '"+req.body.nd_ri 
    + "', '"+req.body.nd_scd_p 
    + "', '"+req.body.nd_scd 
    + "', '"+req.body.nd_u_p 
    + "', '"+req.body.nd_u 
    + "', '"+req.body.needle_no 
    + "', '"+req.body.net_UF 
    + "', '"+req.body.next_date 
    + "', '"+req.body.next_time 
    + "', '"+req.body.no_heparin 
    + "', '"+req.body.normal_formula 
    + "', '"+req.body.notice 
    + "', '"+req.body.nss_flush 
    + "', '"+req.body.nurse_assign 
    + "', '"+req.body.nurse_note LONG
    + "', '"+req.body.other_patient 
    + "', '"+req.body.patient 
    + "', '"+req.body.physician 
    + "', '"+req.body.post_BP 
    + "', '"+req.body.post_BW 
    + "', '"+req.body.post_RR 
    + "', '"+req.body.pre_time 
    + "', '"+req.body.prefix_patient 
    + "', '"+req.body.problem 
    + "', '"+req.body.problem_desc 
    + "', '"+req.body.pulse 
    + "', '"+req.body.q 
    + "', '"+req.body.refer_to_home 
    + "', '"+req.body.refer_to_ward 
    + "', '"+req.body.refer_to_ward_desc 
    + "', '"+req.body.self_pay 
    + "', '"+req.body.self_tpb 
    + "', '"+req.body.set_uf 
    + "', '"+req.body.sign_symp 
    + "', '"+req.body.state_enterprise 
    + "', '"+req.body.tableHead 
    + "', '"+req.body.tableO2sat 
    + "', '"+req.body.tablEQB 
    + "', '"+req.body.tableTMP 
    + "', '"+req.body.tableUF 
    + "', '"+req.body.tableUFR 
    + "', '"+req.body.tableVP 
    + "', '"+req.body.tableSubmitTime 
    + "', '"+req.body.thrill 
    + "', '"+req.body.record_timestamp 
    + "', '"+req.body.toShow 
    + "', '"+req.body.total_UF 
    + "', '"+req.body.total_intake 
    + "', '"+req.body.use_no 
    + "', '"+req.body.v_HCO3 
    + "', '"+req.body.v_HCO3_unit 
    + "', '"+req.body.v_K 
    + "', '"+req.body.v_K_unit 
    + "', '"+req.body.v_Na 
    + "', '"+req.body.v_Na_unit 
    + "', '"+req.body.v_other 
    + "', '"+req.body.v_other_desc 
    + "', '"+req.body.venofer 
    + "', '"+req.body.ward 
    + "', '"+req.body.weight_gain 
    + "', '"+req.body.weight_loss

    req.body.AVF + "', '"+req.body.AVG + "', '"+req.body.CGD + "', '"+req.body.DLC + "', '"+req.body.LMW + "', '"+req.body.NHSO + "', '"+req.body.PERM + "', '"+req.body.SSS + "', '"+req.body.acute + "', '"+req.body.aph_ano + "', '"+req.body.aph_ano_n + "', '"+req.body.aph_anx + "', '"+req.body.aph_anx_n + "', '"+req.body.aph_bt + "', '"+req.body.aph_bt_n + "', '"+req.body.aph_c + "', '"+req.body.aph_c_n + "', '"+req.body.aph_cd + "', '"+req.body.aph_cd_n + "', '"+req.body.aph_cp + "', '"+req.body.aph_cp_n + "', '"+req.body.aph_d + "', '"+req.body.aph_d_n + "', '"+req.body.aph_e + "', '"+req.body.aph_e_n + "', '"+req.body.aph_env + "', '"+req.body.aph_env_n + "', '"+req.body.aph_f + "', '"+req.body.aph_f_n + "', '"+req.body.aph_h + "', '"+req.body.aph_h_n + "', '"+req.body.aph_i + "', '"+req.body.aph_i_n + "', '"+req.body.aph_lc + "', '"+req.body.aph_lc_n + "', '"+req.body.aph_nv + "', '"+req.body.aph_nv_n + "', '"+req.body.aph_other + "', '"+req.body.aph_p + "', '"+req.body.aph_p_n + "', '"+req.body.aph_ps + "', '"+req.body.aph_ps_n + "', '"+req.body.aph_sd + "', '"+req.body.aph_sd_n + "', '"+req.body.av_shunt_bruit + "', '"+req.body.av_shunt_thrill + "', '"+req.body.bruit + "', '"+req.body.chronic + "', '"+req.body.eph_ad + "', '"+req.body.eph_ap + "', '"+req.body.eph_arr + "', '"+req.body.eph_cc + "', '"+req.body.eph_com + "', '"+req.body.eph_cp + "', '"+req.body.eph_eb + "', '"+req.body.eph_fb + "', '"+req.body.eph_hn + "', '"+req.body.eph_hyper + "', '"+req.body.eph_hypo + "', '"+req.body.eph_mc + "', '"+req.body.eph_nocom + "', '"+req.body.eph_nop + "', '"+req.body.eph_osat + "', '"+req.body.eph_s + "', '"+req.body.AS_m2 + "', '"+req.body.BP + "', '"+req.body.BW + "', '"+req.body.DW + "', '"+req.body.ESA + "', '"+req.body.HD_no + "', '"+req.body.HN + "', '"+req.body.Kuf + "', '"+req.body.LMW_unit + "', '"+req.body.NSS + "', '"+req.body.PR + "', '"+req.body.RR + "', '"+req.body.TCV + "', '"+req.body.TPN + "', '"+req.body.UFG + "', '"+req.body.aph_other_desc + "', '"+req.body.catheter_site + "', '"+req.body.characteristics_found + "', '"+req.body.conductivity + "', '"+req.body.created + "', '"+req.body.date + "', '"+req.body.diagnosis + "', '"+req.body.dialysate_flow + "', '"+req.body.dialysate_temp + "', '"+req.body.dialysis_len + "', '"+req.body.dialyzer + "', '"+req.body.doctor_note LONG + "', '"+req.body.extra_f + "', '"+req.body.first_patient + "', '"+req.body.glucose + "', '"+req.body.hd_timeoff + "', '"+req.body.g_time + "', '"+req.body.g_bpdown + "', '"+req.body.g_bpup + "', '"+req.body.g_pr + "', '"+req.body.g_rr + "', '"+req.body.hd_timeon + "', '"+req.body.heparin_init_dose + "', '"+req.body.heparin_init_unit + "', '"+req.body.i_bt + "', '"+req.body.i_cbd + "', '"+req.body.i_i + "', '"+req.body.i_ldt + "', '"+req.body.i_maf + "', '"+req.body.i_mwe + "', '"+req.body.i_n + "', '"+req.body.i_nd + "', '"+req.body.i_oc + "', '"+req.body.i_ot + "', '"+req.body.i_other + "', '"+req.body.i_other_desc + "', '"+req.body.i_ps + "', '"+req.body.i_pu + "', '"+req.body.i_rf + "', '"+req.body.i_se + "', '"+req.body.i_t + "', '"+req.body.id + "', '"+req.body.insertion_date + "', '"+req.body.last_BW + "', '"+req.body.last_TCV + "', '"+req.body.last_patient + "', '"+req.body.machine + "', '"+req.body.maintenance_unit + "', '"+req.body.nd_ane_p + "', '"+req.body.nd_ane + "', '"+req.body.nd_arr_p + "', '"+req.body.nd_arr + "', '"+req.body.nd_cf_p + "', '"+req.body.nd_cf + "', '"+req.body.nd_dfv_p + "', '"+req.body.nd_dfv + "', '"+req.body.nd_ei_p + "', '"+req.body.nd_ei + "', '"+req.body.nd_fve_p + "', '"+req.body.nd_fve + "', '"+req.body.nd_i_p + "', '"+req.body.nd_i + "', '"+req.body.nd_m_p + "', '"+req.body.nd_m + "', '"+req.body.nd_other_desc + "', '"+req.body.nd_other + "', '"+req.body.nd_other_p + "', '"+req.body.nd_rcdd_desc + "', '"+req.body.nd_rcdd + "', '"+req.body.nd_rcdd_p + "', '"+req.body.nd_rf_p + "', '"+req.body.nd_rf + "', '"+req.body.nd_rh_p + "', '"+req.body.nd_rh + "', '"+req.body.nd_ri_p + "', '"+req.body.nd_ri + "', '"+req.body.nd_scd_p + "', '"+req.body.nd_scd + "', '"+req.body.nd_u_p + "', '"+req.body.nd_u + "', '"+req.body.needle_no + "', '"+req.body.net_UF + "', '"+req.body.next_date + "', '"+req.body.next_time + "', '"+req.body.no_heparin + "', '"+req.body.normal_formula + "', '"+req.body.notice + "', '"+req.body.nss_flush + "', '"+req.body.nurse_assign + "', '"+req.body.nurse_note LONG + "', '"+req.body.other_patient + "', '"+req.body.patient + "', '"+req.body.physician + "', '"+req.body.post_BP + "', '"+req.body.post_BW + "', '"+req.body.post_RR + "', '"+req.body.pre_time + "', '"+req.body.prefix_patient + "', '"+req.body.problem + "', '"+req.body.problem_desc + "', '"+req.body.pulse + "', '"+req.body.q + "', '"+req.body.refer_to_home + "', '"+req.body.refer_to_ward + "', '"+req.body.refer_to_ward_desc + "', '"+req.body.self_pay + "', '"+req.body.self_tpb + "', '"+req.body.set_uf + "', '"+req.body.sign_symp + "', '"+req.body.state_enterprise + "', '"+req.body.tableHead + "', '"+req.body.tableO2sat + "', '"+req.body.tablEQB + "', '"+req.body.tableTMP + "', '"+req.body.tableUF + "', '"+req.body.tableUFR + "', '"+req.body.tableVP + "', '"+req.body.tableSubmitTime + "', '"+req.body.thrill + "', '"+req.body.record_timestamp + "', '"+req.body.toShow + "', '"+req.body.total_UF + "', '"+req.body.total_intake + "', '"+req.body.use_no + "', '"+req.body.v_HCO3 + "', '"+req.body.v_HCO3_unit + "', '"+req.body.v_K + "', '"+req.body.v_K_unit + "', '"+req.body.v_Na + "', '"+req.body.v_Na_unit + "', '"+req.body.v_other + "', '"+req.body.v_other_desc + "', '"+req.body.venofer + "', '"+req.body.ward + "', '"+req.body.weight_gain + "', '"+req.body.weight_loss