import React from 'react'
import { StyleSheet, Text, View, Button, Dimensions, Image, ScrollView, SafeAreaView, Alert } from 'react-native'
import {Header} from 'react-native-elements'
import { TextInput } from 'react-native-gesture-handler'
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import { CheckBox, ListItem, SearchBar } from 'react-native-elements';

import AwesomeAlert from 'react-native-awesome-alerts';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
console.disableYellowBox = true;
import axios from 'axios';
export default class RecentlyDeleted extends React.Component {
  goToHome = () => this.props.navigation.navigate('App')
  constructor(props) {
    super(props);
    this.state = {
        list:[],
        showAlert:false,//delte alert
        recoverAlert:false,
        showdallAlert:false,
        recoverAllAlert:false,
    };
  }
  
  async componentDidMount() {


    this.refreshListFalseToShow()
    
  }

  async refreshListFalseToShow(){

    var patientList =[]
    var keyList =[]
    var createdList =[]
    var typeList =[]
    var num=0

    const config = {
      method: 'get',
      url: 'http://'+window.ip_server+':'+window.port_server+'/getForRefreshList'
    }

    let res = await axios(config)

    for(let i=0;i<res.data.length;i++){
          
          if(!res.data[i].toShow){
            patientList.push(res.data[i].patient)
            keyList.push(res.data[i].record_id)
            createdList.push(res.data[i].created)

            if(res.data[i].acute){
                typeList.push('Acute')
            }
            else{
                typeList.push('Chronic')
            }
            num=num+1
         }

    }
    
    var localList =[]
    var j=0
    for(var i=num-1;i>=0;i=i-1){
      
        localList.push({})
      localList[j].title = patientList[i]
      localList[j].icon = "description"
      localList[j].subtitle= createdList[i]
      localList[j].rightSubtitle = typeList[i]
      localList[j].rightTitle = keyList[i]

      j=j+1
      
    }
    this.setState({list:localList})
    this.hideDAlert()
}
    
    perDeleteRecord = key =>{

      window.deleteKey=key
      this.showDAlert()
        
  }
  showDAlert = () => {
    this.setState({
      showAlert: true
    });
  };
  hideDAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  showRallAlert = () => {
    this.setState({
      recoverAllAlert: true
    });
  };
  hideRallAlert = () => {
    this.setState({
      recoverAllAlert: false
    });
  };
  recoverAll =()=>{
    if(this.state.list.length!=0){
        this.showRallAlert()
    }
      
  }

  async toRecoverAllRecord(){
    
    var url = 'http://'+window.ip_server+':'+window.port_server+'/recoverAllRecord';
      axios.post(url,{
      })
      .then(function (response) {
      console.log(response);
      })
      .catch(function (error) {
      console.log(error);
      });
    // this.refreshListFalseToShow()
    this.setState({list:[]})
  }

  deleteAll =()=>{
    if(this.state.list.length!=0){

    this.showDallAlert()
    }
    
      
  }
  showDallAlert = () => {
    this.setState({
      showdallAlert: true
    });
  };
  hideDallAlert = () => {
    this.setState({
      showdallAlert: false
    });
  };
  async toDeleteAllRecord(){
    
    var url = 'http://'+window.ip_server+':'+window.port_server+'/perDeleteAllRecord';
      axios.post(url,{
      })
      .then(function (response) {
      console.log(response);
      })
      .catch(function (error) {
      console.log(error);
      });
    //this.refreshListFalseToShow()
    this.setState({list:[]})

  }
  async toDeleteRecord(key){
    
    var url = 'http://'+window.ip_server+':'+window.port_server+'/perDeleteRecord';
      axios.post(url,{
          record_id:key
      })
      .then(function (response) {
      console.log(response);
      })
      .catch(function (error) {
      console.log(error);
      });
    //this.refreshListFalseToShow()

    var locallist=this.state.list
    for(let i=0;i<locallist.length;i++){
      if(key==locallist[i].rightTitle){
          locallist.splice(i,1)
      }

    }
    this.setState({list:locallist})
  }
  
showRAlert = () => {
  this.setState({
    recoverAlert: true
  });
};
hideRAlert = () => {
  this.setState({
    recoverAlert: false
  });
};
    recoverRecord= key =>{
      window.deleteKey=key
      this.showRAlert()
        
  }
  async toRecoverRecord(key){
    
    var url = 'http://'+window.ip_server+':'+window.port_server+'/recoverRecord';
      axios.post(url,{
          record_id:key
      })
      .then(function (response) {
      console.log(response);
      })
      .catch(function (error) {
      console.log(error);
      });
    //this.refreshListFalseToShow()

    var locallist=this.state.list
    for(let i=0;i<locallist.length;i++){
      if(key==locallist[i].rightTitle){
          locallist.splice(i,1)
      }

    }
    this.setState({list:locallist})

  }
  render(){
    
    return (
      <SafeAreaView style={styles.container}>
          <View style={styles.main}>
            <View style={styles.up}>
              <View style={styles.up_line}>
              <View style={styles.up_left}>
                      <Ionicons.Button
                        name={'ios-arrow-back'}
                        size={hp('3')}
                        color='#000000'
                        backgroundColor='#fff'
                        onPress={this.goToHome}
                        title="back"
                      />
                </View>
              <View style={styles.up_right}>
                    <Text style={styles.head}>Recently Deleted</Text>
              </View>
              <View style={styles.up_left}>

              </View>
            </View>
            </View>
          </View>
          <View style={styles.deleteAndRecoverAll}>
              <View>
                <Button title='Delete All' onPress={this.deleteAll} color='red'/>
              </View>
              <View>
                <Button title='Recover All' onPress={this.recoverAll}/>
              </View>
          </View>
          <View style={styles.down}>
            <ScrollView style={styles.scrollView}>
                <View>
                      {
                        this.state.list.map((item, i) => (
                          <ListItem
                            key={i}
                            onPress={() => this.perDeleteRecord(item.rightTitle)}
                            onLongPress={() => this.recoverRecord(item.rightTitle)}
                            title={item.title}
                            subtitle={item.subtitle}
                            rightSubtitle={item.rightSubtitle}
                            leftIcon={{ name: item.icon }}
                            containerStyle={{
                                  width:wp('100%')
                            }}
                            titleStyle={{
                              fontSize:hp('2.6%'),
                            }}
                            subtitleStyle={{
                              fontSize:hp('2.6%'),
                                  color:'grey'
                            }}
                            rightSubtitleStyle={{
                              fontSize:hp('3%'),
                              color:'grey'
                            }}
                            // topDivider={true}
                            bottomDivider={true}
                            chevron
                          />
                        ))
                      }
                    </View>
            </ScrollView>
          </View>
      <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="Are you sure to permanently delete this record?"
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="Delete"
          confirmButtonColor="red"
          cancelButtonColor="#606060"
          onCancelPressed={() => {
            window.deleteKey=''
            this.hideDAlert();
          }}
          onConfirmPressed={() => {
            this.toDeleteRecord(window.deleteKey)
            this.hideDAlert()
          }}
        />
        <AwesomeAlert
          show={this.state.showdallAlert}
          showProgress={false}
          title="Are you sure to permanently delete all record?"
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="Delete"
          confirmButtonColor="red"
          cancelButtonColor="#606060"
          onCancelPressed={() => {
            window.deleteKey=''
            this.hideDallAlert();
          }}
          onConfirmPressed={() => {
            this.toDeleteAllRecord()
            this.hideDallAlert()
          }}
        />
        <AwesomeAlert
          show={this.state.recoverAlert}
          showProgress={false}
          title="Are you sure to recover this record?"
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="Recover"
          confirmButtonColor="blue"
          cancelButtonColor="#606060"
          onCancelPressed={() => {
            window.deleteKey=''
            this.hideRAlert()
            
          }}
          onConfirmPressed={() => {
            this.toRecoverRecord(window.deleteKey)
            this.hideRAlert()
            
          }}
        />
        <AwesomeAlert
          show={this.state.recoverAllAlert}
          showProgress={false}
          title="Are you sure to recover all record?"
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="Cancel"
          confirmText="Recover"
          confirmButtonColor="blue"
          cancelButtonColor="#606060"
          onCancelPressed={() => {
            window.deleteKey=''
            this.hideRallAlert();
          }}
          onConfirmPressed={() => {
            this.toRecoverAllRecord()
            this.hideRallAlert()
          }}
        />
        
      </SafeAreaView>
    )
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    
  },
  deleteAndRecoverAll:{
      flexDirection:'row',
      backgroundColor:'#f9f9f9',
      justifyContent:'flex-end'

  },
  head:{
    fontSize:hp('2.6%'),
    fontWeight:'bold',

  },
  main:{
    flexDirection:'row'

  },
  up:{
    flexDirection:'row',
    backgroundColor:'#fff',
    
  },
  up_left:{
    justifyContent:'flex-start',
    alignItems:'flex-start',
    width:wp('10%'),
  },
  up_line:{
    flexDirection:'row',
    
  },
  up_right:{
    width:wp('80%'),
    justifyContent:'center',
    alignItems:'center',
  },
  down:{
    flexDirection:'column',
    backgroundColor:'#f9f9f9',
  },
})