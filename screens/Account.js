import React from 'react'
import { StyleSheet, Text, View, TextInput, SafeAreaView, Alert, Image } from 'react-native'
import { Grid, Col } from 'react-native-easy-grid'
import { Button, CheckBox } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
console.disableYellowBox = true;
import axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Account extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      
      user_data:[],
      notAdmin:true,
      
    };
  }
  async componentDidMount() {
    
    let res = await axios.post('http://'+window.ip_server+':'+window.port_server+'/getUserDetail', {
          
          id:window.session_id
        
      })
  
      let data = res.data
      console.log(data)
      if(data[0].role=='admin'){
        this.setState({notAdmin:false})
      }
      this.setState({user_data:data[0]})

}
goToManageUser = () => {
    
  //window.session_id=0
  this.props.navigation.navigate('ManageUser')
}
  
  goToLogin = () => {
    
    window.session_id=0
    this.props.navigation.navigate('Login')
  }
  render(){
    return(
      <SafeAreaView style={styles.container}>
          <View style={styles.main}>
            <View> 
              <Image style={styles.bgLogin} source={require('../assets/bgLogin.png')}/>
            </View>
            <View style={styles.right}>
              <View>
                <Image style={styles.logoLogin} source={require('../assets/logoLogin.png')}/>
              </View>
              <View>
                <Text style={{fontSize:hp('4')}}>Account</Text>
              </View>
              <View>
                <Text style={{fontSize:hp('2.6'),width:hp(40)}}>name: {this.state.user_data.name}</Text>
              </View>
              <View>
                <Text style={{fontSize:hp('2.6'),width:hp(40)}}>email: {this.state.user_data.email}</Text>
              </View>
              <View>
                <Button
                  title="Administrator Page"
                  disabled={this.state.notAdmin}
                  onPress={this.goToManageUser}
                />
              </View>
              <View>
                  <Ionicons.Button
                          name={'ios-log-out'}
                          size={hp('10')}
                          color='red'
                          backgroundColor='#fff'
                          onPress={this.goToLogin}
                          title="login"
                        />
              </View>
            </View>
          </View>
        </SafeAreaView>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    
  },
  main:{
    flexDirection:'row'
    
  },
  right:{
    flexDirection:'column',
    justifyContent:'space-around',
    alignItems:'center'
  },
  bgLogin:{
    height: hp('92%'), 
    width: wp('64%'),
    alignSelf:'flex-start',
    resizeMode:'contain'
  },
  logoLogin:{
    height: hp('36%'), 
    width: wp('36%'),
    alignSelf:'flex-end',
    resizeMode:'contain'
  }

})