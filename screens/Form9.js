import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput,Dimensions } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
import { Button, CheckBox } from 'react-native-elements';
import {
    LineChart,
  } from "react-native-chart-kit";
import { Table, TableWrapper,Rows, Cols, Cell } from 'react-native-table-component';
import {Row as Rowt} from 'react-native-table-component';
import {Col as Colt} from 'react-native-table-component';
import NumericInput from 'react-native-numeric-input'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Html5Entities } from 'html-entities'; 
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
console.disableYellowBox = true;
const chartConfig = {
    backgroundGradientFrom: "#ffffff",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#08130D",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
    barPercentage: 0.5,
    strokeWidth: 2, // optional, default 3
    decimalPlaces: 0,
  };
  
  
export default class Form9 extends React.Component {

goToForm7 = () => this.props.navigation.navigate('Form7')
goToForm10 = () => this.props.navigation.navigate('Form10')

constructor(props) {
    super(props);
    this.state = {
        g_hour:'',
        g_min:'',
        
        g_o2sat:'',
        g_qb:'',
        g_vp:'',
        g_tmp:'',
        g_ufr:'',
        g_uf:'',

        tableHead: ['time','','','','','','','','','','',''],
        tableTitle: ['O2 Sat', 'QB', 'VP', 'TMP', 'UFR', 'UF','Column'],
        tableData: [
            //index 0 to 11 
            ['','','','','','','','','','','',''],//o2sat
            ['','','','','','','','','','','',''],//qb
            ['','','','','','','','','','','',''],//vp
            ['','','','','','','','','','','',''],//tmp
            ['','','','','','','','','','','',''],//ufr
            ['','','','','','','','','','','',''],//uf
            ['1','2','3','4','5','6','7','8','9','10','11','12'],//uf
      ],
      tableSubmitTime:[],
      TtextMarginLeft:0,
       TmarginLeft:0,
        Twidth:0,
        i:1,

        g_bp_up:'',
        g_bp_down:'',
        g_pr:'',
        g_rr:'',
    };
}
data = {
    labels: ['time'],
    datasets: [
      {
        data: [0],//bp_up
        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
        strokeWidth: 2
      },
      {
        data:[0],//bp_down
        color: (opacity = 1) => `rgba(0, 130, 0, ${opacity})`,
        strokeWidth: 2
        },
        {
        data:[0],//pr
        color: (opacity = 1) => `rgba(255, 0, 0, ${opacity})`,
        strokeWidth: 2
      },
      {
        data:[0],//rr
        color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`,
        strokeWidth: 2
        },
      
    ],
  };

  async componentDidMount(){
    
      this.setState({tableHead:window.tableHead})

      this.setState({TtextMarginLeft:window.TtextMarginLeft})
      this.setState({TmarginLeft:window.TmarginLeft})
      this.setState({Twidth:window.Twidth})

      var local_tableData = [
        //index 0 to 11 
        ['','','','','','','','','','','',''],//o2sat
        ['','','','','','','','','','','',''],//qb
        ['','','','','','','','','','','',''],//vp
        ['','','','','','','','','','','',''],//tmp
        ['','','','','','','','','','','',''],//ufr
        ['','','','','','','','','','','',''],//uf
        ['1','2','3','4','5','6','7','8','9','10','11','12'],//uf
    ]
    local_tableData[0]=window.tableO2sat
    local_tableData[1]=window.tableQB
    local_tableData[2]=window.tableVP
    local_tableData[3]=window.tableTMP
    local_tableData[4]=window.tableUFR
    local_tableData[5]=window.tableUF

    this.setState({tableData:local_tableData})
    
    
    if(window.g_time[1]!=''){
        this.data.labels=window.g_time
    }
    // this.data.labels=window.g_time

    //console.log(window.tableHead)
    var local_data = {
        labels: ['time'],
        datasets: [
          {
            data: [0],//bp_up
            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            strokeWidth: 2
          },
          {
            data:[0],//bp_down
            color: (opacity = 1) => `rgba(0, 130, 0, ${opacity})`,
            strokeWidth: 2
            },
            {
            data:[0],//pr
            color: (opacity = 1) => `rgba(255, 0, 0, ${opacity})`,
            strokeWidth: 2
          },
          {
            data:[0],//rr
            color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`,
            strokeWidth: 2
            },
          
        ],
      };
      local_data.datasets[0].data=window.g_bpup
      local_data.datasets[1].data=window.g_bpdown
      local_data.datasets[2].data=window.g_pr
      local_data.datasets[3].data=window.g_rr
      this.data.datasets=local_data.datasets
      this.setState({tableSubmitTime:window.tableSubmitTime})


  }



handleBack = () => {

    window.tableHead= this.state.tableHead
    window.tableO2sat= this.state.tableData[0]
    window.tableQB= this.state.tableData[1]
    window.tableVP= this.state.tableData[2]
    window.tableTMP= this.state.tableData[3]
    window.tableUFR= this.state.tableData[4]
    window.tableUF= this.state.tableData[5]
    window.g_time= this.data.labels
    window.g_bpup= this.data.datasets[0].data
    window.g_bpdown= this.data.datasets[1].data
    window.g_pr= this.data.datasets[2].data
    window.g_rr= this.data.datasets[3].data
    window.tableSubmitTime= this.state.tableSubmitTime

    window.TtextMarginLeft=this.state.TtextMarginLeft
    window.TmarginLeft=this.state.TmarginLeft
    window.Twidth=this.state.Twidth
    

    this.goToForm7()
    
}

handleSubmit = values => {
    if(this.state.g_hour!=''&&this.state.g_min!=''&&this.state.g_bp_up!=''&&this.state.g_bp_down!=''&&this.state.g_pr!=''&&this.state.g_rr!='')
    
    {
        var newmin=this.state.g_min
        if(parseInt(newmin)<10){
            newmin='0'+newmin
        }
        var t = this.state.g_hour+':'+newmin
        var bpup = parseInt(this.state.g_bp_up)
        var bpdown = parseInt(this.state.g_bp_down)
        var pr = parseInt(this.state.g_pr)
        var rr = parseInt(this.state.g_rr)

        this.data.labels.push(t)
        this.data.datasets[0].data.push(bpup)
        this.data.datasets[1].data.push(bpdown)
        this.data.datasets[2].data.push(pr)
        this.data.datasets[3].data.push(rr)
        var time = new Date();
        this.setState({g_hour:time.getHours(),
                        g_min:time.getMinutes()
                    
    })
    }
}

handleSubmitToNextPage =() =>{
    
     
    
    window.tableHead= this.state.tableHead
    window.tableO2sat= this.state.tableData[0]
    window.tableQB= this.state.tableData[1]
    window.tableVP= this.state.tableData[2]
    window.tableTMP= this.state.tableData[3]
    window.tableUFR= this.state.tableData[4]
    window.tableUF= this.state.tableData[5]
    window.g_time= this.data.labels
    window.g_bpup= this.data.datasets[0].data
    window.g_bpdown= this.data.datasets[1].data
    window.g_pr= this.data.datasets[2].data
    window.g_rr= this.data.datasets[3].data
    window.tableSubmitTime= this.state.tableSubmitTime

    window.TtextMarginLeft=this.state.TtextMarginLeft
    window.TmarginLeft=this.state.TmarginLeft
    window.Twidth=this.state.Twidth
    

    this.goToForm10()
}

handleSubmitTime = () => {
    
    var time = new Date();
    this.setState({g_hour:time.getHours(),
                    g_min:time.getMinutes()
                    
    })
    
    
}

//i=0
j=0
handleSubmitTable = () =>{
    if(this.state.g_hour!=''&&this.state.g_min!=''&&this.state.g_o2sat!=''&&this.state.g_qb!=''&&this.state.g_vp!=''&&this.state.g_tmp!=''&&this.state.g_ufr!=''&&this.state.g_uf!=''){
        
        //this.state.g_min='3'
        
        
        this.state.tableData[0].splice(this.state.i-1,1,this.state.g_o2sat)
        this.state.tableData[1].splice(this.state.i-1,1,this.state.g_qb)
        this.state.tableData[2].splice(this.state.i-1,1,this.state.g_vp)
        this.state.tableData[3].splice(this.state.i-1,1,this.state.g_tmp)
        this.state.tableData[4].splice(this.state.i-1,1,this.state.g_ufr)
        this.state.tableData[5].splice(this.state.i-1,1,this.state.g_uf)

        var newmin=this.state.g_min
        if(parseInt(newmin)<10){
            newmin='0'+newmin
        }
        var t = this.state.g_hour+':'+newmin

        this.state.tableSubmitTime.push(t)
        
        if(this.j==0){
            if(parseInt(this.state.g_min)<30){
                this.setState({TtextMarginLeft:wp('1')})
                this.setState({TmarginLeft:0})
                this.setState({Twidth:wp('56')})

                var h1=parseInt(this.state.g_hour)
                var h2=h1+1
                var h3=h2+1
                var h4=h3+1
                var h5=h4+1
                var h6=h5+1
                this.state.tableHead.splice(1,1,this.state.g_hour)
                this.state.tableHead.splice(3,1,h2.toString())
                this.state.tableHead.splice(5,1,h3.toString())
                this.state.tableHead.splice(7,1,h4.toString())
                this.state.tableHead.splice(9,1,h5.toString())
                this.state.tableHead.splice(11,1,h6.toString())
            }
            else{
                
                this.setState({TtextMarginLeft:wp('1')})
                this.setState({TmarginLeft:0})
                this.setState({Twidth:wp('60')})

                var h1=parseInt(this.state.g_hour)
                var h2=h1+1
                var h3=h2+1
                var h4=h3+1
                var h5=h4+1
                var h6=h5+1
                var h7=h6+1
                this.state.tableHead.splice(2,1,h2.toString())
                this.state.tableHead.splice(4,1,h3.toString())
                this.state.tableHead.splice(6,1,h4.toString())
                this.state.tableHead.splice(8,1,h5.toString())
                this.state.tableHead.splice(10,1,h6.toString())
                this.state.tableHead.splice(12,1,h7.toString())
            }



        }
        
        
        
        
        var time = new Date();
        this.setState({g_hour:time.getHours(),
                        g_min:time.getMinutes()})
        // var locali = this.state.i+1
        // this.setState({i:locali})
        
        
        this.j=this.j+1
        
    }
}


renderTime = () =>{
    if(this.state.g_hour==''&&this.state.g_min==''){
        return <View><Text style={{fontSize:hp('2.6')}}>เวลาจะแสดงที่นี่</Text></View>
    }
    else{
        if(parseInt(this.state.g_min)<10){
            var newmin = '0'+this.state.g_min
        return <View><Text style={{fontSize:hp('2.6')}}>{this.state.g_hour}:{newmin}</Text></View>
        }
        else{
            return <View ><Text style={{fontSize:hp('2.6')}}>{this.state.g_hour}:{this.state.g_min}</Text></View>
        }
    }
}

arabic = "O&#8322;"; 
entities = new Html5Entities();
o2 =()=>{
    return <Text style={{fontSize:hp('3')}}> {this.entities.decode(this.arabic)} Sat(%) </Text>

}

  render() {
    return (
        <Formik
            initialValues={{
                            g_bp_up:'',
                            g_bp_down:'',
                            g_pr:'',
                            g_rr:'',

            }}
            onSubmit={values => {
            this.handleSubmit(values)
            }}
        >
        {({handleSubmit,handleChange,values}) => (
            <SafeAreaView style={styles.container}>
                <KeyboardAwareScrollView>
            <View style={styles.up}>
            <View style={styles.up_line}>
            <View style={styles.up_left}>
                    <Ionicons.Button
                      name={'ios-arrow-back'}
                      size={hp('4')}
                      color='#000000'
                      backgroundColor='#fff'
                      onPress={this.handleBack}
                      title="back"
                    />
              </View>
            <View style={styles.up_right}>
                  <Text style={styles.head}></Text>
            </View>
            <View style={styles.up_right_page}>
                  <Text style={styles.head}>8/11</Text>
            </View>
          </View>
          </View>
          <Grid>
            <Col size={70} style={styles.left_content}>
                <View style={{flexDirection:'column'}}>
                        <View>
                            <LineChart
                            data={this.data}
                            width={wp('62')}
                            height={hp('50')}
                            chartConfig={chartConfig}
                            withShadow={false}
                            bezier
                            verticalLabelRotation={60}
                            
                            />
                        </View>
                        <Table>
                            <Rowt data={this.state.tableHead} flexArr={[]} style={{height: hp('5'),  backgroundColor: '#f1f8ff',marginLeft:this.state.TmarginLeft,width:this.state.Twidth }} textStyle={{marginLeft:this.state.TtextMarginLeft,justifyContent:'space-between'}} />
                            
                        </Table>
                        <Table borderStyle={{borderWidth: 1, borderColor: '#a9a9a9',}}>
                            <TableWrapper style={styles.wrapper}>
                                <Colt data={this.state.tableTitle} style={styles.title} heightArr={[hp('5')]} textStyle={styles.text}/>
                                <Rows data={this.state.tableData} flexArr={[3,3,3,3,3,3,3,3,3,3,3,3,3]} style={styles.row} textStyle={styles.text}/>
                            </TableWrapper>
                        </Table>
                </View>

            </Col>
            <Col size={22} style={styles.mid_content}>
                <View style={{flexDirection:'column'}}>
                    <View>
                        <Button
                            title="กดเพื่อบันทึกตามเวลาจริง"
                            onPress={this.handleSubmitTime}
                        />
                    </View>
                    <View style={{alignSelf:'center'}}>
                        <Text style={{fontSize:wp('2.6'),margin:wp('2')}}>Time</Text>
                    </View>
                    <View style={{justifyContent:'center',alignItems:'center'}}>
                            {this.renderTime()}
                    </View>
                    <View style={{alignSelf:'center',marginTop:wp('3'),margin:wp('2')}}>
                        <Text style={{fontSize:hp('2.6'),color:'green'}}>BP (mmHg)</Text>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'center'}}>
                            <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('5'),height:hp('4.4'),borderColor:'black',borderColor:'grey'}}
                                onChangeText={(g_bp_up) => this.setState({g_bp_up})}
                                fontSize={hp('2.6')}
                                value={this.state.g_bp_up}
                                ref={(input) => { this.t1 = input; }}
                                onSubmitEditing={() => { this.t2.focus(); }}
                                blurOnSubmit={false}
                                />
                            <View style={{marginLeft:wp('1'),marginRight:wp('1')}}>
                                    <Text style={{fontSize:hp('3')}}>/</Text>
                            </View>
                            <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('5'),height:hp('4.4'),borderColor:'green'}}
                                onChangeText={(g_bp_down) => this.setState({g_bp_down})}
                                fontSize={hp('2.6')}
                                value={this.state.g_bp_down}
                                ref={(input) => { this.t2 = input; }}
                                onSubmitEditing={() => { this.t3.focus(); }}
                                blurOnSubmit={false}
                                />

                    </View>
                    <View style={{alignSelf:'center',margin:wp('2')}}>
                        <Text style={{fontSize:hp('2.6'),color:'red'}}>PR (beats/min)</Text>
                    </View>
                    <View style={{alignSelf:'center'}}>
                            <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('12'),height:hp('4.4'),borderColor:'black',borderColor:'grey'}}
                                onChangeText={(g_pr) => this.setState({g_pr})}
                                fontSize={hp('2.6')}
                                value={this.state.g_pr}
                                ref={(input) => { this.t3 = input; }}
                                onSubmitEditing={() => { this.t4.focus(); }}
                                blurOnSubmit={false}
                                />
                    </View>
                    <View style={{alignSelf:'center',margin:wp('2')}}>
                        <Text style={{fontSize:hp('2.6'),color:'blue'}}>RR (times/min)</Text>
                    </View>
                    <View style={{alignSelf:'center'}}>
                            <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('12'),height:hp('4.4'),borderColor:'black',borderColor:'grey'}}
                                onChangeText={(g_rr) => this.setState({g_rr})}
                                fontSize={hp('2.6')}
                                value={this.state.g_rr}
                                ref={(input) => { this.t4 = input; }}
                                onSubmitEditing={() => { this.t5.focus(); }}
                                blurOnSubmit={false}
                                />
                    </View>
                    <View style={{margin:wp('1.7')}}>
                        <Button
                            title="บันทึกเพื่อพลอตกราฟ"
                            onPress={handleSubmit}
                        
                        />
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'center',padding:wp('2')}}>
                    <Button
                        title="Next"
                        onPress={this.handleSubmitToNextPage}
                        containerStyle={{
                            width:wp('10')
                            }}
                    />
                </View>

                </View>
            </Col>
            <Col size={20} style={styles.right_content}>
                        <View style={{flexDirection:'column'}}>
                                <View>
                                <NumericInput type='up-down' 
                                value={this.state.i} 
                                onChange={value => this.setState({i:value})}
                                totalWidth={wp('18')}
                                minValue={1}
                                maxValue={12}
                                />
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <Text style={{fontSize:hp('2.6'),color:'black'}}>{this.o2()}</Text>
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('12'),height:hp('4.4'),borderColor:'black',borderColor:'grey'}}
                                onChangeText={(g_o2sat) => this.setState({g_o2sat})}
                                fontSize={hp('2.6')}
                                value={this.state.g_o2sat}
                                ref={(input) => { this.t5 = input; }}
                                onSubmitEditing={() => { this.t6.focus(); }}
                                blurOnSubmit={false}
                                />
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <Text style={{fontSize:hp('2.6'),color:'black'}}>QB</Text>
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('12'),height:hp('4.4'),borderColor:'black',borderColor:'grey'}}
                                onChangeText={(g_qb) => this.setState({g_qb})}
                                fontSize={hp('2.6')}
                                value={this.state.g_qb}
                                ref={(input) => { this.t6 = input; }}
                                onSubmitEditing={() => { this.t7.focus(); }}
                                blurOnSubmit={false}
                                />
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <Text style={{fontSize:hp('2.6'),color:'black'}}>VP</Text>
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('12'),height:hp('4.4'),borderColor:'black',borderColor:'grey'}}
                                onChangeText={(g_vp) => this.setState({g_vp})}
                                fontSize={hp('2.6')}
                                value={this.state.g_vp}
                                ref={(input) => { this.t7 = input; }}
                                onSubmitEditing={() => { this.t8.focus(); }}
                                blurOnSubmit={false}
                                />
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <Text style={{fontSize:hp('2.6'),color:'black'}}>TMP</Text>
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('12'),height:hp('4.4'),borderColor:'black',borderColor:'grey'}}
                                onChangeText={(g_tmp) => this.setState({g_tmp})}
                                fontSize={hp('2.6')}
                                value={this.state.g_tmp}
                                ref={(input) => { this.t8 = input; }}
                                onSubmitEditing={() => { this.t9.focus(); }}
                                blurOnSubmit={false}
                                />
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <Text style={{fontSize:hp('2.6'),color:'black'}}>UFR</Text>
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('12'),height:hp('4.4'),borderColor:'black',borderColor:'grey'}}
                                onChangeText={(g_ufr) => this.setState({g_ufr})}
                                fontSize={hp('2.6')}
                                value={this.state.g_ufr}
                                ref={(input) => { this.t9 = input; }}
                                onSubmitEditing={() => { this.t10.focus(); }}
                                blurOnSubmit={false}
                                />
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <Text style={{fontSize:hp('2.6'),color:'black'}}>UF</Text>
                            </View>
                            <View style={{alignSelf:'center',marginTop:wp('1')}}>
                                <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('12'),height:hp('4.4'),borderColor:'black',borderColor:'grey'}}
                                onChangeText={(g_uf) => this.setState({g_uf})}
                                fontSize={hp('2.6')}
                                value={this.state.g_uf}
                                ref={(input) => { this.t10 = input; }}
                                onSubmitEditing={() => { this.t1.focus(); }}
                                blurOnSubmit={false}
                                />
                            </View>
                            <View style={{flexDirection:'row',justifyContent:'center',padding:wp('2')}}>
                                <Button
                                    title="บันทึกลงตาราง"
                                    onPress={this.handleSubmitTable}
                                />
                            </View>
                        </View>
            </Col>

          </Grid>
          </KeyboardAwareScrollView>
          </SafeAreaView>

        )}
        </Formik>

    )

}}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      
    },
    up_left:{
      justifyContent:'flex-start',
      alignItems:'flex-start',
      width:wp('10%'),
      
    },
    up_right_page:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      width:wp('10%'),
      padding:hp('2')
    },
    up_line:{
      flexDirection:'row',
      
    },
    up_right:{
      width:wp('80%'),
      justifyContent:'center',
      alignItems:'center',
    },
    head:{
      fontSize:hp('2.6%'),
      fontWeight:'bold',
  
    },
    //from table-component
    containerT: { flex: 1, backgroundColor: '#fff' },
    wrapper: { flexDirection: 'row' },
    title: { flex: 4, backgroundColor: '#f6f8fa' },
    row: {  height: hp('5')  },
    text: { textAlign: 'center' },
    textData: { textAlign: 'right'},
    //end
    left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }

  })