import React from 'react'
import { StyleSheet, Text, View, TextInput, SafeAreaView, Alert, Image, KeyboardAvoidingView,TouchableWithoutFeedback,Keyboard } from 'react-native'
import { Button, CheckBox } from 'react-native-elements'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'

import { Grid, Col } from 'react-native-easy-grid'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { G } from 'react-native-svg'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
console.disableYellowBox = true;
import { JSHash, JSHmac, CONSTANTS } from "react-native-hash";
import axios from 'axios';
import AwesomeAlert from 'react-native-awesome-alerts';

export default class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showAlert:false,
      setServer:false,
      ip:'172.20.10.5',
      port:'3000'
      
    };
  }
  async componentDidMount() {
    // let test_hash = await JSHash('test', CONSTANTS.HashAlgorithms.sha256)
    // console.log(test_hash)
    
    //console.log(window.session_id)
    window.session_id=0;
    //let go = 'bf01ea32efb02f7066182865573c06289666dbae627270e0124b0c9c02d58935'
    // JSHash("test", CONSTANTS.HashAlgorithms.sha256)
    // .then(hash => console.log(hash))
    // .catch(e => console.log(e));
    console.log("HI")

  }
  showAlert = () => {
    this.setState({
      showAlert: true
    });
  };
  hideAlert = () => {
    this.setState({
      showAlert: false
    });
  };
  setServer = ()=>{
    this.setState({setServer:!this.state.setServer})
  }
  confirm = ()=>{
    this.setState({setServer:!this.state.setServer})
    window.ip_server=this.state.ip
    window.port_server=this.state.port
  }

  
  goToSignup = () => this.props.navigation.navigate('Signup')

  handleSubmit = values => {
    
    if(this.validateEmail(values.email)){
      this.authenEmailUser(values.email,values.password)
    }
    else{
      this.authenNameUser(values.email,values.password)
    }

  }

    validateEmail(email) //return true when correct email format
      {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //console.log(re.test(email))
        return re.test(email);
      }


    async authenNameUser(name,password){

      let res = await axios.post('http://'+window.ip_server+':'+window.port_server+'/authenName', {
          
          name:name
        
      })
  
      let data = res.data
      //console.log(data)
      //data[0].password
      let hash_input_pass = await JSHash(password, CONSTANTS.HashAlgorithms.sha256)
      //console.log(hash_input_pass)
      if(data.length>0){
        
        if(hash_input_pass==data[0].password){
          window.session_id=data[0].id
          this.props.navigation.navigate('App')
        }else{this.showAlert()}
        
      }else{
        this.showAlert()
      }
  
    }

  async authenEmailUser(email,password){

    let res = await axios.post('http://'+window.ip_server+':'+window.port_server+'/authenEmail', {
       
        email:email
      
    })

    let data = res.data
    //data[0].password
    let hash_input_pass = await JSHash(password, CONSTANTS.HashAlgorithms.sha256)
    //console.log(hash_input_pass)
    if(data.length>0){
      if(hash_input_pass==data[0].password){
        window.session_id=data[0].id
        this.props.navigation.navigate('App')
      }else{this.showAlert()}
      
    }else{
      this.showAlert()
    }

  }

  render(){
    const {showAlert} = this.state;

    return(
    
    <Formik
        initialValues={{ email: '', password: '' }}
        onSubmit={values => {
          this.handleSubmit(values)
        }}
    >
    {({ handleChange, values, handleSubmit }) => (
        
          <SafeAreaView style={styles.container}>
          <KeyboardAwareScrollView>
          <View style={styles.main}>
            <View> 
              <Image style={styles.bgLogin} source={require('../assets/bgLogin.png')}/>
            </View>
            <View style={styles.right}>
              <View>
                <Image style={styles.logoLogin} source={require('../assets/logoLogin.png')}/>
              </View>
              <View>
                <FormInput
                    name='email'
                    value={values.email}
                    onChangeText={handleChange('email')}
                    placeholder='Enter email'
                    autoCapitalize='none'
                    iconName='ios-mail'
                    iconColor='#2C384A'
                />
                <FormInput
                    name='password'
                    value={values.password}
                    onChangeText={handleChange('password')}
                    placeholder='Enter password'
                    secureTextEntry
                    iconName='ios-lock'
                    iconColor='#2C384A'
                />
              </View>
             
              <View style={styles.buttonContainer}>
                <FormButton
                  buttonType='outline'
                  onPress={handleSubmit}
                  title='LOGIN'
                  buttonColor='darkgreen'
                />
              </View>
              <View>{this.state.setServer&& 
              <View style={{flexDirection:'column',alignSelf:'center',marginTop:wp('-10')}}>
                <View style={{flexDirection:'row'}}>
                <View><Text>IP</Text></View>
                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('20'),height:hp('4.4'),borderColor:'black',borderColor:'grey',marginLeft:wp(2)}}
                                    onChangeText={(ip) => this.setState({ip})}
                                    fontSize={hp('2.6')}
                                    value={this.state.ip}
                                />    
                </View>
                <View><Text></Text></View>
                <View style={{flexDirection:'row'}}>
                <View><Text>Port</Text></View>
                <TextInput
                                    style={{borderWidth:StyleSheet.hairlineWidth,width:wp('18.6'),height:hp('4.4'),borderColor:'black',borderColor:'grey',marginLeft:wp(2)}}
                                    onChangeText={(port) => this.setState({port})}
                                    fontSize={hp('2.6')}
                                    value={this.state.port}
                                /> 
                                
                </View>
                <View>
                <Button
                                onPress={this.confirm}
                                title='Confirm IP and Port'
                                containerStyle={{
                                width:wp('18'),
                                margin:wp('1'),
                                alignSelf:'center'
                                }}
                            />

                </View>

              </View>
              
              
              
              
              }
              </View>
              <View style={{alignSelf:'flex-end',flexDirection:'row'}}>
              <View style={{marginTop:wp('1')}}><Text>Server setting</Text></View>
              <View>
              <CheckBox
                                  checkedIcon={<Image style={{width:wp(1.8),height:wp(1.8)}} source={require('../assets/setting.png')}/>}
                                  uncheckedIcon={<Image style={{width:wp(1.8),height:wp(1.8)}} source={require('../assets/setting.png')} />}
                                  checked={this.state.setServer}
                                  onPress={() => this.setServer()}
                                  //containerStyle={{alignItems:'flex-end',alignSelf:'flex-end',backgroundColor:'black',width:wp(40),justifyContent:'flex-end'}}
                                  /> 

              </View>
              </View>
              
              
              
            </View>
            
          </View>
          </KeyboardAwareScrollView>
          
          <AwesomeAlert
          show={showAlert}
          showProgress={false}
          title="Invalid email or password"
          titleStyle={{fontSize:hp('4.6')}}
                confirmButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1),}}
                //confirmButtonStyle={{marginLeft:60}}
                cancelButtonTextStyle={{fontSize:hp('4'),fontWeight:'bold',paddingLeft: wp(4),paddingRight:wp(4),padding: wp(1)}}
          message=""
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="Cancel"
          confirmText="OK"
          confirmButtonColor="red"
          // cancelButtonColor="#606060"
          onCancelPressed={() => {
            // window.deleteKey=''
            // this.hideAlert();
          }}
          onConfirmPressed={() => {
            // this.toDeleteRecord(window.deleteKey)
             this.hideAlert()
          }}
        />
      
        </SafeAreaView>
        
        )}
    </Formik>
    
  )}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    
  },
  main:{
    flexDirection:'row'

  },
  right:{
    flexDirection:'column',
    justifyContent:'space-around'
  },
  buttonContainer: {
    height: hp('20%'), 
    width: wp('30%'),
    alignSelf:'center',
  },
  bgLogin:{
    height: hp('100%'), 
    width: wp('64%'),
    alignSelf:'flex-start',
    resizeMode:'contain'
  },
  logoLogin:{
    height: hp('36%'), 
    width: wp('36%'),
    alignSelf:'flex-end',
    resizeMode:'contain'
  }

})