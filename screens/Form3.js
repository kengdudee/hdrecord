import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TextInput } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import FormInput from '../components/FormInput'
import FormButton from '../components/FormButton'
import { Formik } from 'formik'
// import { TextInput } from 'react-native-gesture-handler';
import { Button, CheckBox } from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
console.disableYellowBox = true;

export default class Form3 extends React.Component {

goToForm2 = () => this.props.navigation.navigate('Form2')
goToForm4 = () => this.props.navigation.navigate('Form4')

constructor(props) {
    super(props);
    this.state = {
        DLC:false,
        PERM:false,
        AVF:false,
        AVG:false,
        thrill:false,
        bruit:false,

        catheter_site:'',
        insertion_date:'',
        needle_no:'',
        characteristics_found:'',
        BP:'',
        BPup:'',
        BPdown:'',
        PR:'',
        RR:'',
        sign_symp:''
    };
}
async componentDidMount(){
    this.setState({DLC:window.DLC})
    this.setState({PERM:window.PERM})
    this.setState({AVF:window.AVF})
    this.setState({AVG:window.AVG})
    this.setState({thrill:window.thrill})
    this.setState({bruit:window.bruit})
    this.setState({catheter_site:window.catheter_site})
    this.setState({insertion_date:window.insertion_date})
    this.setState({needle_no:window.needle_no})
    this.setState({characteristics_found:window.characteristics_found})
    this.setState({BP:window.BP})
    var localBPup = window.BP.split('/')[0]
    var localBPdown = window.BP.split('/')[1]
    this.setState({BPup:localBPup})
    if(localBPdown!=undefined){
        this.setState({BPdown:localBPdown})
    }
    this.setState({PR:window.PR})
    this.setState({RR:window.RR})
    this.setState({sign_symp:window.sign_symp})
}
handleBack = ()=>{
    var localBP = this.state.BPup+"/"+this.state.BPdown
    //console.log(localBP)

    window.DLC=this.state.DLC
    window.PERM=this.state.PERM
    window.AVF=this.state.AVF
    window.AVG=this.state.AVG
    window.thrill=this.state.thrill
    window.bruit=this.state.bruit
    window.catheter_site=this.state.catheter_site
    window.insertion_date=this.state.insertion_date
    window.needle_no=this.state.needle_no
    window.characteristics_found=this.state.characteristics_found
    window.BP=localBP
    window.PR=this.state.PR
    window.RR=this.state.RR
    window.sign_symp=this.state.sign_symp
    this.goToForm2()
    
}

handleSubmit = values => {

    var localBP = this.state.BPup+"/"+this.state.BPdown
    //console.log(localBP)

    window.DLC=this.state.DLC
    window.PERM=this.state.PERM
    window.AVF=this.state.AVF
    window.AVG=this.state.AVG
    window.thrill=this.state.thrill
    window.bruit=this.state.bruit
    window.catheter_site=this.state.catheter_site
    window.insertion_date=this.state.insertion_date
    window.needle_no=this.state.needle_no
    window.characteristics_found=this.state.characteristics_found
    window.BP=localBP
    window.PR=this.state.PR
    window.RR=this.state.RR
    window.sign_symp=this.state.sign_symp
    this.goToForm4()
}
dOrP = key =>{
    var d ="d"
    if(!d.localeCompare(key)){
        this.setState({DLC: !this.state.DLC})
        this.setState({PERM: false})
    }
    else{
        this.setState({DLC: false})
        this.setState({PERM: !this.state.PERM})
    }
}
fOrG = key =>{
    var f ="f"
    if(!f.localeCompare(key)){
        this.setState({AVF: !this.state.AVF})
        this.setState({AVG: false})
    }
    else{
        this.setState({AVF: false})
        this.setState({AVG: !this.state.AVG})
    }
}

  render() {
    return (
        <Formik
        initialValues={{catheter_site:'',insertion_date:'',needle_no:'',characteristics_found:'',BPup:'',BPdown:'',PR:'',RR:'',sign_symp:''}}
        onSubmit={values => {
          this.handleSubmit(values)
        }}
        >
        {({ handleChange, values, handleSubmit}) => (
        <SafeAreaView style={styles.container}>
            <KeyboardAwareScrollView>
              <View style={styles.up}>
              <View style={styles.up_line}>
              <View style={styles.up_left}>
                      <Ionicons.Button
                        name={'ios-arrow-back'}
                        size={hp('4')}
                        color='#000000'
                        backgroundColor='#fff'
                        onPress={this.handleBack}
                        title="back"
                      />
                </View>
              <View style={styles.up_right}>
                    <Text style={styles.head}></Text>
              </View>
              <View style={styles.up_right_page}>
                    <Text style={styles.head}>3/11</Text>
              </View>
            </View>
            </View>
            <Grid>
              <Col size={50} style={styles.left_content}>
                  <View style={{flexDirection:'column'}}>
                      <View style={{flexDirection:'row',padding:hp('2')}}>
                        <Text style={{fontSize:hp('2.6')}}>VASCULA ACCESS</Text>
                      </View>
                      <View style={styles.checkBoxArea}>
                            <CheckBox
                                title='DLC'
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                checked={this.state.DLC}
                                onPress={() => this.dOrP("d")}
                                size={wp('2')}
                                textStyle={{
                                    fontSize:hp('2.6')
                                }}
                                />
                                <CheckBox
                                    title='PERM'
                                    checked={this.state.PERM}
                                    onPress={() => this.dOrP("p")}
                                    size={wp('2')}
                                    textStyle={{
                                        fontSize:hp('2.6')
                                    }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    
                                />
                        </View>
                        <View style={{flexDirection:'row',padding:hp('2')}}>
                            <Text style={{fontSize:hp('2.6')}}>Catheter : site</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View>
                                <TextInput
                                        style={styles.longTextInput}
                                        onChangeText={(catheter_site) => this.setState({catheter_site})}
                                        value={this.state.catheter_site}
                                        fontSize={hp('2.6')}
                                        onSubmitEditing={() => { this.secondTextInput.focus(); }}
                                        blurOnSubmit={false}
                                    />
                            </View>
                        </View>
                        <View style={{flexDirection:'row',padding:hp('2')}}>
                            <Text style={{fontSize:hp('2.6')}}>Insertion Date</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View>
                                <TextInput
                                        style={{borderWidth:StyleSheet.hairlineWidth,width:wp('46'),height:hp('4.4'),marginLeft:hp('2'),marginBottom:hp('2'),borderColor:'grey'}}
                                        onChangeText={(insertion_date) => this.setState({insertion_date})}
                                        value={this.state.insertion_date}
                                        fontSize={hp('2.6')}
                                        ref={(input) => { this.secondTextInput = input; }}
                                        onSubmitEditing={() => { this.thirdTextInput.focus(); }}
                                        blurOnSubmit={false}
                                    />
                            </View>
                        </View>
                        <View style={styles.checkBoxArea}>
                            <CheckBox
                                title='AVF'
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                checked={this.state.AVF}
                                onPress={() => this.fOrG("f")}
                                size={wp('2')}
                                textStyle={{
                                    fontSize:hp('2.6')
                                }}
                                />
                                <CheckBox
                                    title='AVG'
                                    checked={this.state.AVG}
                                    onPress={() => this.fOrG("g")}
                                    size={wp('2')}
                                    textStyle={{
                                        fontSize:hp('2.6')
                                    }}
                                    checkedIcon='dot-circle-o'
                                    uncheckedIcon='circle-o'
                                    
                                />
                        </View>
                        <View style={{flexDirection:'row',padding:hp('2')}}>
                            <Text style={{fontSize:hp('2.6')}}>Needle No.</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View>
                                <TextInput
                                        style={{borderWidth:StyleSheet.hairlineWidth,width:wp('46'),height:hp('4.4'),marginLeft:hp('2'),marginBottom:hp('2'),borderColor:'grey'}}
                                        onChangeText={(needle_no) => this.setState({needle_no})}
                                        value={this.state.needle_no}
                                        fontSize={hp('2.6')}
                                        ref={(input) => { this.thirdTextInput = input; }}
                                        onSubmitEditing={() => { this.fourthTextInput.focus(); }}
                                        blurOnSubmit={false}
                                    />
                            </View>
                        </View>
                        <View style={styles.checkBoxArea}>
                            <CheckBox
                                title='Thrill'
                                checked={this.state.thrill}
                                onPress={() => this.setState({thrill: !this.state.thrill})}
                                size={wp('2')}
                                textStyle={{
                                    fontSize:hp('2.6')
                                }}
                                />
                                <CheckBox
                                    title='Bruit'
                                    checked={this.state.bruit}
                                    onPress={() => this.setState({bruit: !this.state.bruit})}
                                    size={wp('2')}
                                    textStyle={{
                                        fontSize:hp('2.6')
                                    }}
                                />
                        </View>

                  </View>
              </Col>
              <Col size={50} style={styles.right_content}>
                        <View style={{flexDirection:'row',padding:hp('2')}}>
                            <Text style={{fontSize:hp('2.6')}}>Characteristics found</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View>
                                <TextInput
                                        style={styles.longTextInput}
                                        onChangeText={(characteristics_found) => this.setState({characteristics_found})}
                                        value={this.state.characteristics_found}
                                        fontSize={hp('2.6')}
                                        ref={(input) => { this.fourthTextInput = input; }}
                                        onSubmitEditing={() => { this.fifthTextInput.focus(); }}
                                        blurOnSubmit={false}
                                    />
                            </View>
                        </View>
                        <View style={{flexDirection:'row',padding:hp('2')}}>
                            <Text style={{fontSize:hp('2.6')}}>แรกรับ V/S Pre HD BP (mmHg)</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View>
                            <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('21'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                onChangeText={(BPup) => this.setState({BPup})}
                                value={this.state.BPup}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.fifthTextInput = input; }}
                                onSubmitEditing={() => { this.sixthTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                            </View>
                            <View style={{width:wp('1'),height:hp('4.4'),marginLeft:wp('1')}}>
                                <Text style={{fontSize:hp('3')}}>
                                    /
                                </Text>
                            </View>
                            <View>
                            <TextInput
                                style={{borderWidth:StyleSheet.hairlineWidth,width:wp('21'),height:hp('4.4'),marginLeft:hp('2'),borderColor:'grey'}}
                                onChangeText={(BPdown) => this.setState({BPdown})}
                                value={this.state.BPdown}
                                fontSize={hp('2.6')}
                                ref={(input) => { this.sixthTextInput = input; }}
                                onSubmitEditing={() => { this.seventhTextInput.focus(); }}
                                blurOnSubmit={false}
                            />
                    </View>
                </View>
                    <View style={{flexDirection:'row',padding:hp('2')}}>
                            <Text style={{fontSize:hp('2.6')}}>PR (beats/min)</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View>
                                <TextInput
                                        style={styles.longTextInput}
                                        onChangeText={(PR) => this.setState({PR})}
                                        value={this.state.PR}
                                        fontSize={hp('2.6')}
                                        ref={(input) => { this.seventhTextInput = input; }}
                                        onSubmitEditing={() => { this.eighthTextInput.focus(); }}
                                        blurOnSubmit={false}
                                    />
                            </View>
                    </View>
                    <View style={{flexDirection:'row',padding:hp('2')}}>
                            <Text style={{fontSize:hp('2.6')}}>RR (times/min)</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View>
                                <TextInput
                                        style={styles.longTextInput}
                                        onChangeText={(RR) => this.setState({RR})}
                                        value={this.state.RR}
                                        fontSize={hp('2.6')}
                                        ref={(input) => { this.eighthTextInput = input; }}
                                        onSubmitEditing={() => { this.ninthTextInput.focus(); }}
                                        blurOnSubmit={false}
                                        
                                    />
                            </View>
                    </View>
                    <View style={{flexDirection:'row',padding:hp('2')}}>
                            <Text style={{fontSize:hp('2.6')}}>Sign and Symptom</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View>
                                <TextInput
                                        style={{borderWidth:StyleSheet.hairlineWidth,width:wp('46'),height:hp('26'),marginLeft:hp('2'),borderColor:'grey'}}
                                        onChangeText={(sign_symp) => this.setState({sign_symp})}
                                        value={this.state.sign_symp}
                                        fontSize={hp('2.6')}
                                        multiline={true}
                                        ref={(input) => { this.ninthTextInput = input; }}
                                    />
                            </View>
                            
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'flex-end',padding:wp('2')}}>
                  <Button
                            onPress={handleSubmit}
                            title='Next'
                            containerStyle={{
                              width:wp('10')
                            }}
                        />
                  </View>
              </Col>
            </Grid>
            </KeyboardAwareScrollView>

         </SafeAreaView>
        )
        }
        </Formik>
    )
}
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      
    },
    up_left:{
        justifyContent:'flex-start',
        alignItems:'flex-start',
        width:wp('10%'),
        
      },
      up_right_page:{
        justifyContent:'flex-end',
        alignItems:'flex-end',
        width:wp('10%'),
        padding:hp('2')
      },
      up_line:{
        flexDirection:'row',
        
      },
      up_right:{
        width:wp('80%'),
        justifyContent:'center',
        alignItems:'center',
      },
      head:{
        fontSize:hp('2.6%'),
        fontWeight:'bold',
    
      },
      longTextInput:{
        borderWidth:StyleSheet.hairlineWidth,width:wp('46'),
        height:hp('4.4'),
        marginLeft:hp('2'),
        borderColor:'grey'
        
      },
      checkBoxArea:{
        flexDirection:'row',
        padding:hp('2'),
        justifyContent:'center',
        borderTopWidth:StyleSheet.hairlineWidth,
        borderBottomWidth:StyleSheet.hairlineWidth
        
      },
      left_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      mid_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      },
      right_content:{
        borderTopWidth: StyleSheet.hairlineWidth,
        borderRightWidth: StyleSheet.hairlineWidth
      }

})