function test(input1,input2){
    //var input1=[[3,4,5,6,7,8,9,10,11,12,13,14,15],[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],[5,6,7,8,9,10]]
    var au=[]
    //var input2=[[3,4,5],[8,9,10]]
    var result=[]
    var resultIndex=0

    const unique = (value, index, self) => {
        return self.indexOf(value) === index
      }
  
      for(var i=0;i<input1.length;i++){
        for(var j=0;j<input1[i].length;j++){
              au.push(input1[i][j]);
        }
      }
  
      const uniqueAu = au.filter(unique)
  
      uniqueAu.sort(function(a, b){return a - b})
      
      for(var i=0;i<input2.length;i++){
        for(var j=0;j<input2[i].length;j++){
          var isCompareToNotEqual=false
          while(true){
            var k=0
            if(uniqueAu.length!=0){
              if(input2[i][j]!=uniqueAu[k]){
                  result.push([])
                  result[resultIndex].push(uniqueAu[k])
                  uniqueAu.splice(k,1)
                  isCompareToNotEqual=true
              }else{
                uniqueAu.splice(k,1)
                if(isCompareToNotEqual){
                  resultIndex++
                }
                break
              }
            }
          }
        }
      }
      
      result[resultIndex]=uniqueAu
      return result;

}