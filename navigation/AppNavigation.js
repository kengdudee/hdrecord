//AppNavigation.js
import { createStackNavigator } from 'react-navigation-stack'
import React from 'react'
import {createBottomTabNavigator} from 'react-navigation-tabs'
import Home from '../screens/Home'
import Account from '../screens/Account'
import Ionicons from 'react-native-vector-icons/Ionicons'

const AppNavigation = createBottomTabNavigator(
  {
    Home: { screen: Home },
    Account: { screen: Account }
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home`;
        } else if (routeName === 'Account') {
          iconName = `ios-person`;
        }
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#000000',
      inactiveTintColor: 'gray',
    },
  }
  
)

export default AppNavigation