import { createStackNavigator } from 'react-navigation-stack'
import Home from '../screens/Home'
import Form1 from '../screens/Form1'
import Form2 from '../screens/Form2'
import Form3 from '../screens/Form3'
import Form4 from '../screens/Form4'
import Form5 from '../screens/Form5'
import Form6 from '../screens/Form6'
import Form7 from '../screens/Form7'
import Form8 from '../screens/Form8'
import Form9 from '../screens/Form9'
import Form10 from '../screens/Form10'
import Form11 from '../screens/Form11'
import Search from '../screens/Search'
import RecentlyDeleted from '../screens/RecentlyDeleted'
import ViewContent from '../screens/ViewContent'
import ManageUser from '../screens/ManageUser'
import UpdateUser from '../screens/UpdateUser'
import { Form } from 'formik'

const FormNavigation = createStackNavigator(
    {
        Home: { screen: Home },
        Form1: { screen: Form1 },
        Form2: { screen: Form2 },
        Form3: { screen: Form3 },
        Form4: { screen: Form4 },
        Form5: { screen: Form5 },
        Form6: { screen: Form6 },
        Form7: { screen: Form7 },
        Form8: { screen: Form8 },
        Form9: { screen: Form9 },
        Form10: { screen: Form10 },
        Form11: { screen: Form11 },
        Search: { screen: Search },
        RecentlyDeleted: { screen: RecentlyDeleted },
        ViewContent: { screen: ViewContent },
        ManageUser: { screen: ManageUser },
        UpdateUser: {screen: UpdateUser},
    },
    {
      initialRouteName: 'Form1',
      headerMode: 'none'
    }
  )
  
  export default FormNavigation